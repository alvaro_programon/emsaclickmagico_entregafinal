-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 03, 2018 at 05:14 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clickmagicodb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `description`, `token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'Primer usuario para app graficos de prueba', 'jlasjdklsajdlkasjdklasjdklasjdklasdjklas', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'depa text 4', '2018-06-08 02:22:26', '2018-06-08 03:05:29'),
(2, 'depa test', '2018-06-08 02:23:14', '2018-06-08 02:23:14');

-- --------------------------------------------------------

--
-- Table structure for table `elements`
--

CREATE TABLE `elements` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maintainer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `elements`
--

INSERT INTO `elements` (`id`, `name`, `maintainer_id`, `created_at`, `updated_at`) VALUES
(13, '1', 2, '2018-06-13 04:11:28', '2018-06-13 04:11:28'),
(14, '2', 2, '2018-06-13 04:11:32', '2018-06-13 04:11:32'),
(15, '3', 2, '2018-06-13 04:11:35', '2018-06-13 04:11:35'),
(16, '4', 2, '2018-06-13 04:11:38', '2018-06-13 04:11:38'),
(17, '5', 2, '2018-06-13 04:11:40', '2018-06-13 04:11:40'),
(18, 'L1', 4, '2018-06-13 04:26:40', '2018-06-13 04:26:40'),
(19, 'L2', 4, '2018-06-13 04:26:40', '2018-06-13 04:26:40'),
(20, 'L3', 4, '2018-06-13 04:26:40', '2018-06-13 04:26:40'),
(21, 'L4', 4, '2018-06-13 04:26:40', '2018-06-13 04:26:40'),
(22, 'Exploradora', 5, '2018-06-26 18:41:48', '2018-06-26 18:41:48'),
(23, 'Chuquicamata', 5, '2018-06-26 18:41:48', '2018-06-26 18:41:48'),
(24, 'Atacama', 5, '2018-06-26 18:41:48', '2018-06-26 18:41:48'),
(25, '1.- Reconocimiento caminos sectores plataformas', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(26, '2.- Recepción Plataforma de Perforación', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(27, '3.- Traslado equipo Sonda a Plataforma', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(28, '4.- Traslado de personal desde y hacia Plataforma', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(29, '5.- Posicionamiento de la Sonda y estabilizadores hidráulicos', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(30, '6.- Nivelación de la Torre', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(31, '7.- Verificación inclinación del pozo', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(32, '8.- Instalación de carpeta de polietileno bajo camión sonda', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(33, '9.- Izaje de barras y accesorios de perforación con camión pluma', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(34, '10.- Demarcación de la Plataforma y cierre perimetral', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(35, '11.- Retiro de carpeta de polietileno bajo camión sonda', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(36, '12.- Construcción de canales para agua piscina decantadora', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(37, '13.- Armado y chequeo de herramientas según diámetro a perforar', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(38, '14.- Preparación de lodos', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(39, '15.- Inicio de pozo', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(40, '16.- Instalación de Cassing', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(41, '17.- Perforación', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(42, '18.- Conexión de pescante', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(43, '19.- Instalación y retiro de tubos interior con muestra', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(44, '20.- Desarme tubo interior', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(45, '21.- Retiro de muestra de tubo interior', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(46, '22.- Manipulación y almacenamiento de muestras y testigos', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(47, '23.- Carga y traslado de la muestra', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(48, '24.- Introducción equipo medición pozo', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(49, '25.- Desacople adaptador cámara', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(50, '26.- Traslado de camión con combustible a stock Plataforma', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(51, '27.- Traslado camión combustible fuera de faena', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(52, '28.- Abastacimiento de combustible a equipos', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(53, '29.- Carga de estanque camión en toma de agua', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(54, '30.- Abastecimiento de agua a sonda', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(55, '31.- Traslado de camión aljibe fuera de faena', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(56, '32.- Carguío de agua en estanque plataforma', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(57, '33.- Conexión de pescante', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(58, '34.- Retiro de tubo interior con muestra', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(59, '35.- Desarme del tubo interior', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(60, '36.- Retiro de muestra del tubo interior', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(61, '37.- Lavado de la muestra y almacenamiento', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(62, '38.- Armado y desarme de cabeza de tubo interior', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(63, '39.-Carga y traslado de muestras', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(64, '40.- Maniobras de cargas', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(65, '41.- Traslado de cargas', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(66, '42.- Maniobras de descarga', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(67, '43.- Traslado de quipo compresor', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(68, '44.- Cambio aceite equipos', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(69, '45.- Cambio de filtros', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(70, '46.- Engrase', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(71, '47.- Cambio de componentes', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(72, '48.- Soldadura al arco', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(73, '49.- Esmerilado', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(74, '50.- Oxicorte', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(75, '51.- Orden y aseo en Sondas', 6, '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(76, '1.- Instalación brocas de perforación', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(77, '2.- Incorporación de barras', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(78, '3.- Perforación', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(79, '4.- Instalación de Cassing', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(80, '5.- Rescate de herramientas', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(81, '6.- Movimiento de herramienta', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(82, '7.- Cuarteo y retiro de muestras', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(83, '8.- Armado y desarme de barril', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(84, '9.- Armado y desarme de cabeza de tuno interior', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(85, '10.- Posicionamiento equipo alimentador de barras', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(86, '11.- Operación de alimentador de barras', 7, '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(87, 'No percibe el riesgo', 8, '2018-06-26 19:14:49', '2018-06-26 19:14:49'),
(88, 'Ahorro de tiempo', 8, '2018-06-26 19:14:49', '2018-06-26 19:14:49'),
(89, 'No es cómodo', 8, '2018-06-26 19:14:49', '2018-06-26 19:14:49'),
(90, 'No se encuentra o no existe', 8, '2018-06-26 19:14:49', '2018-06-26 19:14:49'),
(91, 'No recibió entrenamiento', 8, '2018-06-26 19:14:49', '2018-06-26 19:14:49'),
(92, 'Alta presión de trabajo', 8, '2018-06-26 19:14:49', '2018-06-26 19:14:49'),
(93, 'Espacios insuficientes', 8, '2018-06-26 19:14:49', '2018-06-26 19:14:49'),
(94, 'No está normado', 8, '2018-06-26 19:14:49', '2018-06-26 19:14:49'),
(95, 'Derrames de hidrocarburo', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(96, 'Manejo inadecuado de residuos (almacenamiento/segregación)', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(97, 'Falta de señalización/advertencia', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(98, 'Sistemas de contención deficientes/insuficientes', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(99, 'Fala/Es inadecuado sistema de contención de derrames', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(100, 'No existe/No se indentifica el peligro', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(101, 'Segregación deficiente/No se respeta', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(102, 'Acumulación/Existencia de materiales combustibles', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(103, 'Baños deficientes/Malas condiciones', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(104, 'Falta de sistemas de combate y control de incendios', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(105, 'Vehículos/Equipos fuera de estándar', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(106, 'Superficies de trabajo deficientes', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(107, 'Salidas de emergencia obstruídas', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(108, 'Fuga de líquidos de operación', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(109, 'Iluminación deficiente', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(110, 'Almacenamiento de EPP inadecuado', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(111, 'Vehículos/Equipos con sistema de iluminación en mal estado', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(112, 'Fuente de calor cercana a materiales combustibles', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(113, 'Interlock no funciona/no existe', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(114, 'Falta de señalización/advertencia', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(115, 'Falta sistema de contención de derrames', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(116, 'No existe conexión a tablero eléctrico', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(117, 'Herramienta manual sin revisión trimestral', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(118, 'Vías de evacuación no señalizadas', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(119, 'Acopio de materiales deficientes', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(120, 'Tablero eléctrico abierto/sin sistema de bloqueo', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(121, 'Orden y Aseo deficiente', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(122, 'No cuenta con certificación', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(123, 'Herramientas en mal estado/no identificadas', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(124, 'No usa EPP adecuado', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(125, 'Falta de protección de partes móviles', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(126, 'Falta de anclaje/amarre/apoyo', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(127, 'No usa cuñas', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(128, 'Zona de tránsito obstaculizadas', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(129, 'Falta de elementos de emergencia', 9, '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(130, 'ECF1', 10, '2018-06-26 19:39:24', '2018-06-26 19:39:24'),
(131, 'ECF2', 10, '2018-06-26 19:39:24', '2018-06-26 19:39:24'),
(132, 'ECF3', 10, '2018-06-26 19:39:24', '2018-06-26 19:39:24'),
(133, 'ECF4', 10, '2018-06-26 19:39:24', '2018-06-26 19:39:24'),
(134, 'ECF5', 10, '2018-06-26 19:39:24', '2018-06-26 19:39:24'),
(135, 'ECF6', 10, '2018-06-26 19:39:24', '2018-06-26 19:39:24'),
(136, '1', 11, '2018-06-26 19:46:19', '2018-06-26 19:46:19'),
(137, '2', 11, '2018-06-26 19:46:19', '2018-06-26 19:46:19'),
(138, '3', 11, '2018-06-26 19:46:19', '2018-06-26 19:46:19'),
(139, 'OM', 11, '2018-06-26 19:46:19', '2018-06-26 19:46:19'),
(140, 'BP', 11, '2018-06-26 19:46:19', '2018-06-26 19:46:19'),
(141, '6', 2, '2018-06-26 19:53:00', '2018-06-26 19:53:00'),
(142, '7', 2, '2018-06-26 19:53:03', '2018-06-26 19:53:03'),
(143, '8', 2, '2018-06-26 19:53:06', '2018-06-26 19:53:06'),
(144, '9', 2, '2018-06-26 19:53:08', '2018-06-26 19:53:08'),
(145, '10', 2, '2018-06-26 19:53:12', '2018-06-26 19:53:12'),
(146, 'Almacenamiento de Barras', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(147, 'Almacenamiento de Materiales', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(148, 'Descarga de Materiales', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(149, 'Perforación', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(150, 'Corte de barras', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(151, 'Recuperación de barras', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(152, 'Traslado de materiales', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(153, 'Transporte de personal', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(154, 'Manipulación de sustancias peligrosas', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(155, 'Almacenamiento de sustancias peligrosas', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30'),
(156, 'Mantenimiento de equipos', 12, '2018-06-26 20:06:30', '2018-06-26 20:06:30');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Bajo', '2018-06-24 04:00:00', NULL),
(2, 'Medio', '2018-06-24 04:00:00', NULL),
(3, 'Alto', '2018-06-24 04:00:00', NULL),
(4, 'Completado', '2018-06-24 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `maintainers`
--

CREATE TABLE `maintainers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `maintainers`
--

INSERT INTO `maintainers` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Numeros del 1 al 10', '2018-06-13 03:26:22', '2018-06-26 19:53:22'),
(4, 'Lineas Metro', '2018-06-13 04:26:40', '2018-06-13 04:26:40'),
(5, 'Contrato', '2018-06-26 18:41:48', '2018-06-26 18:41:48'),
(6, 'Perforación con Diamantina', '2018-06-26 19:00:25', '2018-06-26 19:00:25'),
(7, 'Perforación con Aire Reverso', '2018-06-26 19:02:58', '2018-06-26 19:02:58'),
(8, 'Barrera', '2018-06-26 19:14:49', '2018-06-26 19:14:49'),
(9, 'Hallazgo', '2018-06-26 19:37:34', '2018-06-26 19:37:34'),
(10, 'ECF', '2018-06-26 19:39:24', '2018-06-26 19:39:24'),
(11, 'Nivel de criticidad', '2018-06-26 19:46:19', '2018-06-26 19:46:19'),
(12, 'Tarea', '2018-06-26 20:06:30', '2018-06-26 20:06:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_06_06_204519_create_positions_table', 1),
(2, '2018_06_06_204725_create_supervisors_table', 2),
(3, '2018_06_07_221312_create_departments_table', 3),
(4, '2018_06_08_005208_create_polls_table', 4),
(5, '2018_06_08_233106_create_questions_table', 5),
(6, '2018_06_10_211938_create_options_table', 6),
(7, '2018_06_11_190926_create_options_table', 7),
(8, '2018_06_11_235811_create_questions_table', 8),
(9, '2018_06_12_193156_create_maintainers_table', 9),
(10, '2018_06_12_193444_create_elements_table', 10),
(11, '2018_06_13_022510_create_professionals_table', 11),
(12, '2018_06_13_022802_create_tokens_table', 11),
(13, '2018_06_13_185342_create_supervisors_table', 12),
(14, '2018_06_13_185809_create_supervisors_table', 13),
(15, '2018_06_13_190637_create_supervisors_table', 14),
(16, '2018_06_13_191302_create_tokens_table', 15),
(17, '2018_06_13_191331_create_supervisors_table', 16),
(18, '2018_06_13_191444_create_professionals_table', 17),
(19, '2018_06_13_193036_create_supervisor_tokens_table', 18),
(20, '2018_06_13_193121_create_professional_tokens_table', 18),
(21, '2018_06_13_193504_create_professionals_table', 19),
(22, '2018_06_13_193544_create_supervisors_table', 19),
(23, '2018_06_14_025025_create_supervisor_tokens_table', 20),
(24, '2018_06_14_025038_create_professional_tokens_table', 20),
(25, '2018_06_14_025625_create_professional_tokens_table', 21),
(26, '2018_06_14_025630_create_supervisor_tokens_table', 21),
(27, '2018_06_14_025717_create_professionals_table', 22),
(28, '2018_06_14_025919_create_supervisors_table', 23),
(29, '2018_06_14_204839_create_questions_table', 24),
(30, '2018_06_17_033030_create_responses_table', 25),
(31, '2018_06_18_003948_create_responses_table', 26),
(32, '2018_06_18_215740_create_responses_table', 27),
(33, '2018_06_22_200909_create_tokens_fcm_professional_table', 28),
(34, '2018_06_22_201606_create_tokens_fcm_supervisor_table', 29),
(35, '2018_06_23_213508_create_answers_table', 30),
(36, '2018_06_23_213907_create_response_answer_foreign_table', 31),
(37, '2018_06_24_132733_create_tasks_table', 32),
(38, '2018_06_24_133640_create_levels_table', 33),
(39, '2018_06_24_133728_create_states_table', 34),
(40, '2018_06_26_203635_create_scheduled_tasks_table', 35),
(41, '2018_06_26_205453_create_points_table', 36),
(42, '2018_06_26_205619_create_foreign_points_scheduletasks_table', 37),
(43, '2018_06_27_122040_create_principal_scheduled_tasks_table', 38),
(44, '2018_06_27_122219_create_foreign_points_principal_scheduletasks_table', 39),
(45, '2018_06_27_164219_create_delete_principal_scheduletasks_table', 40),
(46, '2018_07_01_192148_create_admins_table', 41);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `turn_on_question_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `name`, `question_id`, `turn_on_question_id`, `created_at`, `updated_at`) VALUES
(67, 'Si', 41, NULL, '2018-06-22 03:17:57', '2018-06-22 03:17:57'),
(68, 'No', 41, 42, '2018-06-22 03:17:57', '2018-06-22 03:18:08'),
(69, 'Si', 43, NULL, '2018-06-22 04:07:24', '2018-06-22 04:07:24'),
(70, 'No', 43, 44, '2018-06-22 04:07:24', '2018-06-22 04:07:52'),
(71, 'Nivel Bajo', 44, NULL, '2018-06-22 04:07:52', '2018-06-22 04:07:52'),
(72, 'Nivel Medio', 44, NULL, '2018-06-22 04:07:52', '2018-06-22 04:07:52'),
(73, 'Nivel Alto', 44, 45, '2018-06-22 04:07:52', '2018-06-22 04:08:03'),
(74, 'Si', 46, NULL, '2018-06-22 04:10:08', '2018-06-22 04:10:08'),
(76, 'No', 46, 48, '2018-06-22 04:49:43', '2018-06-22 04:49:54'),
(77, 'Si', 49, 50, '2018-06-22 22:30:44', '2018-06-22 22:30:54'),
(78, 'No', 49, NULL, '2018-06-22 22:30:44', '2018-06-22 22:30:44'),
(79, 'Almacenamiento de Barras', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(80, 'Almacenamiento de Materiales', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(81, 'Descarga de Materiales', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(82, 'Perforación', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(83, 'Corte de barras', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(84, 'Recuperación de barras', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(85, 'Traslado de materiales', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(86, 'Transporte de personal', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(87, 'Manipulación de sustancias peligrosas', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(88, 'Almacenamiento de sustancias peligrosas', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(89, 'Mantenimiento de equipos', 52, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(90, 'Si', 53, NULL, '2018-06-26 18:43:48', '2018-06-26 18:43:48'),
(91, 'No', 53, 54, '2018-06-26 18:43:48', '2018-06-26 18:44:15'),
(92, 'Perforación con Diamantina', 55, 56, '2018-06-26 19:07:07', '2018-06-26 19:08:03'),
(93, 'Perforación con Aire Reverso', 55, 57, '2018-06-26 19:07:07', '2018-06-26 19:08:31'),
(94, 'CS', 58, NULL, '2018-06-26 19:15:29', '2018-06-26 19:15:29'),
(95, 'CR', 58, 59, '2018-06-26 19:15:29', '2018-06-26 19:15:42'),
(96, 'NA', 58, NULL, '2018-06-26 19:15:29', '2018-06-26 19:15:29'),
(97, 'Perforación con Diamantina', 65, 66, '2018-06-26 19:43:22', '2018-06-26 19:43:45'),
(98, 'Perforación con Aire Reverso', 65, 67, '2018-06-26 19:43:22', '2018-06-26 19:44:02'),
(99, 'Perforación con Diamantina', 76, 77, '2018-06-26 19:56:59', '2018-06-26 19:57:35'),
(100, 'Perforación con Aire Reverso', 76, 78, '2018-06-26 19:56:59', '2018-06-26 19:57:47'),
(103, 'Si', 83, NULL, '2018-06-26 20:09:15', '2018-06-26 20:09:15'),
(104, 'No', 83, 84, '2018-06-26 20:09:15', '2018-06-26 20:13:23'),
(105, 'CS', 87, NULL, '2018-06-26 20:18:33', '2018-06-26 20:18:33'),
(106, 'CR', 87, 88, '2018-06-26 20:18:33', '2018-06-26 20:18:45'),
(107, 'NA', 87, NULL, '2018-06-26 20:18:33', '2018-06-26 20:18:33'),
(108, 'CS', 89, NULL, '2018-06-26 20:19:26', '2018-06-26 20:19:26'),
(109, 'CR', 89, 90, '2018-06-26 20:19:26', '2018-06-26 20:19:52'),
(110, 'NA', 89, NULL, '2018-06-26 20:19:26', '2018-06-26 20:19:26'),
(111, 'Perforación con Diamantina', 91, 92, '2018-06-26 20:21:05', '2018-06-26 20:21:18'),
(112, 'Perforación con Aire Reverso', 91, 93, '2018-06-26 20:21:05', '2018-06-26 20:21:31'),
(113, 'Si', 94, NULL, '2018-06-26 20:27:29', '2018-06-26 20:27:29'),
(114, 'No', 94, 95, '2018-06-26 20:27:29', '2018-06-26 20:27:46'),
(115, 'Si', 96, NULL, '2018-06-26 20:28:04', '2018-06-26 20:28:04'),
(116, 'No', 96, 97, '2018-06-26 20:28:04', '2018-06-26 20:28:16'),
(117, 'Si', 98, NULL, '2018-06-26 20:28:44', '2018-06-26 20:28:44'),
(118, 'No', 98, 99, '2018-06-26 20:28:44', '2018-06-26 20:29:03'),
(119, 'Si', 100, NULL, '2018-06-26 20:30:08', '2018-06-26 20:30:08'),
(120, 'No', 100, 101, '2018-06-26 20:30:08', '2018-06-26 20:30:19'),
(121, 'Si', 102, NULL, '2018-06-29 18:53:13', '2018-06-29 18:53:13'),
(122, 'No', 102, 103, '2018-06-29 18:53:13', '2018-06-29 18:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

CREATE TABLE `points` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, '1.1 Reunión SSO con EECC (Mensual en ...', '2018-06-27 16:01:00', '2018-06-27 16:10:07'),
(2, '2.1 Reunión Quincenal de SSO EMSA (dí...', '2018-06-27 16:01:13', '2018-06-27 16:01:13'),
(3, '3.1 Reunión interna SSO Distrito o Di...', '2018-06-27 16:01:19', '2018-06-27 16:01:19'),
(4, '4.1 Reunión de Revisión de Incidentes...', '2018-06-27 16:01:25', '2018-06-27 16:01:25'),
(5, '5.1 Reportabilidad de acción, condici...', '2018-06-27 16:01:31', '2018-06-27 16:01:31'),
(6, '6.1 Revisión de Controles Críticos se...', '2018-06-27 16:01:38', '2018-06-27 16:01:38'),
(7, '7.1 Comunicaciones Preventivas y reco...', '2018-06-27 16:01:43', '2018-06-27 16:01:43'),
(9, '8.1 Inspecciones de Condiciones de Tr...', '2018-06-27 16:11:24', '2018-06-27 16:11:24');

-- --------------------------------------------------------

--
-- Table structure for table `polls`
--

CREATE TABLE `polls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `polls`
--

INSERT INTO `polls` (`id`, `name`, `created_at`, `updated_at`) VALUES
(13, 'ECF1', '2018-06-22 03:17:45', '2018-06-22 03:17:45'),
(14, 'ART', '2018-06-26 18:40:11', '2018-06-26 18:40:11'),
(15, 'Reporte de seguridad', '2018-06-26 19:38:04', '2018-06-26 19:38:04'),
(16, 'Comunicación Preventiva', '2018-06-26 19:51:53', '2018-06-26 19:51:53'),
(17, 'IPER', '2018-06-26 20:01:56', '2018-06-26 20:01:56'),
(18, 'Elementos de Protección Personal', '2018-06-26 20:17:22', '2018-06-26 20:17:22');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'GERENTE TERRENO', '2018-06-06 04:00:00', NULL),
(2, 'GERENTE OFICINA', '2018-06-06 04:00:00', NULL),
(3, 'DIRECTOR TERRENO', '2018-06-06 04:00:00', NULL),
(4, 'DIRECTOR OFICINA', '2018-06-06 04:00:00', NULL),
(5, 'GEOLOGO JEFE TERRENO', '2018-06-06 04:00:00', NULL),
(6, 'GEOLOGO JEFE OFICINA', '2018-06-06 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `principal_scheduled_tasks`
--

CREATE TABLE `principal_scheduled_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `point_id` int(10) UNSIGNED NOT NULL,
  `task_date` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `principal_scheduled_tasks`
--

INSERT INTO `principal_scheduled_tasks` (`id`, `point_id`, `task_date`, `created_at`, `updated_at`) VALUES
(5, 9, '2012-12-12', '2018-06-27 21:10:28', '2018-06-27 21:10:28'),
(6, 7, '1996-12-23', '2018-06-27 21:21:55', '2018-06-27 21:21:55'),
(7, 1, '2012-12-23', '2018-06-27 21:25:45', '2018-06-27 21:25:45'),
(8, 3, '2012-12-12', '2018-06-27 21:26:15', '2018-06-27 21:26:15'),
(9, 5, '2008-11-11', '2018-06-27 21:31:13', '2018-06-28 22:15:40'),
(10, 5, '2018-08-30', '2018-06-29 18:45:56', '2018-06-29 18:49:05');

-- --------------------------------------------------------

--
-- Table structure for table `professionals`
--

CREATE TABLE `professionals` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `professionals`
--

INSERT INTO `professionals` (`id`, `username`, `password`, `name`, `last_name`, `email`, `token_id`, `created_at`, `updated_at`) VALUES
(3, 'sacevedo', 'sacevedo', 'SEBASTIAN ANDRES', 'ACEVEDO RODRIGUEZ', 'sebaacev@gmail.com', 6, '2018-06-19 19:11:23', '2018-06-19 19:11:23'),
(4, 'raguilera', 'raguilera', 'RODRIGO ANTONIO', 'AGUILERA PIÑEIRO', 'aguilera@gmail.com', 7, '2018-06-19 20:36:11', '2018-06-19 20:36:11'),
(5, 'cahumada', 'cahumada', 'CARLOS ANTONIO', 'AHUMADA HERRERA', 'carlos@gmail.com', 8, '2018-06-19 20:37:00', '2018-06-19 20:37:00');

-- --------------------------------------------------------

--
-- Table structure for table `professional_tokens`
--

CREATE TABLE `professional_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `professional_tokens`
--

INSERT INTO `professional_tokens` (`id`, `token`, `created_at`, `updated_at`) VALUES
(1, 'YHhdJsQR4DQ3clnHVTI8L80TBM0XvI8QCdD3MJPjVIUDpEWqz0aN4EDcfV6GPoDZgAfZMGI6wK1MrNkbUJDmHZWNvF', '2018-06-14 07:02:50', '2018-06-14 07:02:50'),
(2, 'K1zB4TZCoBwav5T66gl2Y4YNPqKrwEEJyXLmNF0N12SCECVwjcFmL8GNdEAV2pmpzs8ZfhZf7s1OXL0vypQnFMaHLC', '2018-06-14 07:04:40', '2018-06-14 07:04:40'),
(3, '73mcCxvELgSmyAOM2jdMqecWNtfUCMxEQuo8FEiVtUupxPan1Nup63Il4iJMGUcHu6EbBINbzIa5uOQCcyBhwfuoJL', '2018-06-14 07:07:31', '2018-06-14 07:07:31'),
(4, 'E9Or7TPlaVHswcyBatDAXqaMhkEMVG4bFZLrEX9xJIbYNGDVYCmFwibnpO7Ys3tEqb9ktAiiTAs6IFR4LZ6szxzCbL', '2018-06-19 01:37:35', '2018-06-19 01:37:35'),
(5, 'p0aSyhMtljTTzSXFommJimwYt52cL8C2Zs9cjvsxVY7inCB2TUKQ6IVIWUz6len4R8h8tclFCw6bGvS28BFIbuk2eH', '2018-06-19 02:39:28', '2018-06-19 02:39:28'),
(6, 'XvENVRgiHjyHAbIazJRH1kA5LUFQ6uC4ghxGL3IjfhG99EIOfbj96gs6EA5i2G6CTur1IDEQVukTQp3OoGUsSXAux9', '2018-06-19 19:11:23', '2018-06-19 19:11:23'),
(7, 'y4LpYiGK4zXjaU6cxPn2bTqooupWjz1PiXFjILZymhsFNmm0YGXFeou8tFIFOt8ygUBlb66kBwuRxAbW1VhzDrLWpL', '2018-06-19 20:36:11', '2018-06-19 20:36:11'),
(8, 'Qy7lehz1ChlW2Tq2AgZ9Xn06Is2nizoTbfTFvB2i5HFXvMKZrxCSNlflKvAru6AO05pA0EXoqyoQeoBVcUv6sk0YfK', '2018-06-19 20:37:00', '2018-06-19 20:37:00');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poll_id` int(10) UNSIGNED NOT NULL,
  `maintainer_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_by_option_id` int(10) DEFAULT NULL,
  `limit_date` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `turn_on_question_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `name`, `type`, `poll_id`, `maintainer_id`, `activated_by_option_id`, `limit_date`, `turn_on_question_id`, `created_at`, `updated_at`) VALUES
(41, 'B1 - ¿Existe procedimiento que regule el uso y uso y aplicación de los bloqueos, aislamiento y verificación de energía cero?', 'options', 13, NULL, NULL, NULL, NULL, '2018-06-22 03:17:57', '2018-06-26 20:26:19'),
(42, 'Responsable', 'professionals', 13, NULL, 68, NULL, NULL, '2018-06-22 03:18:08', '2018-06-22 03:18:08'),
(43, 'B2 C4 - ¿Los candados son personales y se encuentran con nombre y rut de la persona?', 'options', 13, NULL, NULL, NULL, NULL, '2018-06-22 04:07:24', '2018-06-22 04:07:24'),
(44, 'Gravedad', 'options', 13, NULL, 70, NULL, NULL, '2018-06-22 04:07:52', '2018-06-22 04:07:52'),
(45, 'Foto', 'photo', 13, NULL, 73, NULL, NULL, '2018-06-22 04:08:03', '2018-06-22 04:08:03'),
(46, 'B4 - ¿Existe un Libro de Aislación y Bloqueo?', 'options', 13, NULL, NULL, NULL, NULL, '2018-06-22 04:10:08', '2018-06-26 20:27:02'),
(47, 'Responsable', 'professionals', 13, NULL, 75, '2018-07-20', NULL, '2018-06-22 04:12:42', '2018-06-22 04:12:42'),
(48, 'Responsable', 'professionals', 13, NULL, 76, 'true', NULL, '2018-06-22 04:49:54', '2018-06-22 04:49:54'),
(49, 'B6 - ¿Los puntos de bloqueos se encuentran identificados?', 'options', 13, NULL, NULL, NULL, NULL, '2018-06-22 22:30:44', '2018-06-22 22:30:44'),
(50, 'Responsable', 'professionals', 13, NULL, 77, NULL, NULL, '2018-06-22 22:30:54', '2018-06-22 22:30:54'),
(51, 'Contrato', 'maintainers', 14, 5, NULL, NULL, NULL, '2018-06-26 18:42:00', '2018-06-26 18:42:00'),
(52, 'Tarea', 'options', 14, NULL, NULL, NULL, NULL, '2018-06-26 18:43:31', '2018-06-26 18:43:31'),
(53, '¿Se establece secuencia lógica del trabajo?', 'options', 14, NULL, NULL, NULL, NULL, '2018-06-26 18:43:48', '2018-06-26 18:43:48'),
(54, 'Responsable', 'professionals', 14, NULL, 91, 'true', NULL, '2018-06-26 18:44:15', '2018-06-26 18:44:15'),
(55, 'Tipo Sondaje', 'options', 14, NULL, NULL, NULL, NULL, '2018-06-26 19:07:07', '2018-06-26 19:07:07'),
(56, 'Perforación con Diamantina', 'maintainers', 14, 6, 92, NULL, NULL, '2018-06-26 19:08:03', '2018-06-26 19:08:03'),
(57, 'Perforación con Aire Reverso', 'maintainers', 14, 7, 93, NULL, NULL, '2018-06-26 19:08:31', '2018-06-26 19:08:31'),
(58, 'Mantiene el área ordenada, limpia y despejada.', 'options', 14, NULL, NULL, NULL, NULL, '2018-06-26 19:15:29', '2018-06-26 19:15:29'),
(59, 'Barrera', 'maintainers', 14, 8, 95, NULL, NULL, '2018-06-26 19:15:42', '2018-06-26 19:15:42'),
(60, 'Contrato', 'maintainers', 15, 5, NULL, NULL, NULL, '2018-06-26 19:38:28', '2018-06-26 19:38:28'),
(65, 'Tipo Sondaje', 'options', 15, NULL, NULL, NULL, NULL, '2018-06-26 19:43:22', '2018-06-26 19:43:22'),
(66, 'Perforación con Diamantina', 'maintainers', 15, 6, 97, NULL, NULL, '2018-06-26 19:43:45', '2018-06-26 19:43:45'),
(67, 'Perforación con Aire Reverso', 'maintainers', 15, 7, 98, NULL, NULL, '2018-06-26 19:44:02', '2018-06-26 19:44:02'),
(68, 'ECF', 'maintainers', 15, 10, NULL, NULL, NULL, '2018-06-26 19:45:04', '2018-06-26 19:45:04'),
(69, 'Nivel de criticidad', 'maintainers', 15, 11, NULL, NULL, NULL, '2018-06-26 19:46:43', '2018-06-26 19:46:43'),
(70, 'Responsable', 'professionals', 15, NULL, NULL, 'true', NULL, '2018-06-26 19:47:02', '2018-06-26 19:47:02'),
(71, 'Foto/evidencia', 'photo', 15, NULL, NULL, NULL, NULL, '2018-06-26 19:47:18', '2018-06-26 19:47:18'),
(72, 'Contrato', 'maintainers', 16, 5, NULL, NULL, NULL, '2018-06-26 19:52:25', '2018-06-26 19:52:25'),
(74, 'Número de participantes', 'maintainers', 16, 2, NULL, NULL, NULL, '2018-06-26 19:53:58', '2018-06-26 19:53:58'),
(75, 'Foto', 'photo', 16, NULL, NULL, NULL, NULL, '2018-06-26 19:54:09', '2018-06-26 19:54:09'),
(76, 'Tipo Sondaje', 'options', 16, NULL, NULL, NULL, NULL, '2018-06-26 19:56:59', '2018-06-26 19:56:59'),
(77, 'Perforación con Diamantina', 'maintainers', 16, 6, 99, NULL, NULL, '2018-06-26 19:57:35', '2018-06-26 19:57:35'),
(78, 'Perforación con Aire Reverso', 'maintainers', 16, 7, 100, NULL, NULL, '2018-06-26 19:57:47', '2018-06-26 19:57:47'),
(79, 'Contrato', 'maintainers', 17, 5, NULL, NULL, NULL, '2018-06-26 20:04:07', '2018-06-26 20:04:07'),
(80, 'Tarea', 'maintainers', 17, 12, NULL, NULL, 107, '2018-06-26 20:06:49', '2018-07-01 00:51:34'),
(82, 'Nivel de criticidad', 'maintainers', 17, 11, 102, NULL, NULL, '2018-06-26 20:07:32', '2018-06-26 20:07:32'),
(83, '¿Es rutinaria?', 'options', 17, NULL, NULL, NULL, NULL, '2018-06-26 20:09:15', '2018-06-26 20:09:15'),
(84, 'Nivel de criticidad', 'maintainers', 17, 11, 104, NULL, NULL, '2018-06-26 20:13:23', '2018-06-26 20:13:23'),
(85, 'Contrato', 'maintainers', 18, 5, NULL, NULL, NULL, '2018-06-26 20:17:38', '2018-06-26 20:17:38'),
(86, 'Tarea', 'maintainers', 18, 12, NULL, NULL, NULL, '2018-06-26 20:17:49', '2018-06-26 20:17:49'),
(87, 'Utiliza protección básica y/o específica y en buen estado, según la tarea.', 'options', 18, NULL, NULL, NULL, NULL, '2018-06-26 20:18:33', '2018-06-26 20:18:33'),
(88, 'Barrera', 'maintainers', 18, 8, 106, NULL, NULL, '2018-06-26 20:18:45', '2018-06-26 20:18:45'),
(89, 'Utiliza EPP adecuado y en buen estado para manipular sustancias peligrosas.', 'options', 18, NULL, NULL, NULL, NULL, '2018-06-26 20:19:26', '2018-06-26 20:19:26'),
(90, 'Barrera', 'maintainers', 18, 8, 109, NULL, NULL, '2018-06-26 20:19:52', '2018-06-26 20:19:52'),
(91, 'Tipo Sondaje', 'options', 18, NULL, NULL, NULL, NULL, '2018-06-26 20:21:05', '2018-06-26 20:21:05'),
(92, 'Perforación con Diamantina', 'maintainers', 18, 6, 111, NULL, NULL, '2018-06-26 20:21:18', '2018-06-26 20:21:18'),
(93, 'Perforación con Aire Reverso', 'maintainers', 18, 7, 112, NULL, NULL, '2018-06-26 20:21:31', '2018-06-26 20:21:31'),
(94, 'C2 - Los sistemas hidráulicos y neumáticos, ¿cuentan con manómetros y válvulas de descarga de presión y/o de seguridad?', 'options', 13, NULL, NULL, NULL, NULL, '2018-06-26 20:27:29', '2018-06-26 20:27:29'),
(95, 'Responsable', 'professionals', 13, NULL, 114, 'true', NULL, '2018-06-26 20:27:46', '2018-06-26 20:27:46'),
(96, 'C3 - ¿Están adulteradas tarjetas, pinzas o puntos de bloqueo?', 'options', 13, NULL, NULL, NULL, NULL, '2018-06-26 20:28:04', '2018-06-26 20:28:04'),
(97, 'Responsable', 'professionals', 13, NULL, 116, 'true', NULL, '2018-06-26 20:28:16', '2018-06-26 20:28:16'),
(98, 'C4 C6 - ¿Las tarjetas de identificación personal, indican el nombre, empresa, cargo, rut, teléfono de contacto y área del trabajador y se encuentran en buen estado?', 'options', 13, NULL, NULL, NULL, NULL, '2018-06-26 20:28:44', '2018-06-26 20:28:44'),
(99, 'Responsable', 'professionals', 13, NULL, 118, 'true', NULL, '2018-06-26 20:29:03', '2018-06-26 20:29:03'),
(100, 'C7 - Los mandos remotos de operación de equipos (camiones pluma, polipastos, etc.) cuentan con sistema que permita bloquearse?', 'options', 13, NULL, NULL, NULL, NULL, '2018-06-26 20:30:08', '2018-06-26 20:30:08'),
(101, 'Responsable', 'professionals', 13, NULL, 120, 'true', NULL, '2018-06-26 20:30:19', '2018-06-26 20:30:19'),
(102, 'test', 'options', 17, NULL, NULL, NULL, NULL, '2018-06-29 18:53:13', '2018-06-29 18:53:13'),
(103, 'foto', 'photo', 17, NULL, 122, NULL, NULL, '2018-06-29 18:53:23', '2018-06-29 18:53:23'),
(107, 'Responsable', 'professionals', 17, NULL, NULL, NULL, NULL, '2018-07-01 00:51:34', '2018-07-01 00:51:34');

-- --------------------------------------------------------

--
-- Table structure for table `responses`
--

CREATE TABLE `responses` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED DEFAULT NULL,
  `maintainer_id` int(10) UNSIGNED DEFAULT NULL,
  `supervisor_id` int(10) UNSIGNED DEFAULT NULL,
  `element_id` int(10) UNSIGNED DEFAULT NULL,
  `professional_id` int(10) UNSIGNED DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `limit_date` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_question` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `answer_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `responses`
--

INSERT INTO `responses` (`id`, `question_id`, `option_id`, `maintainer_id`, `supervisor_id`, `element_id`, `professional_id`, `photo`, `type`, `content`, `limit_date`, `father_question`, `created_at`, `updated_at`, `answer_id`) VALUES
(98, 41, 68, NULL, 3, NULL, NULL, NULL, 'options', 'No', NULL, 'null', '2018-06-24 19:58:44', '2018-06-24 19:58:44', NULL),
(99, 42, NULL, NULL, 3, NULL, 3, NULL, 'professionals', 'SEBASTIAN ANDRES ACEVEDO RODRIGUEZ', NULL, 'null', '2018-06-24 19:58:46', '2018-06-24 19:58:46', NULL),
(100, 41, 68, NULL, 3, NULL, NULL, NULL, 'options', 'No', NULL, 'B1 - ¿Existe procedimiento que regule el uso y aplicación de los bloqueos, aislamiento y verificación de energía cero?', '2018-06-24 20:09:48', '2018-06-24 20:09:48', NULL),
(101, 42, NULL, NULL, 3, NULL, 3, NULL, 'professionals', 'SEBASTIAN ANDRES ACEVEDO RODRIGUEZ', 'null', NULL, '2018-06-24 20:09:48', '2018-06-24 20:09:48', NULL),
(102, 41, 68, NULL, 3, NULL, NULL, NULL, 'options', 'No', NULL, 'B1 - ¿Existe procedimiento que regule el uso y aplicación de los bloqueos, aislamiento y verificación de energía cero?', '2018-06-24 20:16:09', '2018-06-24 20:16:09', NULL),
(103, 42, NULL, NULL, 3, NULL, 3, NULL, 'professionals', 'SEBASTIAN ANDRES ACEVEDO RODRIGUEZ', 'null', 'B1 - ¿Existe procedimiento que regule el uso y aplicación de los bloqueos, aislamiento y verificación de energía cero?', '2018-06-24 20:16:11', '2018-06-24 20:16:11', NULL),
(107, 48, NULL, NULL, 3, NULL, 4, NULL, 'professionals', 'RODRIGO ANTONIO AGUILERA PIÑEIRO', NULL, 'JAJAJAJAJA', '2018-06-25 19:01:35', '2018-06-25 19:01:35', NULL),
(108, 43, 70, NULL, 3, NULL, NULL, NULL, 'options', 'No', NULL, 'B2 C4 - ¿Los candados son personales y se encuentran con nombre y rut de la persona?', '2018-06-25 19:03:25', '2018-06-25 19:03:25', NULL),
(109, 44, 73, NULL, 3, NULL, NULL, NULL, 'options', 'Nivel Alto', NULL, 'B2 C4 - ¿Los candados son personales y se encuentran con nombre y rut de la persona?', '2018-06-25 19:03:26', '2018-06-25 19:03:26', NULL),
(110, 46, 76, NULL, 3, NULL, NULL, NULL, 'options', 'No', NULL, 'B3 - ¿Los candados son personales y se encuentran con nombre y rut de la persona?', '2018-06-25 19:03:26', '2018-06-25 19:03:26', NULL),
(111, 48, NULL, NULL, 3, NULL, 3, NULL, 'professionals', 'SEBASTIAN ANDRES ACEVEDO RODRIGUEZ', '2018-5-27', 'B3 - ¿Los candados son personales y se encuentran con nombre y rut de la persona?', '2018-06-25 19:03:29', '2018-06-25 19:03:29', NULL),
(112, 60, NULL, 5, 3, 22, NULL, NULL, 'maintainers', 'Exploradora', NULL, 'Contrato', '2018-06-26 19:51:17', '2018-06-26 19:51:17', NULL),
(113, 65, 97, NULL, 3, NULL, NULL, NULL, 'options', 'Perforación con Diamantina', NULL, 'Tipo Sondaje', '2018-06-26 19:51:17', '2018-06-26 19:51:17', NULL),
(114, 66, NULL, 6, 3, 25, NULL, NULL, 'maintainers', '1.- Reconocimiento caminos sectores plataformas', NULL, 'Tipo Sondaje', '2018-06-26 19:51:18', '2018-06-26 19:51:18', NULL),
(115, 68, NULL, 10, 3, 130, NULL, NULL, 'maintainers', 'ECF1', NULL, 'ECF', '2018-06-26 19:51:18', '2018-06-26 19:51:18', NULL),
(116, 69, NULL, 11, 3, 139, NULL, NULL, 'maintainers', 'OM', NULL, 'Nivel de criticidad', '2018-06-26 19:51:18', '2018-06-26 19:51:18', NULL),
(117, 70, NULL, NULL, 3, NULL, 5, NULL, 'professionals', 'CARLOS ANTONIO AHUMADA HERRERA', '2018-5-28', 'Responsable', '2018-06-26 19:51:18', '2018-06-26 19:51:18', NULL),
(118, 71, NULL, NULL, 3, NULL, NULL, '2018626155119photo.JPG', 'photo', 'Foto', NULL, NULL, '2018-06-26 19:51:19', '2018-06-26 19:51:19', NULL),
(119, 96, 115, NULL, 3, NULL, NULL, NULL, 'options', 'Si', NULL, 'C3 - ¿Están adulteradas tarjetas, pinzas o puntos de bloqueo?', '2018-06-29 18:58:09', '2018-06-29 18:58:09', NULL),
(120, 52, 81, NULL, 3, NULL, NULL, NULL, 'options', 'Almacenamiento de Materiales', NULL, 'Tarea', '2018-06-30 21:05:38', '2018-06-30 21:05:38', NULL),
(121, 46, 76, NULL, 3, NULL, NULL, NULL, 'options', 'No', NULL, 'B4 - ¿Existe un Libro de Aislación y Bloqueo?', '2018-06-30 21:15:48', '2018-06-30 21:15:48', NULL),
(122, 48, NULL, NULL, 3, NULL, 4, NULL, 'professionals', 'RODRIGO ANTONIO AGUILERA PIÑEIRO', '2018-5-21', 'B4 - ¿Existe un Libro de Aislación y Bloqueo?', '2018-06-30 21:15:49', '2018-06-30 21:15:49', NULL),
(123, 43, 69, NULL, 3, NULL, NULL, NULL, 'options', 'Si', NULL, 'B2 C4 - ¿Los candados son personales y se encuentran con nombre y rut de la persona?', '2018-06-30 21:15:49', '2018-06-30 21:15:49', NULL),
(124, 41, 67, NULL, 3, NULL, NULL, NULL, 'options', 'Si', NULL, 'B1 - ¿Existe procedimiento que regule el uso y uso y aplicación de los bloqueos, aislamiento y verificación de energía cero?', '2018-06-30 21:15:49', '2018-06-30 21:15:49', NULL),
(125, 80, NULL, 12, 3, 148, NULL, NULL, 'maintainers', 'Almacenamiento de Materiales', NULL, 'Tarea', '2018-07-01 00:52:02', '2018-07-01 00:52:02', NULL),
(126, 107, NULL, NULL, 3, NULL, 5, NULL, 'professionals', 'CARLOS ANTONIO AHUMADA HERRERA', 'null', 'Tarea', '2018-07-01 00:52:02', '2018-07-01 00:52:02', NULL),
(127, 80, NULL, 12, 3, 152, NULL, NULL, 'maintainers', 'Recuperación de barras', NULL, 'Tarea', '2018-07-01 00:52:37', '2018-07-01 00:52:37', NULL),
(128, 107, NULL, NULL, 3, NULL, 3, NULL, 'professionals', 'SEBASTIAN ANDRES ACEVEDO RODRIGUEZ', 'null', 'Tarea', '2018-07-01 00:52:38', '2018-07-01 00:52:38', NULL),
(129, 80, NULL, 12, 3, 150, NULL, NULL, 'maintainers', 'Perforación', NULL, 'Tarea', '2018-07-01 00:57:00', '2018-07-01 00:57:00', NULL),
(130, 107, NULL, NULL, 3, NULL, 5, NULL, 'professionals', 'CARLOS ANTONIO AHUMADA HERRERA', 'null', 'Tarea', '2018-07-01 00:57:00', '2018-07-01 00:57:00', NULL),
(131, 80, NULL, 12, 3, 148, NULL, NULL, 'maintainers', 'Descarga de Materiales', NULL, 'Tarea', '2018-07-03 04:21:01', '2018-07-03 04:21:01', NULL),
(132, 107, NULL, NULL, 3, NULL, 5, NULL, 'professionals', 'CARLOS ANTONIO AHUMADA HERRERA', 'null', 'Tarea', '2018-07-03 04:21:01', '2018-07-03 04:21:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scheduled_tasks`
--

CREATE TABLE `scheduled_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supervisor_id` int(10) UNSIGNED DEFAULT NULL,
  `professional_id` int(10) UNSIGNED DEFAULT NULL,
  `level_id` int(10) UNSIGNED DEFAULT NULL,
  `level` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comentary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `point_id` int(10) UNSIGNED DEFAULT NULL,
  `principal_scheduled_task_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scheduled_tasks`
--

INSERT INTO `scheduled_tasks` (`id`, `type`, `supervisor_id`, `professional_id`, `level_id`, `level`, `photo`, `comentary`, `created_at`, `updated_at`, `point_id`, `principal_scheduled_task_id`) VALUES
(21, 'supervisors', 3, NULL, NULL, 'Medio', '2018629144834photo.JPG', 'djfjfjglgl', '2018-06-29 18:48:07', '2018-06-29 18:48:34', NULL, 10),
(22, 'professionals', NULL, 4, NULL, NULL, NULL, NULL, '2018-06-29 18:49:05', '2018-06-29 18:49:05', NULL, 10);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `level_id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comentary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supervisors`
--

CREATE TABLE `supervisors` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enable` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_id` int(10) UNSIGNED NOT NULL,
  `token_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supervisors`
--

INSERT INTO `supervisors` (`id`, `username`, `password`, `name`, `last_name`, `email`, `enable`, `position_id`, `token_id`, `created_at`, `updated_at`) VALUES
(3, 'aaguero', 'aaguero', 'Alvaro', 'Aguero', 'aguero.vera.alvaro@gmail.com', 'true', 1, 3, '2018-06-19 19:12:27', '2018-06-19 19:12:27'),
(4, 'max', 'max', 'Max', 'Mujica', 'maxchileasesor@gmail.com', 'true', 5, 4, '2018-06-27 21:37:14', '2018-06-27 21:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `supervisor_tokens`
--

CREATE TABLE `supervisor_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supervisor_tokens`
--

INSERT INTO `supervisor_tokens` (`id`, `token`, `created_at`, `updated_at`) VALUES
(1, 'xUwHSn9RzlMBfp6nBdP5j99439WYVtPTtN9GrclwRAIvEVnrYMvTXoTFn6dgiABZiV9PnmsLSVfhmNrIQxGrsq5XMI', '2018-06-14 07:07:13', '2018-06-14 07:07:13'),
(2, 'LUqWvtzCXK39bL2svzgtZlM1NfHnEFUY9SnF39W1B3AUsRfEtPkShwpzSIGiR9ZyLKxUBVR7UY9iNQrW1YT74uoNo3', '2018-06-14 07:51:12', '2018-06-14 07:51:12'),
(3, 'OpReNZA1j8U0D5tJZGIazUXCxtzfBhLSVRmQ6AVfRMOQwh1tckT3SkRcEdbvEKnOuih3LWT3NGsnE9FmBZF1vpykcU', '2018-06-19 19:12:27', '2018-06-19 19:12:27'),
(4, 'CvWya8zMv2QpT0AaZ26f51G2nuXWFq95XDj1w0er9YBQH6FcDkmo9EiJZf5MXxTyE76CsL8IYBMcbyhQrZNBN9JbMX', '2018-06-27 21:37:14', '2018-06-27 21:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `response_id` int(10) UNSIGNED NOT NULL,
  `professional_id` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comentary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `response_id`, `professional_id`, `state`, `photo`, `comentary`, `created_at`, `updated_at`) VALUES
(5, 103, 3, 'Alto', '20186261428485photo.JPG', 'xjfjfjfkfk', '2018-06-24 20:16:11', '2018-06-26 18:28:48'),
(6, 107, 4, 'Sin estado aún.', NULL, NULL, '2018-06-25 19:01:35', '2018-06-25 19:01:35'),
(7, 111, 3, 'Completado', '2018625181646/Applications/XAMPP/xamppfiles/temp/phpiCvnaPphoto.JPG', 'listo', '2018-06-25 19:03:29', '2018-06-25 22:16:46'),
(8, 117, 5, 'Sin estado aún.', NULL, NULL, '2018-06-26 19:51:18', '2018-06-26 19:51:18'),
(9, 122, 4, 'Sin estado aún.', NULL, NULL, '2018-06-30 21:15:49', '2018-06-30 21:15:49'),
(10, 126, 5, 'Sin estado aún.', NULL, NULL, '2018-07-01 00:52:02', '2018-07-01 00:52:02'),
(11, 128, 3, 'Sin estado aún.', NULL, NULL, '2018-07-01 00:52:38', '2018-07-01 00:52:38'),
(12, 130, 5, 'Sin estado aún.', NULL, NULL, '2018-07-01 00:57:00', '2018-07-01 00:57:00'),
(13, 132, 5, 'Sin estado aún.', NULL, NULL, '2018-07-03 04:21:01', '2018-07-03 04:21:01');

-- --------------------------------------------------------

--
-- Table structure for table `tokens_fcm_professional`
--

CREATE TABLE `tokens_fcm_professional` (
  `id` int(10) UNSIGNED NOT NULL,
  `professional_id` int(10) UNSIGNED NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imei` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tokens_fcm_professional`
--

INSERT INTO `tokens_fcm_professional` (`id`, `professional_id`, `token`, `imei`, `created_at`, `updated_at`) VALUES
(8, 4, 'cgywoeDI8Ew:APA91bFzcpSLhhysi4CWNPvbDAVGhGiST7TRY3AKRSYpQpg5r_0znLgDGCaDRLrrg1fDGi9D41vO1L_JIV7lJa4WfHvXm8NAHNtzTp4QbfPPE4yBPy_awB9dr8u0A0tiNj5knZ4-Vut4u3vSjSaIAjUkXrfYzL8hSg', 'c30a9da69eabd61d', '2018-06-28 23:07:41', '2018-06-28 23:07:41'),
(9, 3, 'dCKd_lyzhdo:APA91bHc7RQRDzkNZSyF16rY3tYKydyY44D-vAY2HPQHG2OS-Waq1JmTi1WfebXVFnTEPCAwxKyFQDsOqxR3BEMNmIqr7qn9WLJoLEQkT0Pf8e2cycFCp6TcW7ri3V8ddvDXckRBEmusChXmI4sUG6YprM1v2XQzZw', '270cc296f5fba80b', '2018-06-29 04:36:31', '2018-06-29 04:36:31');

-- --------------------------------------------------------

--
-- Table structure for table `tokens_fcm_supervisor`
--

CREATE TABLE `tokens_fcm_supervisor` (
  `id` int(10) UNSIGNED NOT NULL,
  `supervisor_id` int(10) UNSIGNED NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imei` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tokens_fcm_supervisor`
--

INSERT INTO `tokens_fcm_supervisor` (`id`, `supervisor_id`, `token`, `imei`, `created_at`, `updated_at`) VALUES
(4, 3, 'fFJyNUqXmFU:APA91bE9_oPVF1Rjh8nMYvNGBirw_ye2MP8mn5WJnHCauccYfR1ur36DMimTHsT_pMUPF5L-0d_Qjvx6lokrFD-Or6h1T3SDsCBy58XPVNAMUfykEAgZVNwJTFLUeajN1tNK2-UmytSqxLwkBEQfxSJKO_Doyzm91Q', 'c30a9da69eabd61d', '2018-06-23 21:19:03', '2018-06-23 21:19:03'),
(5, 3, '222222', '3', '2018-06-23 23:15:52', '2018-06-23 23:18:48'),
(6, 3, 'c-e4y-wo6K4:APA91bF_-4JnzSPgbManX4I3Pj2CzYYTL9CudnLCB1ZvCPrf-qVREdZtAyKew8s3QLT0rIKAZYEIvqZ3EeqArxCYOAuwtBoSSkWLrEZhseHMLbw6EJTbi9axyuauq7dODvv6w_t4qKrvRPOqAQ0HLs1LgocXQFuKqQ', '270cc296f5fba80b', '2018-06-25 19:02:26', '2018-06-25 19:02:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Alvaro', 'aguero.vera.alvaro@gmail.com', '$2y$10$d39YRSWk35DG8Iy1T2MPOeu9o7C1L3zoBdwfXupx6Z8EKFFFt23Ey', 'QGALcyjntkkMkkXDlcQRH5l2t1qVqbGpr6soKGVjdKAq9OzrZ8ma69RP4WEC', '2018-06-07 00:34:40', '2018-06-07 00:34:40'),
(2, 'Max', 'maxchileasesor@gmail.com', '$2y$10$giFNv11qJ.fyJ.HYi8JrrenGGQRV.EAGm20UKMF0UJIG189d2Lase', NULL, '2018-06-07 23:01:09', '2018-06-07 23:01:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elements_maintainer_id_foreign` (`maintainer_id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maintainers`
--
ALTER TABLE `maintainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_question_id_foreign` (`question_id`);

--
-- Indexes for table `points`
--
ALTER TABLE `points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `principal_scheduled_tasks`
--
ALTER TABLE `principal_scheduled_tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `principal_scheduled_tasks_point_id_foreign` (`point_id`);

--
-- Indexes for table `professionals`
--
ALTER TABLE `professionals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `professionals_token_id_foreign` (`token_id`);

--
-- Indexes for table `professional_tokens`
--
ALTER TABLE `professional_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_poll_id_foreign` (`poll_id`),
  ADD KEY `questions_maintainer_id_foreign` (`maintainer_id`);

--
-- Indexes for table `responses`
--
ALTER TABLE `responses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `responses_question_id_foreign` (`question_id`),
  ADD KEY `responses_option_id_foreign` (`option_id`),
  ADD KEY `responses_maintainer_id_foreign` (`maintainer_id`),
  ADD KEY `responses_supervisor_id_foreign` (`supervisor_id`),
  ADD KEY `responses_element_id_foreign` (`element_id`),
  ADD KEY `responses_professional_id_foreign` (`professional_id`),
  ADD KEY `responses_answer_id_foreign` (`answer_id`);

--
-- Indexes for table `scheduled_tasks`
--
ALTER TABLE `scheduled_tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scheduled_tasks_supervisor_id_foreign` (`supervisor_id`),
  ADD KEY `scheduled_tasks_professional_id_foreign` (`professional_id`),
  ADD KEY `scheduled_tasks_level_id_foreign` (`level_id`),
  ADD KEY `scheduled_tasks_point_id_foreign` (`point_id`),
  ADD KEY `scheduled_tasks_principal_scheduled_task_id_foreign` (`principal_scheduled_task_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `states_task_id_foreign` (`task_id`),
  ADD KEY `states_level_id_foreign` (`level_id`);

--
-- Indexes for table `supervisors`
--
ALTER TABLE `supervisors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supervisors_position_id_foreign` (`position_id`),
  ADD KEY `supervisors_token_id_foreign` (`token_id`);

--
-- Indexes for table `supervisor_tokens`
--
ALTER TABLE `supervisor_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_response_id_foreign` (`response_id`),
  ADD KEY `tasks_professional_id_foreign` (`professional_id`);

--
-- Indexes for table `tokens_fcm_professional`
--
ALTER TABLE `tokens_fcm_professional`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tokens_fcm_professional_professional_id_foreign` (`professional_id`);

--
-- Indexes for table `tokens_fcm_supervisor`
--
ALTER TABLE `tokens_fcm_supervisor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tokens_fcm_supervisor_supervisor_id_foreign` (`supervisor_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `elements`
--
ALTER TABLE `elements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `maintainers`
--
ALTER TABLE `maintainers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `points`
--
ALTER TABLE `points`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `polls`
--
ALTER TABLE `polls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `principal_scheduled_tasks`
--
ALTER TABLE `principal_scheduled_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `professionals`
--
ALTER TABLE `professionals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `professional_tokens`
--
ALTER TABLE `professional_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `responses`
--
ALTER TABLE `responses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `scheduled_tasks`
--
ALTER TABLE `scheduled_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supervisors`
--
ALTER TABLE `supervisors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `supervisor_tokens`
--
ALTER TABLE `supervisor_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tokens_fcm_professional`
--
ALTER TABLE `tokens_fcm_professional`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tokens_fcm_supervisor`
--
ALTER TABLE `tokens_fcm_supervisor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `elements`
--
ALTER TABLE `elements`
  ADD CONSTRAINT `elements_maintainer_id_foreign` FOREIGN KEY (`maintainer_id`) REFERENCES `maintainers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `principal_scheduled_tasks`
--
ALTER TABLE `principal_scheduled_tasks`
  ADD CONSTRAINT `principal_scheduled_tasks_point_id_foreign` FOREIGN KEY (`point_id`) REFERENCES `points` (`id`);

--
-- Constraints for table `professionals`
--
ALTER TABLE `professionals`
  ADD CONSTRAINT `professionals_token_id_foreign` FOREIGN KEY (`token_id`) REFERENCES `professional_tokens` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_maintainer_id_foreign` FOREIGN KEY (`maintainer_id`) REFERENCES `maintainers` (`id`),
  ADD CONSTRAINT `questions_poll_id_foreign` FOREIGN KEY (`poll_id`) REFERENCES `polls` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `responses`
--
ALTER TABLE `responses`
  ADD CONSTRAINT `responses_answer_id_foreign` FOREIGN KEY (`answer_id`) REFERENCES `answers` (`id`),
  ADD CONSTRAINT `responses_element_id_foreign` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`),
  ADD CONSTRAINT `responses_maintainer_id_foreign` FOREIGN KEY (`maintainer_id`) REFERENCES `maintainers` (`id`),
  ADD CONSTRAINT `responses_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`),
  ADD CONSTRAINT `responses_professional_id_foreign` FOREIGN KEY (`professional_id`) REFERENCES `professionals` (`id`),
  ADD CONSTRAINT `responses_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `responses_supervisor_id_foreign` FOREIGN KEY (`supervisor_id`) REFERENCES `supervisors` (`id`);

--
-- Constraints for table `scheduled_tasks`
--
ALTER TABLE `scheduled_tasks`
  ADD CONSTRAINT `scheduled_tasks_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`),
  ADD CONSTRAINT `scheduled_tasks_point_id_foreign` FOREIGN KEY (`point_id`) REFERENCES `points` (`id`),
  ADD CONSTRAINT `scheduled_tasks_principal_scheduled_task_id_foreign` FOREIGN KEY (`principal_scheduled_task_id`) REFERENCES `principal_scheduled_tasks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `scheduled_tasks_professional_id_foreign` FOREIGN KEY (`professional_id`) REFERENCES `professionals` (`id`),
  ADD CONSTRAINT `scheduled_tasks_supervisor_id_foreign` FOREIGN KEY (`supervisor_id`) REFERENCES `supervisors` (`id`);

--
-- Constraints for table `states`
--
ALTER TABLE `states`
  ADD CONSTRAINT `states_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`),
  ADD CONSTRAINT `states_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`);

--
-- Constraints for table `supervisors`
--
ALTER TABLE `supervisors`
  ADD CONSTRAINT `supervisors_position_id_foreign` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`),
  ADD CONSTRAINT `supervisors_token_id_foreign` FOREIGN KEY (`token_id`) REFERENCES `supervisor_tokens` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_professional_id_foreign` FOREIGN KEY (`professional_id`) REFERENCES `professionals` (`id`),
  ADD CONSTRAINT `tasks_response_id_foreign` FOREIGN KEY (`response_id`) REFERENCES `responses` (`id`);

--
-- Constraints for table `tokens_fcm_professional`
--
ALTER TABLE `tokens_fcm_professional`
  ADD CONSTRAINT `tokens_fcm_professional_professional_id_foreign` FOREIGN KEY (`professional_id`) REFERENCES `professionals` (`id`);

--
-- Constraints for table `tokens_fcm_supervisor`
--
ALTER TABLE `tokens_fcm_supervisor`
  ADD CONSTRAINT `tokens_fcm_supervisor_supervisor_id_foreign` FOREIGN KEY (`supervisor_id`) REFERENCES `supervisors` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
