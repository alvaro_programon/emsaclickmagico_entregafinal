@extends('layouts.app')

@section('content')
	<div class="container">
    
    <?php if($levels!=null){ ?>
		<div class="panel panel-default">
		  <br>
		  <div class="panel-heading">Estados : {{$levels->count()}}</div>
		  <br>
		  <table class="table"> 
		  	<thead> 
		  	<tr> 
		  	<th>Nombre</th>
		  	<th>Fecha creación</th> 
		  	</tr> 
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($levels as $level)
		  	<tr> 
			  	<td>{{$level->name}}</td>
			  	<td>{{$level->created_at}}</td>
		  	</tr> 
		  	 @endforeach
		  	</tbody> 
		  	</table>
		</div>
		<?php } ?>

	</div>
@stop