@extends('layouts.app')

@section('content')
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

		<b><h2>Detalle de Pregunta
			@if(is_null($question->activated_by_option_id))
			<a href="{{ url('detailPoll') }}/{{$question->poll->id}}" class="btn btn-outline-info" role="button">Volver</a>
			@else
			<a href="{{ url('detailQuestion') }}/{{$question->get_id_parent_question($question->activated_by_option_id)}}" class="btn btn-outline-info" role="button">Volver</a>
			@endif
			
			<a href="{{ url('updateQuestion') }}/{{$question->id}}" class="btn btn-outline-primary" role="button">Editar Pregunta</a>

			@if($question->type=='professionals' || $question->type=='maintainers' || $question->type=='options')
				<a href="{{ url('addActionFromQuestion') }}/{{$question->id}}" class="btn btn-warning" role="button">Agregar Acción +</a>
			@endif

		</h2></b><br>
		<b>Pregunta:</b> <h4>{{$question->name}}</h4>
		<b>Tipo:</b> 
		@if($question->type=='description')
			<p class="text-primary">Pregunta tipo Descripción.</p>
		@elseif($question->type=='photo')
			<p class="text-primary">Pregunta tipo Foto.</p>
		@elseif($question->type=='professionals')
			<p class="text-primary">Pregunta que utiliza los datos de los Profesionales.</p>
		@elseif($question->type=='options')
			<div class="panel-heading">
				<p class="text-primary">Pregunta con {{$question->options->count()}} opción(es).</p>
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addOptionModal">
			  Agregar Opción +
				</button>
			</div>
			<br>
		  	<table class="table"> 
		  	<thead> 
		  	<tr> 
			  	<th>Opción</th>
			  	<th>Modificar</th>
				<th>Borrar -</th>
				<th>Agregar Acción +</th>
		  	</tr> 
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($question->options as $option)
		  	<tr> 
			  	<td>{{$option->name}}</td>
			  	<td>
<a href="{{ url('updateOption') }}/{{$option->id}}" class="btn btn-outline-primary btn-sm" role="button">Editar</a></td>
	    <td>
<a href="{{ url('deleteOption') }}/{{$option->id}}" onclick="
return confirm('Está seguro que desea borrar esta Opción?')" class="btn btn-outline-danger btn-sm" role="button">Eliminar</a></td>
<td>
	<?php $parent_question=$option->has_question($option->id); ?>
	@if(is_null($parent_question))
		<a href="{{ url('addAction') }}/{{$option->id}}" class="btn btn-outline-success btn-sm" role="button">Agregar Acción</a>
	@else
		<a href="{{ url('detailQuestion') }}/{{$parent_question->id}}" class="btn btn-outline-primary btn-sm" role="button">Editar Acción</a>
	@endif
</td>
		  	</tr> 
		  	 @endforeach
		  	</tbody> 
		  	</table>

		  	<!-- Modal -->
			<div class="modal fade" id="addOptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Agregar Opción</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      {!! Form::open([ 'route' => ['addOptionPost',$question->id], 'method' => 'POST']) !!}

			      <div class="modal-body">
			      	<b>{!! Form::label('Opción', 'Opción', ['for' => 'Opción'] ) !!}</b>
					</br>
					{!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'opción...'] ) !!}
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			        <button type="submit" class="btn btn-primary">Agregar +</button>
			      </div>

			      {!! Form::close() !!}
			    </div>
			  </div>
			</div>
		@else
			<br>{!! Form::open([ 'route' => ['updateMaintainerQuestionPost',$question->id], 'method' => 'POST']) !!}

			{!! Form::label('Mantenedor Actual de esta Pregunta', 'Mantenedor Actual de esta Pregunta', ['for' => 'Mantenedor Actual de esta Pregunta'] ) !!}

				<select name="maintainer_id" id="maintainer_id" class="form-control">
		        @foreach ($maintainers as $maintainer)
		        	@if($maintainer->id==$question->maintainer->id)
		        	<option value="{{$maintainer->id}}" selected="true">{{$maintainer->name}}</option>
		        	@else
		        	<option value="{{$maintainer->id}}">{{$maintainer->name}}</option>
		        	@endif
				@endforeach
				</select>
				</br></br>
		        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Guardar Cambios</button>
		    	{!! Form::close() !!}
		@endif

		<script type="text/javascript">
			$(document).ready(function() {
			    $('#addOptionModal').on('shown.bs.modal', function ()
			    {
				  	$('#name').trigger('focus');
				});
			});
		</script>
@stop