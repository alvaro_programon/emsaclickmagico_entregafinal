@extends('layouts.app')

@section('content')

		{!! Form::open([ 'route' => ['updatePositionPost',$position->id], 'method' => 'POST']) !!}

		<h2>Detalle de Cargo</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('nombre', 'Cargo', ['for' => 'nombre'] ) !!}
		    {!! Form::text('name', $position->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'cargo...' , 'required']  ) !!}
		</div>

        <br>
        <a href="{{ url('positions') }}" class="btn btn-outline-info" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}
    
@stop