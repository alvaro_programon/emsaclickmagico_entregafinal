@extends('layouts.app')

@section('content')
	    	
		{!! Form::open([ 'route' => ['updatePollPost',$poll->id], 'method' => 'POST']) !!}

		<h2>Actualizar Encuesta</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('nombre', 'Encuesta', ['for' => 'nombre'] ) !!}
		    {!! Form::text('name', $poll->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'encuesta...' , 'required']  ) !!}
		</div>

        <br>
        <a href="{{ url('detailPoll') }}/{{$poll->id}}" class="btn btn-outline-primary" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}
    
@stop