@extends('layouts.app')

@section('content')
	    
		{!! Form::open([ 'route' => 'addPollPost', 'method' => 'POST']) !!}

		<h2>Nueva Encuesta</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('nombre', 'Nombre Encuesta', ['for' => 'nombre'] ) !!}
		    {!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'nombre encuesta...' , 'required']  ) !!}
		</div>

		<div class="form-group">
        {!! Form::label('Departamento', 'Departamento', ['for' => 'Departamento'] ) !!}
            <select name="department" id="department" class="form-control">
                @foreach($departments as $department)
                <option value="{{$department->name}}">{{$department->name}}</option>
                @endforeach
            </select>
        </div>

        <br>
        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Crear Encuesta</button>
    {!! Form::close() !!}
    </div>
@stop