@extends('layouts.app')

@section('content')
	    
		{!! Form::open([ 'route' => 'addSupervisorPost', 'method' => 'POST']) !!}

		<h2>Nuevo Supervisor</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('nombre', 'Nombre', ['for' => 'nombre'] ) !!}
		    {!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'nombre usuario...' , 'required']  ) !!}
		</div>

        <div class="form-group">
		    {!! Form::label('apellido', 'Apellido', ['for' => 'apellido'] ) !!}
		    {!! Form::text('last_name', null , ['class' => 'form-control', 'id' => 'last_name', 'placeholder' => 'apellido...', 'required']  ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('email', 'Email', ['for' => 'email'] ) !!}
		    {!! Form::email('email', null , ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'email...', 'required']  ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('username', 'Nombre de Usuario', ['for' => 'username'] ) !!}
		    {!! Form::text('username', null , ['class' => 'form-control', 'id' => 'username', 'placeholder' => 'nombre de usuario...', 'required'] ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('password', 'Contraseña', ['for' => 'password'] ) !!}
		    {!! Form::text('password', null , ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'contraseña...', 'required' ,'type'=>'password']  ) !!}
		</div>

		<div class="form-group">
			{{ Form::checkbox('active', 'true') }}
			{!! Form::label('active', 'Activo', ['for' => 'active'] ) !!}
		</div>

		<div class="form-group">
        {!! Form::label('Cargo', 'Cargo', ['for' => 'Cargo'] ) !!}
            <select name="position_id" id="position_id" class="form-control">
                @foreach($positions as $position)
                <option value="{{$position->id}}">{{$position->name}}</option>
                @endforeach
            </select>
        </div>
        <br>
        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Crear Usuario</button>
    {!! Form::close() !!}
    </div>
@stop