@extends('layouts.app')

@section('content')

		{!! Form::open([ 'route' => ['updateOptionPost',$option->id,$option->question_id], 'method' => 'POST']) !!}

		<h2>Detalle de Opción</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('opcion', 'Opción', ['for' => 'opcion'] ) !!}
		    {!! Form::text('name', $option->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'opción...' , 'required']  ) !!}
		</div>

        <br>
        <a href="{{ url('detailQuestion') }}/{{ $option->question_id }}" class="btn btn-outline-info" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}
    
@stop