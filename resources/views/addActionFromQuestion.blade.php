@extends('layouts.app')

@section('content')
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	    
		{!! Form::open([ 'route' => ['addActionFromQuestionPost',$question->poll_id,$question->id], 'method' => 'POST']) !!}

		<h2>Nueva Acción de opción : <b>{{$question->name}}</b></h2>
		</br>

		<div class="form-group">
		    <b>{!! Form::label('nombre', 'Título Pregunta', ['for' => 'nombre'] ) !!}</b>
		    {!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'título pregunta...' , 'required']  ) !!}
		</div>

		<b>{!! Form::label('Tipo de Pregunta', 'Tipo de Pregunta', ['for' => 'Tipo de Pregunta'] ) !!}</b>
		</br>

		{!! Form::label('Descripción', 'Descripción', ['for' => 'Descripción'] ) !!}
		{!! Form::radio('type','description',false,['id'=>'type', 'required','onchange'=>'hideOptions(this)']) !!}
		&nbsp;&nbsp;
		{!! Form::label('Opciones', 'Opciones', ['for' => 'Opciones'] ) !!}
		{!! Form::radio('type','options',false,['id'=>'type','required','onchange'=>'showOptions(this)']) !!}
		&nbsp;&nbsp;
		{!! Form::label('Desde Mantenedor', 'Desde Mantenedor', ['for' => 'Desde Mantenedor'] ) !!}
		{!! Form::radio('type','maintainers',false,['id'=>'type','required','onchange'=>'showMaintainers(this)']) !!}
		&nbsp;&nbsp;
		{!! Form::label('Foto', 'Foto', ['for' => 'Foto'] ) !!}
		{!! Form::radio('type','photo',false,['id'=>'type', 'required','onchange'=>'hideOptions(this)']) !!}
		&nbsp;&nbsp;
		{!! Form::label('Profesionales', 'Profesionales', ['for' => 'Profesionales'] ) !!}
		{!! Form::radio('type','professionals',false,['id'=>'type', 'required','onchange'=>'showlimitDateDiv(this)']) !!}

		</br></br>

		<div id="optionsDiv" style="display: none;/*background: white;margin: 10px;*/">
			<b>{!! Form::label('Opciones', 'Opciones', ['for' => 'Opciones'] ) !!}</b>
			{{ Form::button('Agregar Opción +', array('class' => 'btn btn-primary btn-sm' , 'type' => 'button' , 'id'=>'addOption')) }}
			</br></br>

			<div id="containerOptions">

				<div id="div_borrar0">

					<div class="input-group mb-3">
					    {!! Form::text('options[]', null , ['class' => 'form-control', 'id' => 'options[]', 'placeholder' => 'opción...'] ) !!}
					    <div class="input-group-append">
					    {{ Form::button('Borrar -', array('class' => 'btn btn-danger btn-sm btn_remove' , 'type' => 'button','id'=>'0' ,'name'=> 'remove')) }}
					    </div>
					</div>

				</div>

			</div>
		</div>

		<div id="maintainersDiv" style="display: none;">
			<b>{!! Form::label('Seleccionar Mantenedor', 'Seleccionar Mantenedor', ['for' => 'Seleccionar Mantenedor'] ) !!}</b>
				<select name="maintainer_id" id="maintainer_id" class="form-control">
		        @foreach ($maintainers as $maintainer)
					<option value="{{$maintainer->id}}">{{$maintainer->name}}</option>
				@endforeach
				</select>
		</div>

		<div id="limitDateDiv" style="display: none;">
			<div class="form-group">

			<input value="true" type="checkbox" name="checkbox_limit_date" onchange="showLimitDate(this)" placeholder="SADasdsa" > Fecha Límite

			<div id="limit_date_div" style="display: none;">
				{!! Form::date('date', null , ['class' => 'form-control', 'id' => 'date']  ) !!}
			</div>
			</div>
		</div>

        </br></br>
        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Crear Pregunta</button>
    {!! Form::close() !!}
    </div>

    <script type="text/javascript">

    	jQuery(document).ready(function($)
        {
        	var current_product=1;
            $('#addOption').on('click',function(x)
            {
                var optionGroup = "<div id='div_borrar"+current_product+"'><div class='input-group mb-3'><input id='options[]' type='text' name='options[]' class='form-control obligatory-input' placeholder='opción...' required /><div class='input-group-append'><button type='button' class='btn btn-danger btn-sm btn_remove' id='"+current_product+"'>Borrar -</button></div></div></div>";
                
                $("#containerOptions").append(optionGroup);
                current_product=current_product+1;
            });

            $(document).on('click','.btn_remove', function()
            {
            	var cantidad_opciones=$('#containerOptions').children().length;
            	console.log("cantidad_opciones :"+cantidad_opciones);
            	if (cantidad_opciones>1)
            	{
					var button_id = $(this).attr("id");
            		var div_borrar = document.getElementById("div_borrar"+button_id).remove();
            	}
	      	});
        });

        function showlimitDateDiv(checkboxElem)//option press
    	{
    		if (checkboxElem.checked)
    		{
			    var limitDateDiv = document.getElementById("limitDateDiv");
			    if (limitDateDiv.style.display === "none")
			    {
			        limitDateDiv.style.display = "block";
			    }
			}
			//solo cuando presiono options es required
			document.getElementById("options[]").required = false;
			var maintainersDiv = document.getElementById("maintainersDiv");
			if (maintainersDiv.style.display === "block")
			{
			    maintainersDiv.style.display = "none";
			}
			var optionsDiv = document.getElementById("optionsDiv");
			if (optionsDiv.style.display === "block")
			{
			    optionsDiv.style.display = "none";
			}
		}

        function showLimitDate(checkboxElem)
        {
        	var limit_date_div = document.getElementById("limit_date_div");
        	if (checkboxElem.checked)
    		{
			    limit_date_div.style.display = "block";
			}
			else
			{
				limit_date_div.style.display = "none";
			}
        }

        function showMaintainers()//option press
    	{
    		var limitDateDiv = document.getElementById("limitDateDiv");
			if (limitDateDiv.style.display === "block")
			{
			    limitDateDiv.style.display = "none";
			}
    		var optionsDiv = document.getElementById("optionsDiv");
			if (optionsDiv.style.display === "block")
			{
			    optionsDiv.style.display = "none";
			}
			var maintainersDiv = document.getElementById("maintainersDiv");
			if (maintainersDiv.style.display === "none")
			{
			    maintainersDiv.style.display = "block";
			}
			document.getElementById("options[]").required = false;
		}
    	function showOptions(checkboxElem)//option press
    	{
    		var limitDateDiv = document.getElementById("limitDateDiv");
			if (limitDateDiv.style.display === "block")
			{
			    limitDateDiv.style.display = "none";
			}
    		if (checkboxElem.checked)
    		{
			    var optionsDiv = document.getElementById("optionsDiv");
			    if (optionsDiv.style.display === "none")
			    {
			        optionsDiv.style.display = "block";
			    }
			}
			//solo cuando presiono options es required
			document.getElementById("options[]").required = true;
			var maintainersDiv = document.getElementById("maintainersDiv");
			if (maintainersDiv.style.display === "block")
			{
			    maintainersDiv.style.display = "none";
			}
		}
		function hideOptions(checkboxElem)//description press
    	{
    		var limitDateDiv = document.getElementById("limitDateDiv");
			if (limitDateDiv.style.display === "block")
			{
			    limitDateDiv.style.display = "none";
			}
    		if (checkboxElem.checked)
    		{
			    var optionsDiv = document.getElementById("optionsDiv");
			    if (optionsDiv.style.display === "block")
			    {
			        optionsDiv.style.display = "none";
			    }
			}
			document.getElementById("options[]").required = false;
			var maintainersDiv = document.getElementById("maintainersDiv");
			if (maintainersDiv.style.display === "block")
			{
			    maintainersDiv.style.display = "none";
			}
		}
        
    </script>

@stop