@extends('layouts.app')

@section('content')

		{!! Form::open([ 'route' => ['updateMaintainerPost',$maintainer->id], 'method' => 'POST']) !!}

		<h2>Detalle de Mantenedor</h2>
		</br>

		<div class="form-group">
		    <b>{!! Form::label('nombre', 'Nombre', ['for' => 'nombre'] ) !!}</b>
		    {!! Form::text('name', $maintainer->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'nombre mantenedor...' , 'required']  ) !!}
		</div>

        <br>
        <a href="{{ url('detailMaintainer') }}/{{ $maintainer->id }}" class="btn btn-outline-info" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}
    
@stop