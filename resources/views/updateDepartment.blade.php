@extends('layouts.app')

@section('content')

		{!! Form::open([ 'route' => ['updateDepartmentPost',$department->id], 'method' => 'POST']) !!}

		<h2>Detalle de Departamento</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('nombre', 'Departamento', ['for' => 'nombre'] ) !!}
		    {!! Form::text('name', $department->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'departamento...' , 'required']  ) !!}
		</div>

        <br>
        <a href="{{ url('departments') }}" class="btn btn-outline-info" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}

@stop