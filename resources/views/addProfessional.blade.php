@extends('layouts.app')

@section('content')
	    
		{!! Form::open([ 'route' => 'addProfessionalPost', 'method' => 'POST']) !!}

		<h2>Nuevo Profesional</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('username', 'Nombre de Usuario', ['for' => 'username'] ) !!}
		    {!! Form::text('username', null , ['class' => 'form-control', 'id' => 'username', 'placeholder' => 'nombre de usuario para logear...', 'required'] ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('password', 'Contraseña', ['for' => 'password'] ) !!}
		    {!! Form::text('password', null , ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'contraseña...', 'required' ,'type'=>'password']  ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('nombre', 'Nombre', ['for' => 'nombre'] ) !!}
		    {!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'nombre profesional...' , 'required']  ) !!}
		</div>

        <div class="form-group">
		    {!! Form::label('apellido', 'Apellido', ['for' => 'apellido'] ) !!}
		    {!! Form::text('last_name', null , ['class' => 'form-control', 'id' => 'last_name', 'placeholder' => 'apellido profesional...', 'required']  ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('email', 'Email', ['for' => 'email'] ) !!}
		    {!! Form::email('email', null , ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'email...', 'required']  ) !!}
		</div>

        <br>
        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Crear Profesional</button>
    {!! Form::close() !!}
    </div>
@stop