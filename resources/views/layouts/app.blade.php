<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="https://emsa.clickmagico.cl/galery/icon_tittle.jpg" />
    <title>
      Emsa Codelco
    </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
          <a class="navbar-brand" href="{{ url('/') }}">
                                            ClickMágico
                                        </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    @guest
                    @else
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Supervisores
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="{{ route('/') }}">Listar Supervisores</a>
                          <a class="dropdown-item" href="{{ route('addSupervisor') }}">Agregar Supervisor</a>
                          <a class="dropdown-item" href="{{ route('positions') }}">Cargos</a>
                          <a class="dropdown-item" href="{{ route('departments') }}">Departamentos</a>
                        </div>
                      </li>

                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Tareas Programadas
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="{{ route('scheduledTasks') }}">Listar Tareas Programadas</a>
                          <a class="dropdown-item" href="{{ route('addScheduledTask') }}">Agregar Tarea Programadas</a>
                          <a class="dropdown-item" href="{{ route('points') }}">Listado de Puntos</a>
                        </div>
                      </li>

                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Tareas
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="{{ route('polls') }}">Listar Tareas</a>
                          <a class="dropdown-item" href="{{ route('addPoll') }}">Agregar Tarea</a>
                        </div>
                      </li>

                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Mis Mantenedores
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="{{ route('maintainers') }}">Listar Mantenedores</a>
                          <a class="dropdown-item" href="{{ route('addMaintainer') }}">Agregar Mantenedor</a>
                        </div>
                      </li>

                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Profesionales
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="{{ route('professionals') }}">Listar Profesionales</a>
                          <a class="dropdown-item" href="{{ route('addProfessional') }}">Agregar Profesional</a>
                        </div>
                      </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Respuestas
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="{{ route('responses') }}">Respuestas Supervisores</a>
                          <a class="dropdown-item" href="{{ route('professionalResponses') }}">Respuestas Profesionales</a>
                        </div>
                      </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('levels') }}">Estados Tareas</a>
                    </li>

                    </ul>
                    @endguest
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Ingresar</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">

            <div class="container">
                @if(Session::has('message1'))
                    <p class="alert alert-success">{{ Session::get('message1') }}</p>
                @endif
                @if(Session::has('message2'))
                    <p class="alert alert-warning">{{ Session::get('message2') }}</p>
                @endif
                @if(Session::has('message3'))
                    <p class="alert alert-danger">{{ Session::get('message3') }}</p>
                @endif

                @yield('content')
            </div>

        </main>
    <div style="align-items: center">
        <center>
            <img style="width: 80px;height: 80px;" src="https://emsa.clickmagico.cl/galery/click_magico_logo_sin_texto.png">
        </center>
    </div>
</body>
</html>
