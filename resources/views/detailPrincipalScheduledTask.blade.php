@extends('layouts.app')

@section('content')
	    
		{!! Form::open([ 'route' => ['updatePrincipalScheduledTaskPost',$principal_scheduled_task->id], 'method' => 'POST']) !!}

		<h2>
			<a href="{{ url('scheduledTasks') }}" class="btn btn-outline-info" role="button"> <- Volver</a> Detalles Tarea Programada</h2>
		</br>

		<div class="form-group">
        <b>{!! Form::label('Punto', 'Punto', ['for' => 'Punto'] ) !!}</b>
            <select name="point_id" id="point_id" class="form-control">
                @foreach($points as $point)
                @if($point->id==$principal_scheduled_task->point_id)
                <option value="{{$point->id}}" selected="true">{{$point->name}}</option>
                @else
                <option value="{{$point->id}}">{{$point->name}}</option>
                @endif
                @endforeach
            </select>
        </div>

        <div>
        	<b>{!! Form::label('Fecha Tarea Programada', 'Fecha Tarea Programada', ['for' => 'Fecha Tarea Programada'] ) !!}</b>
			{!! Form::date('task_date', $principal_scheduled_task->task_date , ['class' => 'form-control', 'id' => 'task_date']  ) !!}
		</div>

		<br><b>{!! Form::label('Tipo de Usuario', 'Tipo de Usuario', ['for' => 'Tipo de Usuario'] ) !!}</b>
		</br>

		{!! Form::label('Supervisores', 'Supervisores', ['for' => 'Supervisores'] ) !!}

		{!! Form::radio('type','supervisors',false,['id'=>'type', 'required','onchange'=>'showSupervisors(this)']) !!}

		&nbsp;&nbsp;
			
		{!! Form::label('Profesionales', 'Profesionales', ['for' => 'Profesionales'] ) !!}
			
		{!! Form::radio('type','professionals',false,['id'=>'type','required','onchange'=>'showProfessionals(this)']) !!}
		
		</br></br>

		<div id="supervisorsDiv" style="display: none;/*background: white;margin: 10px;*/">

			<b>{!! Form::label('Supervisores', 'Supervisores', ['for' => 'Supervisores'] ) !!}</b>

			<div style="overflow-y:scroll;height: 180px;">
				@foreach ($supervisors as $supervisor)
					@if($principal_scheduled_task->existsSupervisor($principal_scheduled_task->id,$supervisor->id))
					<input value="{{$supervisor->id}}" type="checkbox" name="checkbox_supervisors[]" checked> {{$supervisor->name}}
					@else
					<input value="{{$supervisor->id}}" type="checkbox" name="checkbox_supervisors[]"> {{$supervisor->name}}
					@endif
					<br>
				@endforeach
			</div>
		</div>

		<div id="professionalsDiv" style="display: none;">
			<b>{!! Form::label('Profesionales', 'Profesionales', ['for' => 'Profesionales'] ) !!}</b>

			<div style="overflow-y:scroll;height: 180px;">
				@foreach ($professionals as $professional)
					@if($principal_scheduled_task->existsProfessional($principal_scheduled_task->id,$professional->id))
					<input value="{{$professional->id}}" type="checkbox" name="checkbox_professionals[]" checked> {{$professional->name}}
					@else
					<input value="{{$professional->id}}" type="checkbox" name="checkbox_professionals[]"> {{$professional->name}}
					@endif
					<br>
				@endforeach
			</div>
		</div>

        </br></br>
        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Guardar Cambios</button>
    {!! Form::close() !!}
    </div>

    <script type="text/javascript">
    	function showSupervisors(checkboxElem)//option press
    	{
    		if (checkboxElem.checked)
    		{
			    var supervisorsDiv = document.getElementById("supervisorsDiv");
			    if (supervisorsDiv.style.display === "none")
			    {
			        supervisorsDiv.style.display = "block";
			    }
			}
			var professionalsDiv = document.getElementById("professionalsDiv");
			if (professionalsDiv.style.display === "block")
			{
			    professionalsDiv.style.display = "none";
			}
		}

        function showProfessionals(checkboxElem)
        {
        	if (checkboxElem.checked)
    		{
			    var professionalsDiv = document.getElementById("professionalsDiv");
			    if (professionalsDiv.style.display === "none")
			    {
			        professionalsDiv.style.display = "block";
			    }
			}
			var supervisorsDiv = document.getElementById("supervisorsDiv");
			if (supervisorsDiv.style.display === "block")
			{
			    supervisorsDiv.style.display = "none";
			}
        }
    </script>

@stop