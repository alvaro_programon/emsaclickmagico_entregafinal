
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<style>
#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>
<body>

  <table id="customers">
    <tr>
      <td><b>Tarea</b></td>
      <td><b>Por Vencer</b></td>
      <td><b>Vencido</b></td>
      <td><b>Completado</b></td>
      <td><b>Total</b></td>
    </tr>

    <tr>
      <td>1.1 Reunión SSO con EECC (Mensual en ...</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
    </tr>

    <tr>
      <td>2.1 Reunión Quincenal de SSO EMSA (dí...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>

    <tr>
      <td>4.1 Reunión de Revisión de Incidentes...</td>
      <td>2</td>
      <td>3</td>
      <td>0</td>
      <td>5</td>
    </tr>

    <tr>
      <td>6.1 Revisión de Controles Críticos se...</td>
      <td>2</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
    </tr>

    <tr>
      <td>Reporte de seguridad</td>
      <td>2</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
    </tr>

    <tr>
      <td>8.1 Inspecciones de Condiciones de Tr...</td>
      <td>5</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
    </tr>
  </table>

</body>
</html>

