@extends('layouts.app')

@section('content')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	{!! Form::text('myInput', null , ['class' => 'form-control', 'id' => 'myInput', 'placeholder' => 'Búsqueda...' , 'onkeyup' => 'myFunction()' ]  ) !!}
	<br>
    <?php if($tasks!=null){ ?>
		<div class="panel panel-default">
		  <div class="panel-heading">Respuestas : {{$tasks->count()}} - 
		  	<a href="{{ route('export_excel_tasks',['type'=>'xls']) }}">Excel .xls</a> - 
			<a href="{{ route('export_excel_tasks',['type'=>'csv']) }}">Excel .csv</a></div>
		  <br>
		  <table class="table" id="myTable"> 
		  	<thead> 
		  	<tr>
		  	<th>Tarea</th>
		  	<th>Profesional</th>
		  	<th>Estado</th> 
		  	<th>Foto</th> 
		  	<th>Comentario</th>
		  	<th>Fecha Realización</th>
		  	</tr>
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($tasks as $task)
		  	<tr>
		  		<td>{{$task->response->father_question}}</td>
			  	<td>{{$task->professional->name}}</td>
			  	<td>{{$task->state}}</td>
				<td>
				  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#seePhotoModal" data-photo_src="https://emsa.clickmagico.cl/galery/{{$task->photo}}">
				  		Ver Foto
				  </button>
				</td>
				<td>{{$task->comentary}}</td>
			  	<td>{{$task->created_at}}</td>
			</tr>
		  	 @endforeach
		  	</tbody> 
		  	</table>

		  	<!-- Modal -->
			<div class="modal fade" id="seePhotoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Foto</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      
			      <div class="modal-body">
			      	<img id="see_photo" src="" width="400" height="400">
			      </div>

			      <div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar</button>
			      </div>
			    </div>
			  </div>
			</div>

		</div>
		<?php } ?>

		<script>
			$(document).ready(function() {
			    $('#seePhotoModal').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget)
				  var photo_src = button.data('photo_src')
				  $('#see_photo').attr('src',photo_src); 
				});
			});
			function myFunction()
			{
			  var input, filter, table, tr, td, i;
			  input = document.getElementById("myInput");
			  filter = input.value.toUpperCase();
			  table = document.getElementById("myTable");
			  tr = table.getElementsByTagName("tr");

			  for (i = 0; i < tr.length; i++)
			  {
			    td_name = tr[i].getElementsByTagName("td")[0];
				td_last_name = tr[i].getElementsByTagName("td")[1];
				td_position = tr[i].getElementsByTagName("td")[2];
				td_active = tr[i].getElementsByTagName("td")[4];
				td_created_at = tr[i].getElementsByTagName("td")[5];

			    if (td_name && td_last_name && td_position && td_active && td_created_at)
			    {
			      if (td_name.innerHTML.toUpperCase().indexOf(filter) > -1 || td_last_name.innerHTML.toUpperCase().indexOf(filter) > -1 || td_position.innerHTML.toUpperCase().indexOf(filter) > -1 || td_active.innerHTML.toUpperCase().indexOf(filter) > -1 || td_created_at.innerHTML.toUpperCase().indexOf(filter) > -1)
			      {
			        tr[i].style.display = "";
			      } 
			      else
			      {
			        tr[i].style.display = "none";
			      }
			    }
			  }
			}
		</script>
		
@stop
