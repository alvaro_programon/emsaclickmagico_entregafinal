@extends('layouts.app')

@section('content')
	
	<br>
    <?php if($principalScheduledTasks!=null){ ?>
		<div class="panel panel-default">
		  <div class="panel-heading">Tareas Programadas : {{$principalScheduledTasks->count()}}</div>
		  <br>

		  @foreach ($principalScheduledTasks as $principalScheduledTask)
		  <div class="card text-white bg-primary mb-3" >
		  	<div class="card-header">Fecha creación {{$principalScheduledTask->created_at}} 
		  	<a href="{{ url('deletePrincipalScheduledTask') }}/{{$principalScheduledTask->id}}" onclick="
return confirm('Está seguro que desea borrar esta Tarea Programada?')" class="btn btn-danger" role="button">Eliminar</a> 
			<a href="{{ url('detailPrincipalScheduledTask') }}/{{$principalScheduledTask->id}}" class="btn btn-light" role="button">Ver Más</a>
			<a href="{{ url('principalScheduledTaskResponse') }}/{{$principalScheduledTask->id}}" class="btn btn-light" role="button">Ver Respuestas</a>
			</div>
			  <div class="card-body">
			    <h5 class="card-title">
			    	{{$principalScheduledTask->point->name}}
			    </h5>
			    <p class="card-text">
			    	Asignada a {{$principalScheduledTask->scheduledTasks->count()}} usuario(s).
			    </p>
			  </div>
			</div>
			@endforeach
		</div>
		<?php } ?>
		
@stop