@extends('layouts.app')

@section('content')

		{!! Form::open([ 'route' => ['updateProfessionalPost',$professional->id], 'method' => 'POST']) !!}

		<h2>Detalle de Profesional</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('username', 'Nombre de Usuario', ['for' => 'username'] ) !!}
		    {!! Form::text('username', $professional->username , ['class' => 'form-control', 'id' => 'username', 'placeholder' => 'nombre de profesional para login...', 'required'] ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('password', 'Contraseña', ['for' => 'password'] ) !!}
		    {!! Form::text('password', $professional->password , ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'contraseña...', 'required']  ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('nombre', 'Nombre', ['for' => 'nombre'] ) !!}
		    {!! Form::text('name', $professional->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'nombre profesional...' , 'required']  ) !!}
		</div>

        <div class="form-group">
		    {!! Form::label('apellido', 'Apellido', ['for' => 'apellido'] ) !!}
		    {!! Form::text('last_name', $professional->last_name , ['class' => 'form-control', 'id' => 'last_name', 'placeholder' => 'apellido...', 'required']  ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('email', 'Email', ['for' => 'email'] ) !!}
		    {!! Form::email('email', $professional->email , ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'email...', 'required']  ) !!}
		</div>

        <br>
        <a href="{{ url('profesionals') }}" class="btn btn-outline-info" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}
    
@stop