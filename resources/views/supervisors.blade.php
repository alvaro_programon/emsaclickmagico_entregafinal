@extends('layouts.app')

@section('content')
	
	<!--<div class="input-group mb-3">
		    {!! Form::text('search', null , ['class' => 'form-control', 'id' => 'myInsddput', 'placeholder' => 'Búsqueda...' ]  ) !!}
		    <div class="input-group-append">
		    {{ Form::button('Buscar', array('class' => 'btn btn-primary')) }}
		    </div>
	</div>-->
	
	{!! Form::text('myInput', null , ['class' => 'form-control', 'id' => 'myInput', 'placeholder' => 'Búsqueda...' , 'onkeyup' => 'myFunction()' ]  ) !!}
	<br>
    <?php if($supervisors!=null){ ?>
		<div class="panel panel-default">
		  <div class="panel-heading">Supervisores : {{$supervisors->count()}}</div>
		  <br>
		  <table class="table" id="myTable"> 
		  	<thead> 
		  	<tr> 
		  	<th>Nombre</th> 
		  	<th>Apellido</th> 
		  	<th>Cargo</th> 
		  	<th>Activo</th> 
		  	<th>Fecha creación</th> 
		  	<th>Ver en detalle</th> 
		  	<th>Borrar usuario</th> 
		  	</tr> 
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($supervisors as $supervisor)
		  	<tr> 
			  	<td>{{$supervisor->name}}</td>
			  	<td>{{$supervisor->last_name}}</td>
			  	<td>{{$supervisor->position->name}}</td>
			  	@if($supervisor->enable == 'true')
			  		<td><img class="img-responsive" style="width: 60%;height: 70%" src="{{url('icons/ic_aprove.png ')}}"></td>
			  	@else
			  		<td><img class="img-responsive" style="width: 60%;height: 70%" src="{{url('icons/ic_reprove.png ')}}"></td>
			  	@endif
			  	<td>{{$supervisor->created_at}}</td>
			  	<td>
<a href="{{ url('updateSupervisor') }}/{{$supervisor->id}}" class="btn btn-outline-primary" role="button">Ver</a></td>
	    <td>
<a href="{{ url('deleteSupervisor') }}/{{$supervisor->id}}" onclick="
return confirm('Está seguro que desea borrar este Usuario?')" class="btn btn-outline-danger" role="button">Eliminar</a></td>
		  	</tr>
		  	 @endforeach
		  	</tbody> 
		  	</table>
		</div>
		<?php } ?>

		<script>
			function myFunction()
			{
			  // Declare variables 
			  var input, filter, table, tr, td, i;
			  input = document.getElementById("myInput");
			  filter = input.value.toUpperCase();
			  table = document.getElementById("myTable");
			  tr = table.getElementsByTagName("tr");

			  // Loop through all table rows, and hide those who don't match the search query
			  for (i = 0; i < tr.length; i++)
			  {
			    td_name = tr[i].getElementsByTagName("td")[0];
				td_last_name = tr[i].getElementsByTagName("td")[1];
				td_position = tr[i].getElementsByTagName("td")[2];
				td_active = tr[i].getElementsByTagName("td")[3];
				td_created_at = tr[i].getElementsByTagName("td")[4];

			    if (td_name && td_last_name && td_position && td_active && td_created_at)
			    {
			      if (td_name.innerHTML.toUpperCase().indexOf(filter) > -1 || td_last_name.innerHTML.toUpperCase().indexOf(filter) > -1 || td_position.innerHTML.toUpperCase().indexOf(filter) > -1 || td_active.innerHTML.toUpperCase().indexOf(filter) > -1 || td_created_at.innerHTML.toUpperCase().indexOf(filter) > -1)
			      {
			        tr[i].style.display = "";
			      } 
			      else
			      {
			        tr[i].style.display = "none";
			      }
			    }
			  }
			}
		</script>
		
@stop