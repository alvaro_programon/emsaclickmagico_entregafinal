@extends('layouts.app')

@section('content')
	    
		{!! Form::open([ 'route' => ['addQuestionPredefinedPost',$poll->id], 'method' => 'POST']) !!}

		<h2>Nueva Pregunta - {{$poll->name}}</h2>
		</br>

		<div class="form-group">
		    <b>{!! Form::label('nombre', 'Título Pregunta', ['for' => 'nombre'] ) !!}</b>
		    {!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'título pregunta...' , 'required','maxlength' => '240']  ) !!}
		</div>

        </br>
    	</br>
        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Crear Pregunta</button>
    {!! Form::close() !!}
    </div>

@stop
