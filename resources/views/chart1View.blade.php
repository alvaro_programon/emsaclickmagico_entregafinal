
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <style>
    .highcharts-data-table table {
        border-collapse: collapse;
        border-spacing: 0;
        background: white;
        min-width: 100%;
        margin-top: 10px;
        font-family: sans-serif;
        font-size: 0.9em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        border: 1px solid silver;
        padding: 0.5em;
    }
    .highcharts-data-table tr:nth-child(even), .highcharts-data-table thead tr {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #eff;
    }
    .highcharts-data-table caption {
        border-bottom: none;
        font-size: 1.1em;
        font-weight: bold;
    }
    </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <div class="col-xl-6 mt-5">
        <a href="{{ route('landingcharts') }}" class="btn btn-primary btn-md">Atras</a>
    </div>
    <div id="filter" style="margin: 20px;">
        <h2>Encuestas</h2>
        </br>
        <div class="row">
            <div class="form-group col-4">
                {!! Form::label('Contrato', 'Contrato', ['for' => 'Contrato'] ) !!}
                <select name="contrato_id" id="contrato_id" class="form-control">
                    @foreach($contratos as $contrato)
                    <option value="{{$contrato->id}}">{{$contrato->name}}</option>
                    @endforeach
                </select>
            </div>
            @csrf
            <div class="form-group col-4">
                {!! Form::label('Fecha Inicio', 'Fecha Inicio', ['for' => 'Fecha Inicio'] ) !!} 
                {{ Form::date('fecha_inicio', date( "Y-m-d", strtotime(date('Y-m-d')." -1 month")), ['class'=>'form-control','id'=>'fecha_inicio']) }}
            </div>
            <div class="form-group col-4">
                {!! Form::label('Fecha Fin', 'Fecha Fin', ['for' => 'Fecha Fin'] ) !!} 
                {{ Form::date('fecha_fin', date('Y-m-d'),['class'=>'form-control','id'=>'fecha_fin']) }}
            </div>
        </div>
        <a href="#" id="filtrar" class="btn btn-outline-primary btn-lg btn-block">Filtrar</a> 
    </div>
    <div class="row mt-4" style="margin: 20px;">
        <div class="col-12" id="grafico">

        </div>
    </div>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.1.1/css/highcharts.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script>
        $(document).ready(function() {
            $('#filtrar').click(function (){
                $('.highcharts-data-table').html('')
                var _token = $("input[name='_token']").val();
                contrato = $('#contrato_id').val();
                fecha_inicio = $('#fecha_inicio').val();
                fecha_fin = $('#fecha_fin').val();
                if(contrato != '' && fecha_inicio != '' && fecha_fin != ''){
                    $.ajax({
                        method:'POST',
                        url: '{{route("resultchart1")}}',
                        data:{
                            contrato_id:contrato,fecha_inicio:fecha_inicio,fecha_fin:fecha_fin,_token:_token
                        },
                        success: function(resultado){
                            series=[];
                            asignadas=[];
                            realizadas=[];
                            $.each(resultado.elements,function(k,v){
                                series.push(v.poll)
                                asignadas.push(parseInt(v.asignadas))
                                realizadas.push(parseInt(v.realizadas))
                            })
                            Highcharts.chart('grafico', {
                                chart: {
                                    type: 'bar'
                                },
                                title: {
                                    text: 'Encuestas'
                                },
                                subtitle: {
                                    text: 'Contrato: <b>'+$("#contrato_id :selected").text()+'</b> Entre: <b>' + fecha_inicio + ' y ' + fecha_fin+'</b>'

                                },
                                credits:{
                                    enabled: true,
                                    text: 'Fuente: ClickMagico.cl',
                                    position: {
                                        align: 'center',
                                        x: 15
                                    }
                                },
                                tooltip: {
                                    shared: true
                                }, 
                                xAxis: {
                                    categories: series,
                                    allowDecimals:false
                                },
                                yAxis: {
                                    title: {
                                        text: 'Total'
                                    },
                                    allowDecimals:false
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal',
                                        animation: true
                                    }
                                },
                                series: [{
                                    name: 'Realizadas',
                                    data: realizadas
                                }, {
                                    name: 'Asginadas',
                                    data: asignadas
                                }],
                                exporting: {
                                    showTable: false,
                                    position: {
                                        align: 'center',
                                        x: 15
                                    }
                                    
                                }
                            });                         
                        }
                    });
                }
            })
        });
    </script>
