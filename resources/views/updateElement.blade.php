@extends('layouts.app')

@section('content')

		{!! Form::open([ 'route' => ['updateElementPost',$element->id,$element->maintainer_id], 'method' => 'POST']) !!}

		<h2>Detalle de Opción</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('opcion', 'Opción', ['for' => 'opcion'] ) !!}
		    {!! Form::text('name', $element->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'opción...' , 'required']  ) !!}
		</div>

        <br>
        <a href="{{ url('detailMaintainer') }}/{{ $element->maintainer_id }}" class="btn btn-outline-info" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}
    
@stop