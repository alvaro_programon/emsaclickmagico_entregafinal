@extends('layouts.app')

@section('content')

		{!! Form::open([ 'route' => ['updatePointPost',$point->id], 'method' => 'POST']) !!}

		<h2>Detalle de Punto</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('nombre', 'Punto', ['for' => 'nombre'] ) !!}
		    {!! Form::text('name', $point->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'punto de tarea programada...' , 'required']  ) !!}
		</div>

        <br>
        <a href="{{ url('points') }}" class="btn btn-outline-info" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}

@stop