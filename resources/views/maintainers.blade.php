@extends('layouts.app')

@section('content')

		<h2>Mis Mantenedores <label>{{$maintainers->count()}}</label></h2>
		</br>

		<?php if($maintainers!=null){ ?>
		<div class="panel panel-default">
		  <br>
		  @foreach ($maintainers as $maintainer)
			<div class="card border-primary mb-3">
			  <div class="card-header">Fecha creación {{$maintainer->created_at}} <a href="{{ url('deleteMaintainer') }}/{{$maintainer->id}}" onclick="
return confirm('Está seguro que desea borrar este Mantenedor?')" class="btn btn-danger" role="button">Eliminar</a> <a href="{{ url('detailMaintainer') }}/{{$maintainer->id}}" class="btn btn-primary" role="button">Ver Más</a></div>
			  <div class="card-body text-primary">
			    <h5 class="card-title">{{$maintainer->name}}</h5>
				<p class="card-text">Mantenedor con {{$maintainer->elements->count()}} opciones.
				</p>
			  </div>
			</div>
			@endforeach
		</div>
		<?php } ?>	
@stop