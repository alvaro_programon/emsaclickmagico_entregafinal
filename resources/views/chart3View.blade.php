
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <style>
    .highcharts-data-table table {
        border-collapse: collapse;
        border-spacing: 0;
        background: white;
        min-width: 100%;
        margin-top: 10px;
        font-family: sans-serif;
        font-size: 0.9em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        border: 1px solid silver;
        padding: 0.5em;
    }
    .highcharts-data-table tr:nth-child(even), .highcharts-data-table thead tr {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #eff;
    }
    .highcharts-data-table caption {
        border-bottom: none;
        font-size: 1.1em;
        font-weight: bold;
    }
    </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <div class="col-xl-6 mt-5">
        <a href="{{ route('landingcharts') }}" class="btn btn-primary btn-md">Atras</a>
    </div>
    <div id="filter" style="margin: 20px;">
        <h2>Avance Tareas Programadas Supervisores</h2>
        </br>
        <div class="row">
            @csrf
            <div class="form-group col-4">
                {!! Form::label('Fecha Inicio', 'Fecha Inicio', ['for' => 'Fecha Inicio'] ) !!} 
                {{ Form::input('date', 'fecha_inicio', null, ['class'=>'form-control','id'=>'fecha_inicio']) }}
            </div>
            <div class="form-group col-4">
                {!! Form::label('Fecha Fin', 'Fecha Fin', ['for' => 'Fecha Fin'] ) !!} 
                {{ Form::input('date', 'fecha_fin', null,['class'=>'form-control','id'=>'fecha_fin']) }}
            </div>
        </div>
        <a href="#" id="filtrar" class="btn btn-outline-primary btn-lg btn-block">Filtrar</a> 
    </div>
    <div class="row mt-4" style="margin: 20px;">
        <div class="col-12" id="grafico">

        </div>
    </div>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.1.1/css/highcharts.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script>
        $(document).ready(function() {
            $('#filtrar').click(function (){
                var _token = $("input[name='_token']").val();
                fecha_inicio = $('#fecha_inicio').val();
                fecha_fin = $('#fecha_fin').val();
                if(fecha_inicio != '' && fecha_fin != ''){
                    var resultado;
                    $.ajax({
                        method:'POST',
                        url: '{{route("resultchart3")}}',
                        data:{
                            fecha_inicio:fecha_inicio,fecha_fin:fecha_fin,_token:_token
                        },
                        success: function(resultado){
                            series=[];
                            sinP=[];
                            bajos=[];
                            medios=[];
                            altos=[];
                            completados=[];
                            $.each(resultado.elements,function(k,v){
                                series.push(v.point)
                                sinP.push(parseInt(v.sinProgreso))
                                bajos.push(parseInt(v.bajos))
                                medios.push(parseInt(v.medios))
                                altos.push(parseInt(v.altos))
                                completados.push(parseInt(v.completados))
                            })
                            
                            Highcharts.chart('grafico', {
                                chart: {
                                    type: 'bar'
                                },
                                title: {
                                    text: 'Avance Tareas Programadas Supervisores'
                                },
                                subtitle: {
                                    text: 'Entre ' + fecha_inicio + ' y ' + fecha_fin
                                },
                                credits:{
                                    enabled: true,
                                    text: 'Fuente: ClickMagico.cl',
                                    position: {
                                        align: 'center',
                                        x: 15
                                    }
                                },
                                tooltip: {
                                    shared: true
                                },                                      
                                xAxis: {
                                    categories: series,
                                    title: {
                                        text: null
                                    },
                                    allowDecimals:false
                                },
                                yAxis: {
                                    title: {
                                        text: 'Total'
                                    },
                                    allowDecimals:false
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal',
                                        animation: true
                                    }
                                },
                                series: [{
                                    name:'Sin avance',
                                    data: sinP
                                },{
                                    name:'Bajos',
                                    data: bajos
                                },{
                                    name:'Medios',
                                    data: medios
                                },{
                                    name:'Altos',
                                    data: altos
                                },{
                                    name:'Completados',
                                    data: completados
                                }],
                                exporting: {
                                    showTable: false,
                                    position: {
                                        align: 'center',
                                        x: 15
                                    }
                                    
                                }
                            });                      
                        }
                    });
                }
            })
        });
    </script>