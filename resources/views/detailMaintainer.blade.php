@extends('layouts.app')

@section('content')

		<b>
		<h2>Detalle de Mantenedor
		<a href="{{ url('maintainers') }}" class="btn btn-outline-info" role="button">Volver</a>
		<a href="{{ url('updateMaintainer') }}/{{$maintainer->id}}" class="btn btn-outline-primary" role="button">Editar Nombre Mantenedor</a>
		</h2>
		</b>
		<br>
		Mantenedor: <h4>{{$maintainer->name}}</h4>
		
			<div class="panel-heading">
				<p class="text-primary">Mantenedor con {{$maintainer->elements->count()}} opción(es).</p>
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addOptionModal">
			  	Agregar Opción +
				</button>
			</div>
			<br>
		  	<table class="table"> 
		  	<thead> 
		  	<tr> 
			  	<th>Opción</th>
			  	<th>Modificar</th>
				<th>Borrar -</th>
		  	</tr> 
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($maintainer->elements as $element)
		  	<tr> 
			  	<td>{{$element->name}}</td>
			  	<td>
<a href="{{ url('updateElement') }}/{{$element->id}}" class="btn btn-outline-primary btn-sm" role="button">Editar</a></td>
	    <td>
<a href="{{ url('deleteElement') }}/{{$element->id}}" onclick="
return confirm('Está seguro que desea borrar esta Opción?')" class="btn btn-outline-danger btn-sm" role="button">Eliminar</a></td>
		  	</tr> 
		  	 @endforeach
		  	</tbody> 
		  	</table>

		  	<!-- Modal -->
			<div class="modal fade" id="addOptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Agregar Opción</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      {!! Form::open([ 'route' => ['addElementPost',$maintainer->id], 'method' => 'POST']) !!}

			      <div class="modal-body">
			      	<b>{!! Form::label('Opción', 'Opción', ['for' => 'Opción'] ) !!}</b>
					</br>
					{!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'opción...'] ) !!}
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			        <button type="submit" class="btn btn-primary">Agregar +</button>
			      </div>

			      {!! Form::close() !!}
			    </div>
			  </div>
			</div>
@stop