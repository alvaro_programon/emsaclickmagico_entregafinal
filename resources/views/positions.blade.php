@extends('layouts.app')

@section('content')
	<div class="container">

	{!! Form::open([ 'route' => 'addPositionPost', 'method' => 'POST']) !!}

		<div class="input-group mb-3">
		    {!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Crear cargo...' , 'required']  ) !!}
		    <div class="input-group-append">
		    {{ Form::button('Crear Cargo', array('class' => 'btn btn-primary' , 'type' => 'submit')) }}
		    </div>
		</div>

    {!! Form::close() !!}
    
    <?php if($positions!=null){ ?>
		<div class="panel panel-default">
		  <br>
		  <div class="panel-heading">Cargos : {{$positions->count()}}</div>
		  <br>
		  <table class="table"> 
		  	<thead> 
		  	<tr> 
		  	<th>Nombre</th>
		  	<th>Fecha creación</th> 
		  	<th>Modificar Cargo</th> 
		  	<th>Eliminar Cargo</th> 
		  	</tr> 
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($positions as $position)
		  	<tr> 
			  	<td>{{$position->name}}</td>
			  	<td>{{$position->created_at}}</td>
			  	<td>
<a href="{{ url('updatePosition') }}/{{$position->id}}" class="btn btn-outline-primary" role="button">Editar</a></td>
	    <td>
<a href="{{ url('deletePosition') }}/{{$position->id}}" onclick="
return confirm('Está seguro que desea borrar este Cargo?')" class="btn btn-outline-danger" role="button">Eliminar</a></td>
		  	</tr> 
		  	 @endforeach
		  	</tbody> 
		  	</table>
		</div>
		<?php } ?>

	</div>
@stop