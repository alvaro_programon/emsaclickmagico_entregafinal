@extends('layouts.app')

@section('content')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	{!! Form::text('myInput', null , ['class' => 'form-control', 'id' => 'myInput', 'placeholder' => 'Búsqueda...' , 'onkeyup' => 'myFunction()' ]  ) !!}
	<br>
    <?php if($responses!=null){ ?>
		<div class="panel panel-default">
		  <div class="panel-heading">Respuestas : {{$responses->count()}}
		  <a href="{{ route('export_excel_responses',['type'=>'xls']) }}">Excel .xls</a> - 
			<a href="{{ route('export_excel_responses',['type'=>'csv']) }}">Excel .csv</a></div>
		  <br>
		  <table class="table" id="myTable"> 
		  	<thead> 
		  	<tr>
		  	<th>Supervisor</th>
		  	<th>Pregunta</th>
		  	<th>Tipo</th> 
		  	<th>Respuesta</th> 
		  	<th>Fecha Límite</th> 
		  	<th>Hora Respuesta</th>
		  	<th>Pregunta Padre</th>
		  	<th>Encuesta</th>
		  	</tr>
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($responses as $response)
		  	<tr>
		  			@if($response->supervisor==null)
                                        <td>Iniciar sesion nuevamente en app</td>
                                        @else
                                        <td>{{$response->supervisor->name}}</td>
                                        @endif

					@if($response->question==null)
					<td>Iniciar sesion nuevamente en app</td>
					@else
					<td>{{$response->question->name}}</td>
					@endif
				  		@if($response->type=='description')
				  			<td>Descripción</td>
				  		@elseif($response->type=='options')
				  			<td>Opciones</td>
				  		@elseif($response->type=='maintainers')
				  			<td>Desde mantenedor</td>
				  		@elseif($response->type=='photo')
				  			<td>Foto</td>
				  		@elseif($response->type=='professionals')
				  			<td>Desde Profesionales</td>
				  		@endif
				  	@if($response->type=='photo')
				  		<td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#seePhotoModal" data-photo_src="https://emsa.clickmagico.cl/galery/{{$response->photo}}">Ver Foto</button>
				  		</td>
				  	@else
				  		<td>{{$response->content}}</td>
				  	@endif
				  	<td>{{$response->limit_date}}</td>
			  		<td>{{$response->created_at}}</td>
			  		<td>{{$response->father_question}}</td>
			  		@if($response->question==null)
			  			<td>No tiene.</td>
			  		@elseif($response->question->poll==null)
			  			<td>No tiene.</td>
			  		@else
			  			<td>{{$response->question->poll->name}}</td>
			  		@endif
			  		
			</tr>
		  	 @endforeach
		  	</tbody> 
		  	</table>

		  	<!-- Modal -->
			<div class="modal fade" id="seePhotoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Foto</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      
			      <div class="modal-body">
			      	<img id="see_photo" src="" width="400" height="400">
			      </div>

			      <div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar</button>
			      </div>
			    </div>
			  </div>
			</div>

		</div>
		<?php } ?>

		<script>
			$(document).ready(function() {
			    $('#seePhotoModal').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal
				  var photo_src = button.data('photo_src') // Extract info from data-* attributes
				  //alert(photo_src)
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  //var modal = $(this)
				  //modal.find('.modal-body img-src').val(photo_src)
				  $('#see_photo').attr('src',photo_src); 
				});
			});
			function myFunction()
			{
			  // Declare variables 
			  var input, filter, table, tr, td, i;
			  input = document.getElementById("myInput");
			  filter = input.value.toUpperCase();
			  table = document.getElementById("myTable");
			  tr = table.getElementsByTagName("tr");

			  // Loop through all table rows, and hide those who don't match the search query
			  for (i = 0; i < tr.length; i++)
			  {
			    td_name = tr[i].getElementsByTagName("td")[0];
				td_last_name = tr[i].getElementsByTagName("td")[1];
				td_position = tr[i].getElementsByTagName("td")[2];
				td_active = tr[i].getElementsByTagName("td")[3];
				td_created_at = tr[i].getElementsByTagName("td")[4];

			    if (td_name && td_last_name && td_position && td_active && td_created_at)
			    {
			      if (td_name.innerHTML.toUpperCase().indexOf(filter) > -1 || td_last_name.innerHTML.toUpperCase().indexOf(filter) > -1 || td_position.innerHTML.toUpperCase().indexOf(filter) > -1 || td_active.innerHTML.toUpperCase().indexOf(filter) > -1 || td_created_at.innerHTML.toUpperCase().indexOf(filter) > -1)
			      {
			        tr[i].style.display = "";
			      } 
			      else
			      {
			        tr[i].style.display = "none";
			      }
			    }
			  }
			}
		</script>
		
@stop