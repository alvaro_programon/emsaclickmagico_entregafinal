@extends('layouts.app')

@section('content')
	
	<br>
    <?php if($polls!=null){ ?>
		<div class="panel panel-default">
		  <div class="panel-heading">Tareas : {{$polls->count()}}</div>
		  <br>

		  @foreach ($polls as $poll)
		  <div class="card text-white bg-primary mb-3" >
		  	<div class="card-header">Fecha creación {{$poll->created_at}} <a href="{{ url('deletePoll') }}/{{$poll->id}}" onclick="
return confirm('Está seguro que desea borrar esta Encuesta?')" class="btn btn-danger" role="button">Eliminar</a> <a href="{{ url('detailPoll') }}/{{$poll->id}}" class="btn btn-light" role="button">Ver Más</a></div>
			  <div class="card-body">
			    <h5 class="card-title">{{$poll->name}} - {{$poll->department}}</h5>
			    <p class="card-text">
			    	Tarea con {{$poll->questions->count()}} preguntas.
			    </p>
			  </div>
			</div>
			@endforeach
		</div>
		<?php } ?>
		
@stop