@extends('layouts.app')

@section('content')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	{!! Form::text('myInput', null , ['class' => 'form-control', 'id' => 'myInput', 'placeholder' => 'Búsqueda...' , 'onkeyup' => 'myFunction()' ]  ) !!}
	<br>
    <?php if($principal_scheduled_task!=null){ ?>
		<div class="panel panel-default">
		  <div class="panel-heading">Respuestas : {{$principal_scheduled_task->scheduledTasks->count()}} Punto : {{$principal_scheduled_task->point->name}}
		  </div>
		  <br>
		  <table class="table" id="myTable"> 
		  	<thead> 
		  	<tr>
		  	<th>Nombre</th>
		  	<th>Tipo de Usuario</th>
		  	<th>Comentario</th> 
		  	<th>Foto</th>
		  	<th>Estado</th>
		  	<th>Fecha Asignación</th>
		  	<th>Fecha Última Respuesta</th>
		  	<th>Punto</th>
		  	</tr>
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($principal_scheduled_task->scheduledTasks as $scheduledTask)
		  	<tr>
		  		@if($scheduledTask->type=='supervisors')
		  		<td>{{$scheduledTask->supervisor->name}}</td>
		  		<td>Supervisor</td>
		  		@else
		  		<td>{{$scheduledTask->professional->name}}</td>
		  		<td>Professional</td>
		  		@endif
		  		<td>{{$scheduledTask->comentary}}</td>
		  		<td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#seePhotoModal" data-photo_src="https://emsa.clickmagico.cl/galery/{{$scheduledTask->photo}}">Ver Foto</button>
				</td>
		  		<td>{{$scheduledTask->level}}</td>
				<td>{{$scheduledTask->created_at}}</td>
				<td>{{$scheduledTask->updated_at}}</td>
				<td></td>
			</tr>
		  	 @endforeach
		  	</tbody> 
		  	</table>

		  	<!-- Modal -->
			<div class="modal fade" id="seePhotoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Foto</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      
			      <div class="modal-body">
			      	<img id="see_photo" src="" width="400" height="400">
			      </div>

			      <div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">Aceptar</button>
			      </div>
			    </div>
			  </div>
			</div>

		</div>
		<?php } ?>

		<script>
			$(document).ready(function() {
			    $('#seePhotoModal').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget)
				  var photo_src = button.data('photo_src')
				  $('#see_photo').attr('src',photo_src); 
				});
			});
			function myFunction()
			{
			  var input, filter, table, tr, td, i;
			  input = document.getElementById("myInput");
			  filter = input.value.toUpperCase();
			  table = document.getElementById("myTable");
			  tr = table.getElementsByTagName("tr");

			  for (i = 0; i < tr.length; i++)
			  {
			    td_name = tr[i].getElementsByTagName("td")[0];
				td_last_name = tr[i].getElementsByTagName("td")[1];
				td_active = tr[i].getElementsByTagName("td")[2];
				td_comentary_at = tr[i].getElementsByTagName("td")[4];
				td_created_at = tr[i].getElementsByTagName("td")[5];
				td_updated_at = tr[i].getElementsByTagName("td")[6];

			    if (td_name && td_last_name && td_active && td_comentary_at && td_created_at && td_updated_at)
			    {
			      if (td_name.innerHTML.toUpperCase().indexOf(filter) > -1 || td_last_name.innerHTML.toUpperCase().indexOf(filter) > -1 || td_active.innerHTML.toUpperCase().indexOf(filter) > -1 || td_comentary_at.innerHTML.toUpperCase().indexOf(filter) > -1 || td_created_at.innerHTML.toUpperCase().indexOf(filter) > -1 || td_updated_at.innerHTML.toUpperCase().indexOf(filter) > -1)
			      {
			        tr[i].style.display = "";
			      } 
			      else
			      {
			        tr[i].style.display = "none";
			      }
			    }
			  }
			}
		</script>
		
@stop