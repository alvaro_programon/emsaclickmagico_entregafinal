@extends('layouts.app')

@section('content')
	
	<!--<div class="input-group mb-3">
		    {!! Form::text('search', null , ['class' => 'form-control', 'id' => 'myInsddput', 'placeholder' => 'Búsqueda...' ]  ) !!}
		    <div class="input-group-append">
		    {{ Form::button('Buscar', array('class' => 'btn btn-primary')) }}
		    </div>
	</div>-->

	{!! Form::text('myInput', null , ['class' => 'form-control', 'id' => 'myInput', 'placeholder' => 'Búsqueda...' , 'onkeyup' => 'myFunction()' ]  ) !!}
	<br>
    <?php if($professionals!=null){ ?>
		<div class="panel panel-default">
		  <div class="panel-heading">Profesionales : {{$professionals->count()}}</div>
		  <br>
		  <table class="table" id="myTable"> 
		  	<thead> 
		  	<tr> 
		  	<th>Username</th>
		  	<th>Email</th>
		  	<th>Fecha creación</th> 
		  	<th>Ver en detalle</th> 
		  	<th>Borrar profesional</th> 
		  	</tr> 
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($professionals as $professional)
		  	<tr> 
			  	<td>{{$professional->username}}</td>
			  	<td>{{$professional->email}}</td>
			  	<td>{{$professional->created_at}}</td>
			  	<td>
<a href="{{ url('updateProfessional') }}/{{$professional->id}}" class="btn btn-outline-primary" role="button">Ver</a></td>
	    <td>
<a href="{{ url('deleteProfessional') }}/{{$professional->id}}" onclick="
return confirm('Está seguro que desea borrar este Profesional?')" class="btn btn-outline-danger" role="button">Eliminar</a></td>
		  	</tr>
		  	 @endforeach
		  	</tbody> 
		  	</table>
		</div>
		<?php } ?>

		<script>
			function myFunction()
			{
			  // Declare variables 
			  var input, filter, table, tr, td, i;
			  input = document.getElementById("myInput");
			  filter = input.value.toUpperCase();
			  table = document.getElementById("myTable");
			  tr = table.getElementsByTagName("tr");

			  // Loop through all table rows, and hide those who don't match the search query
			  for (i = 0; i < tr.length; i++)
			  {
			    td_username = tr[i].getElementsByTagName("td")[0];
				td_email = tr[i].getElementsByTagName("td")[1];
				td_created_at = tr[i].getElementsByTagName("td")[2];

			    if (td_username && td_email && td_created_at)
			    {
			      if (td_username.innerHTML.toUpperCase().indexOf(filter) > -1 || td_email.innerHTML.toUpperCase().indexOf(filter) > -1 || td_created_at.innerHTML.toUpperCase().indexOf(filter) > -1)
			      {
			        tr[i].style.display = "";
			      } 
			      else
			      {
			        tr[i].style.display = "none";
			      }
			    }
			  }
			}
		</script>
		
@stop