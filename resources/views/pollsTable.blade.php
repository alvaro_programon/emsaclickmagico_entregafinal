
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <!--Div that will hold the pie chart-->
    <div id="chart_div" style="align-content: center;"></div>
    
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart','bar']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');

        var elements=<?php echo $elements; ?>;

        for (var i = 0; i < elements.length; i++)
        {
            console.log('element '+elements[i].name);
            data.addRow([elements[i].name,elements[i].uses]);
        }

        // Set chart options
        var options = {'title':'Puntos de Tareas Programadas',
                       'width':400,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
      	chart.draw(data, options);
      }
    </script>
