@extends('layouts.app')

@section('content')
	<div class="container">

	{!! Form::open([ 'route' => 'addDepartmentPost', 'method' => 'POST']) !!}

		<div class="input-group mb-3">
		    {!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Crear departamento...' , 'required']  ) !!}
		    <div class="input-group-append">
		    {{ Form::button('Crear Departamento', array('class' => 'btn btn-primary' , 'type' => 'submit')) }}
		    </div>
		</div>

    {!! Form::close() !!}
    
    <?php if($departments!=null){ ?>
		<div class="panel panel-default">
		  <br>
		  <div class="panel-heading">Departamentos : {{$departments->count()}}</div>
		  <br>
		  <table class="table"> 
		  	<thead> 
		  	<tr> 
		  	<th>Departamento</th>
		  	<th>Fecha creación</th> 
		  	<th>Modificar Departamento</th> 
		  	<th>Eliminar Departamento</th> 
		  	</tr> 
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($departments as $department)
		  	<tr> 
			  	<td>{{$department->name}}</td>
			  	<td>{{$department->created_at}}</td>
			  	<td>
<a href="{{ url('updateDepartment') }}/{{$department->id}}" class="btn btn-outline-primary" role="button">Editar</a></td>
	    <td>
<a href="{{ url('deleteDepartment') }}/{{$department->id}}" onclick="
return confirm('Está seguro que desea borrar este Departamento?')" class="btn btn-outline-danger" role="button">Eliminar</a></td>
		  	</tr> 
		  	 @endforeach
		  	</tbody> 
		  	</table>
		</div>
		<?php } ?>

	</div>
@stop