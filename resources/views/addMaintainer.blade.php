@extends('layouts.app')

@section('content')
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	    
		{!! Form::open([ 'route' => 'addMaintainerPost', 'method' => 'POST']) !!}

		<h2>Nuevo Mantenedor</h2>
		</br>

		<div class="form-group">
		    <b>{!! Form::label('nombre', 'Nombre', ['for' => 'nombre'] ) !!}</b>
		    {!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'nombre mantenedor...' , 'required']  ) !!}
		</div>

		<div id="optionsDiv">
			<b>{!! Form::label('Opciones', 'Opciones', ['for' => 'Opciones'] ) !!}</b>
			{{ Form::button('Agregar Opción +', array('class' => 'btn btn-primary btn-sm' , 'type' => 'button' , 'id'=>'addOption')) }}
			</br></br>

			<div id="containerOptions">

				<div id="div_borrar0">

					<div class="input-group mb-3">
					    {!! Form::text('options[]', null , ['class' => 'form-control', 'id' => 'options[]', 'placeholder' => 'opción...'] ) !!}
					    <div class="input-group-append">
					    {{ Form::button('Borrar -', array('class' => 'btn btn-danger btn-sm btn_remove' , 'type' => 'button','id'=>'0' ,'name'=> 'remove')) }}
					    </div>
					</div>

				</div>

			</div>

		</div>
		
        <br>
        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Crear Mantenedor</button>
    {!! Form::close() !!}
    </div>

    <script type="text/javascript">
    	jQuery(document).ready(function($)
        {
        	var current_product=1;
            $('#addOption').on('click',function(x)
            {
                var optionGroup = "<div id='div_borrar"+current_product+"'><div class='input-group mb-3'><input id='options[]' type='text' name='options[]' class='form-control obligatory-input' placeholder='opción...' required /><div class='input-group-append'><button type='button' class='btn btn-danger btn-sm btn_remove' id='"+current_product+"'>Borrar -</button></div></div></div>";
                
                $("#containerOptions").append(optionGroup);
                current_product=current_product+1;
            });

            $(document).on('click','.btn_remove', function()
            {
            	var cantidad_opciones=$('#containerOptions').children().length;
            	if (cantidad_opciones>1)
            	{
					var button_id = $(this).attr("id");
            		var div_borrar = document.getElementById("div_borrar"+button_id).remove();
            	}
	      	});
        });
    </script>

@stop