
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <style>
    .highcharts-data-table table {
        border-collapse: collapse;
        border-spacing: 0;
        background: white;
        min-width: 100%;
        margin-top: 10px;
        font-family: sans-serif;
        font-size: 0.9em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        border: 1px solid silver;
        padding: 0.5em;
    }
    .highcharts-data-table tr:nth-child(even), .highcharts-data-table thead tr {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #eff;
    }
    .highcharts-data-table caption {
        border-bottom: none;
        font-size: 1.1em;
        font-weight: bold;
    }
    </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <div class="col-xl-6 mt-5">
        <a href="{{ route('landingcharts') }}" class="btn btn-primary btn-md">Atras</a>
    </div>
    <div id="filter" style="margin: 20px;">
        <h2>Programa personalizado para supervisores</h2>
        </br>
        <div class="row">
            @csrf
            <div class="form-group col-3">
                {!! Form::label('Fecha Inicio', 'Fecha Inicio', ['for' => 'Fecha Inicio'] ) !!} 
                {{ Form::date('fecha_inicio', date( "Y-m-d", strtotime(date('Y-m-d')." -1 month")), ['class'=>'form-control','id'=>'fecha_inicio']) }}
            </div>
            <div class="form-group col-3">
                {!! Form::label('Fecha Fin', 'Fecha Fin', ['for' => 'Fecha Fin'] ) !!} 
                {{ Form::date('fecha_fin', date('Y-m-d'),['class'=>'form-control','id'=>'fecha_fin']) }}
            </div>
            <div class="form-group col-3">
                {!! Form::label('Tipo de Reporte', 'Tipo de Reporte', ['for' => 'Tipo de Reporte'] ) !!}
                <select name="tipo_reporte" id="tipo_reporte" class="form-control">
                    @foreach($tipo_reporte as $k => $v)
                        <option value="{{$k}}">{{$v}}</option>
                    @endforeach
                </select>
            </div>       
        </div>
        <a href="#" id="filtrar" class="btn btn-outline-primary btn-lg btn-block">Filtrar</a> 
    </div>
    <div class="row mt-4" style="margin: 20px;">
        <div class="col-12" id="grafico"> 
        </div>
    </div>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.1.1/css/highcharts.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script>
        $(document).ready(function() {
            $('#filtrar').click(function (){
                $('.highcharts-data-table').html('')
                var _token = $("input[name='_token']").val();
                tipo_reporte = $('#tipo_reporte').val();
                fecha_inicio = $('#fecha_inicio').val();
                fecha_fin = $('#fecha_fin').val();
                if(tipo_reporte != '' && fecha_inicio != '' && fecha_fin != ''){
                    $.ajax({
                        method:'POST',
                        url: '{{route("resultchart4")}}',
                        data:{
                            tipo_reporte:tipo_reporte,fecha_inicio:fecha_inicio,fecha_fin:fecha_fin,_token:_token
                        },
                        success: function(resultado){
                            profesionales=[];
                            asignadas=[];
                            completadas=[];
                            pendientes=[];
                            progreso=[];
                            fuera_de_plazo=[];
                            $.each(resultado.elements,function(k,v){
                                profesionales.push(parseInt(v.profesional))
                                asignadas.push(parseInt(v.asignadas))
                                completadas.push(parseInt(v.completadas))
                                pendientes.push(parseInt(v.pendientes))
                                progreso.push(parseInt(v.progreso))
                                fuera_de_plazo.push(parseInt(v.fuera_de_plazo))
                            })

                            Highcharts.chart('grafico', {
                                chart: {
                                    type: 'bar'
                                },
                                title: {
                                    text: 'Programa personalizado para supervisores'
                                },
                                subtitle: {
                                    text: 'Tipo: <b>'+tipo_reporte+'</b> Entre: <b>' + fecha_inicio + ' y ' + fecha_fin+'</b>'
                                },
                                credits:{
                                    enabled: true,
                                    text: 'Fuente: ClickMagico.cl',
                                    position: {
                                        align: 'center',
                                        x: 15
                                    }
                                },
                                tooltip: {
                                    shared: true
                                }, 
                                xAxis: {
                                    categories: profesionales,
                                    allowDecimals:false
                                },
                                yAxis: {
                                    title: {
                                        text: 'Total'
                                    },
                                    min: 0,
                                    allowDecimals:false
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal',
                                        animation: true
                                    }
                                },
                                series: [{
                                    name: 'Asignadas',
                                    data: asignadas
                                }, {
                                    name: 'Completadas',
                                    data: completadas
                                },{
                                    name: 'Pendientes',
                                    data: pendientes
                                },{
                                    name: 'En Progreso',
                                    data: progreso
                                },{
                                    name: 'Fuera de Plazo',
                                    data: fuera_de_plazo
                                }],
                                exporting: {
                                    showTable: false,
                                    position: {
                                        align: 'center',
                                        x: 15
                                    }
                                }
                            });
                        }
                    });
                }
            })
        });
    </script>
