@extends('layouts.app')

@section('content')
	<div class="container">

	{!! Form::open([ 'route' => 'addPointPost', 'method' => 'POST']) !!}

		<div class="input-group mb-3">
		    {!! Form::text('name', null , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Crear punto de tarea programada...' , 'required']  ) !!}
		    <div class="input-group-append">
		    {{ Form::button('Crear Punto', array('class' => 'btn btn-primary' , 'type' => 'submit')) }}
		    </div>
		</div>

    {!! Form::close() !!}
    
    <?php if($points!=null){ ?>
		<div class="panel panel-default">
		  <br>
		  <div class="panel-heading">Puntos : {{$points->count()}}</div>
		  <br>
		  <table class="table"> 
		  	<thead> 
		  	<tr> 
		  	<th>Punto</th>
		  	<th>Fecha creación</th> 
		  	<th>Modificar Punto</th> 
		  	<th>Eliminar Punto</th> 
		  	</tr> 
		  	</thead> 
		  	<tbody> 
		  	 @foreach ($points as $point)
		  	<tr> 
			  	<td>{{$point->name}}</td>
			  	<td>{{$point->created_at}}</td>
			  	<td>
<a href="{{ url('updatePoint') }}/{{$point->id}}" class="btn btn-outline-primary" role="button">Editar</a></td>
	    <td>
<a href="{{ url('deletePoint') }}/{{$point->id}}" onclick="
return confirm('Está seguro que desea borrar este Punto?')" class="btn btn-outline-danger" role="button">Eliminar</a></td>
		  	</tr> 
		  	 @endforeach
		  	</tbody> 
		  	</table>
		</div>
		<?php } ?>

	</div>
@stop