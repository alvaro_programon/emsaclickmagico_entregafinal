<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="https://emsa.clickmagico.cl/galery/icon_tittle.jpg" />
    <title>
      Emsa Codelco
    </title>
    <!-- Fonts -->
    {{--  <link rel="dns-prefetch" href="https://fonts.gstatic.com">  --}}
    {{--  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">  --}}

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Gráficos</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 text-center mt-5">
                <a href="{{ route('chart1') }}" class="btn btn-primary btn-md">Encuestas</a>
            </div>
            <div class="col-xl-6 text-center mt-5">
                <a href="{{ route('chart2') }}" class="btn btn-primary btn-md">Tareas Programadas Profesionales</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 text-center mt-5">
                <a href="{{ route('chart3') }}" class="btn btn-primary btn-md">Tareas Programadas Supervisores</a>
            </div>
            <div class="col-xl-6 text-center mt-5">
                <a href="" class="btn btn-primary btn-md">Programa personalizado para supervisores</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 text-center  mt-5">
                <a href="" class="btn btn-primary btn-md">Programa personalizado para profesionales</a>
            </div>
            <div class="col-xl-6 text-center  mt-5">
                <a href="" class="btn btn-primary btn-md">Programa personalizado Control desviaciones</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 text-center mt-5">
                <a href="" class="btn btn-primary btn-md">Observaciones de conducta</a>
            </div>
        </div>
    </div>
</body>
</html>