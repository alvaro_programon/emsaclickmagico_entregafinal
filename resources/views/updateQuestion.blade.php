@extends('layouts.app')

@section('content')

		{!! Form::open([ 'route' => ['updateQuestionPost',$question->id], 'method' => 'POST']) !!}

		<h2>Detalle de Pregunta</h2>
		</br>

		<div class="form-group">
		    <b>{!! Form::label('pregunta', 'Pregunta', ['for' => 'pregunta'] ) !!}</b>
		    {!! Form::text('name', $question->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'pregunta...' , 'required']  ) !!}
		</div>

        <br>
        <a href="{{ url('detailQuestion') }}/{{ $question->id }}" class="btn btn-outline-info" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}
    
@stop