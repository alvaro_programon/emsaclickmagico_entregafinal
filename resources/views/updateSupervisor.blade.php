@extends('layouts.app')

@section('content')

		{!! Form::open([ 'route' => ['updateSupervisorPost',$supervisor->id], 'method' => 'POST']) !!}

		<h2>Detalle de Usuario</h2>
		</br>

		<div class="form-group">
		    {!! Form::label('nombre', 'Nombre', ['for' => 'nombre'] ) !!}
		    {!! Form::text('name', $supervisor->name , ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'nombre usuario...' , 'required']  ) !!}
		</div>

        <div class="form-group">
		    {!! Form::label('apellido', 'Apellido', ['for' => 'apellido'] ) !!}
		    {!! Form::text('last_name', $supervisor->last_name , ['class' => 'form-control', 'id' => 'last_name', 'placeholder' => 'apellido...', 'required']  ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('email', 'Email', ['for' => 'email'] ) !!}
		    {!! Form::email('email', $supervisor->email , ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'email...', 'required']  ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('username', 'Nombre de Usuario', ['for' => 'username'] ) !!}
		    {!! Form::text('username', $supervisor->username , ['class' => 'form-control', 'id' => 'username', 'placeholder' => 'nombre de usuario...', 'required'] ) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('password', 'Contraseña', ['for' => 'password'] ) !!}
		    {!! Form::text('password', $supervisor->password , ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'contraseña...', 'required']  ) !!}
		</div>

		@if($supervisor->active == 'true')
		    <div class="form-group">
			{{ Form::checkbox('active','true',true) }}
			{!! Form::label('active', 'Activo', ['for' => 'active'] ) !!}
			</div>
		@else
			<div class="form-group">
			{{ Form::checkbox('active','true',false) }}
			{!! Form::label('active', 'Activo', ['for' => 'active'] ) !!}
			</div>
		@endif

		<div class="form-group">
        {!! Form::label('Cargo', 'Cargo', ['for' => 'Cargo'] ) !!}
            <select name="position_id" id="position_id" class="form-control">
                @foreach($positions as $position)
                	@if($supervisor->position_id == $position->id)
					    <option value="{{$position->id}}" selected="true">{{$position->name}}</option>
					@else
						<option value="{{$position->id}}">{{$position->name}}</option>
					@endif
                @endforeach
            </select>
        </div>
		
        <br>
        <a href="{{ url('/') }}" class="btn btn-outline-info" role="button">Volver</a>
        <button type="submit" class="btn btn-outline-primary">Guardar Cambios</button>
    {!! Form::close() !!}
    
@stop