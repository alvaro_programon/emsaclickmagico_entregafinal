@extends('layouts.app')

@section('content')

		<h2>Detalle de Tarea <label>{{$poll->name}}</label>    
			<a href="{{ url('updatePoll') }}/{{$poll->id}}" class="btn btn-outline-primary" role="button">Editar Tarea</a> 
			<a href="{{ url('addQuestion') }}/{{$poll->id}}" class="btn btn-primary" role="button">Agregar Pregunta</a>
			<a href="{{ url('addQuestionPredefined') }}/{{$poll->id}}" class="btn btn-primary" role="button">Agregar Pregunta Si/No -> Responsable -> Limite Fecha</a>
		</h2>
		</br>

		<?php if($poll!=null){ ?>
		<div class="panel panel-default">
		  <div class="panel-heading">Preguntas : {{$questions->count()}}</div>
		  <br>

		  @foreach ($questions as $question)
			<div class="card border-primary mb-3">
			  <div class="card-header">Fecha creación {{$question->created_at}} <a href="{{ url('deleteQuestion') }}/{{$poll->id}}/{{$question->id}}" onclick="
return confirm('Está seguro que desea borrar esta Pregunta?')" class="btn btn-danger" role="button">Eliminar</a> <a href="{{ url('detailQuestion') }}/{{$question->id}}" class="btn btn-primary" role="button">Ver Más</a></div>
			  <div class="card-body text-primary">
			    <h5 class="card-title">{{$question->name}}</h5>
			    @if($question->type=='options')
			    	<p class="card-text">Pregunta con {{$question->options->count()}} opciones. 
			    	</p>
			    @elseif($question->type=='maintainers')
			    	<p class="card-text">Pregunta tipo desde mantenedor. Mantenedor : {{$question->maintainer->name}}</p>
			    @elseif($question->type=='photo')
					<p class="card-text">Pregunta tipo foto.</p>
			    @else
			    	<p class="card-text">Pregunta tipo descripción.</p>
			    @endif
			    
			  </div>
			</div>
			@endforeach
		  
		</div>
		<?php } ?>	
@stop