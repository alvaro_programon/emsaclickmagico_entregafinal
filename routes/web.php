<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//API EMLiderazgo
Route::post('/api/emliderazgo/login','EMLiderazgoController@login');

Route::post('/api/emliderazgo/polls','EMLiderazgoController@polls');

Route::post('/api/emliderazgo/questions','EMLiderazgoController@questions');

Route::post('/api/emliderazgo/saveResponse','EMLiderazgoController@saveResponse');

Route::post('/api/emliderazgo/reestableshingFcm','EMLiderazgoController@reestableshingFcm');

Route::post('/api/emliderazgo/scheduledTasks','EMLiderazgoController@scheduledTasks');

Route::post('/api/emliderazgo/data','EMLiderazgoController@data');

Route::post('/api/emliderazgo/saveScheduledTask','EMLiderazgoController@saveScheduledTask');


//API EMProfesional
Route::post('/api/emprofesional/login','EMProfesionalController@login');

Route::post('/api/emprofesional/tasks','EMProfesionalController@tasks');

Route::post('/api/emprofesional/saveResponse','EMLiderazgoController@saveResponse');

Route::post('/api/emprofesional/reestableshingFcm','EMProfesionalController@reestableshingFcm');

Route::post('/api/emprofesional/levels','EMProfesionalController@levels');

Route::post('/api/emprofesional/updateTask','EMProfesionalController@updateTask');

Route::post('/api/emprofesional/saveScheduledTask','EMProfesionalController@saveScheduledTask');

Route::post('/api/emprofesional/scheduledTasks','EMProfesionalController@scheduledTasks');

Route::post('/api/emprofesional/data','EMProfesionalController@data');


//API EMGraficos
Route::post('/api/emgraficos/login','EMGraficosController@login');

Route::get('/api/emgraficos/maintainerChart/{maintainer_id}', 'EMGraficosController@maintainerChart');

Route::post('/api/emgraficos/maintainers','EMGraficosController@maintainers');

Route::get('/api/emgraficos/pointsChart', 'EMGraficosController@pointsChart');

Route::get('/api/emgraficos/pollsTable', 'EMGraficosController@pollsTable');

Route::get('/api/emgraficos/responses', 'EMGraficosController@responses');

Route::get('/api/emgraficos/scheduledTasksTable', 'EMGraficosController@scheduledTasksTable');

Route::get('/api/emgraficos/chart1','EMGraficosController@chart1')->name('chart1');
Route::post('/api/emgraficos/resultchart1','EMGraficosController@resultchart1')->name('resultchart1');

Route::get('/api/emgraficos/chart2','EMGraficosController@chart2')->name('chart2');
Route::post('/api/emgraficos/resultchart2','EMGraficosController@resultchart2')->name('resultchart2');

Route::get('/api/emgraficos/chart3','EMGraficosController@chart3')->name('chart3');
Route::post('/api/emgraficos/resultchart3','EMGraficosController@resultchart3')->name('resultchart3');

Route::get('/api/emgraficos/chart4','EMGraficosController@chart4')->name('chart4');
Route::post('/api/emgraficos/resultchart4','EMGraficosController@resultchart4')->name('resultchart4');

Route::get('/api/emgraficos/chart5','EMGraficosController@chart5')->name('chart5');
Route::post('/api/emgraficos/resultchart5','EMGraficosController@resultchart5')->name('resultchart5');

Route::get('/api/emgraficos/chart6','EMGraficosController@chart6')->name('chart6');
Route::post('/api/emgraficos/resultchart6','EMGraficosController@resultchart6')->name('resultchart6');

Route::get('/api/emgraficos/chart7','EMGraficosController@chart7')->name('chart7');
Route::post('/api/emgraficos/resultchart7','EMGraficosController@resultchart7')->name('resultchart7');

Route::get('/api/emgraficos/landing','EMGraficosController@landingcharts')->name('landingcharts');
//AUTH ROUTES
Auth::routes();

//WEB ROUTES
Route::get('/','SupervisorController@index')->name('/');

Route::get('addSupervisor','SupervisorController@addSupervisor')->name('addSupervisor');

Route::post('addSupervisorPost','SupervisorController@addSupervisorPost')->name('addSupervisorPost');

Route::get('positions','PositionController@positions')->name('positions');

Route::post('addPositionPost','PositionController@addPositionPost')->name('addPositionPost');

Route::get('deleteSupervisor/{id}','SupervisorController@deleteSupervisor')->name('deleteSupervisor');

Route::get('updateSupervisor/{id}','SupervisorController@updateSupervisor')->name('updateSupervisor');

Route::post('updateSupervisorPost/{id}','SupervisorController@updateSupervisorPost')->name('updateSupervisorPost');

Route::get('departments','DepartmentController@departments')->name('departments');

Route::post('addDepartmentPost','DepartmentController@addDepartmentPost')->name('addDepartmentPost');

Route::get('deletePosition/{id}','PositionController@deletePosition')->name('deletePosition');

Route::get('updatePosition/{id}','PositionController@updatePosition')->name('updatePosition');

Route::post('updatePositionPost/{id}','PositionController@updatePositionPost')->name('updatePositionPost');

Route::get('deleteDepartment/{id}','DepartmentController@deleteDepartment')->name('deleteDepartment');

Route::get('updateDepartment/{id}','DepartmentController@updateDepartment')->name('updateDepartment');

Route::post('updateDepartmentPost/{id}','DepartmentController@updateDepartmentPost')->name('updateDepartmentPost');

Route::get('polls','PollController@index')->name('polls');

Route::get('addPoll','PollController@addPoll')->name('addPoll');

Route::post('addPollPost','PollController@addPollPost')->name('addPollPost');

Route::get('deletePoll/{id}','PollController@deletePoll')->name('deletePoll');

Route::get('updatePoll/{id}','PollController@updatePoll')->name('updatePoll');

Route::post('updatePollPost/{id}','PollController@updatePollPost')->name('updatePollPost');

Route::get('detailPoll/{id}','PollController@detailPoll')->name('detailPoll');

Route::get('addQuestion/{id}','QuestionController@addQuestion')->name('addQuestion');//add question by poll

Route::post('addQuestionPost/{id}','QuestionController@addQuestionPost')->name('addQuestionPost');

Route::get('detailQuestion/{id}','QuestionController@detailQuestion')->name('detailQuestion');

Route::get('deleteQuestion/{id_poll}/{id}','QuestionController@deleteQuestion')->name('deleteQuestion');

Route::get('updateQuestion/{id}','QuestionController@updateQuestion')->name('updateQuestion');

Route::post('updateQuestionPost/{id}','QuestionController@updateQuestionPost')->name('updateQuestionPost');

Route::get('updateOption/{id}','OptionController@updateOption')->name('updateOption');

Route::post('updateOptionPost/{id}/{question_id}','OptionController@updateOptionPost')->name('updateOptionPost');

Route::get('deleteOption/{id}','OptionController@deleteOption')->name('deleteOption');

Route::post('addOptionPost/{question_id}','OptionController@addOptionPost')->name('addOptionPost');

Route::get('addAction/{id}','QuestionController@addAction')->name('addAction');

Route::post('addActionPost/{poll_id}/{option_id}','QuestionController@addActionPost')->name('addActionPost');

Route::get('addActionFromQuestion/{id}','QuestionController@addActionFromQuestion')->name('addActionFromQuestion');

Route::post('addActionFromQuestionPost/{poll_id}/{question_id}','QuestionController@addActionFromQuestionPost')->name('addActionFromQuestionPost');

Route::get('maintainers','MaintainerController@maintainers')->name('maintainers');

Route::get('addMaintainer','MaintainerController@addMaintainer')->name('addMaintainer');

Route::post('addMaintainerPost','MaintainerController@addMaintainerPost')->name('addMaintainerPost');

Route::get('detailMaintainer/{id}','MaintainerController@detailMaintainer')->name('detailMaintainer');

Route::get('updateMaintainer/{id}','MaintainerController@updateMaintainer')->name('updateMaintainer');

Route::post('updateMaintainerPost/{id}','MaintainerController@updateMaintainerPost')->name('updateMaintainerPost');

Route::post('addElementPost/{maintainer_id}','ElementController@addElementPost')->name('addElementPost');

Route::get('updateElement/{id}','ElementController@updateElement')->name('updateElement');

Route::post('updateElementPost/{id}/{maintainer_id}','ElementController@updateElementPost')->name('updateElementPost');

Route::get('deleteElement/{id}','ElementController@deleteElement')->name('deleteElement');

Route::get('deleteMaintainer/{id}','MaintainerController@deleteMaintainer')->name('deleteMaintainer');

Route::get('professionals','ProfessionalController@professionals')->name('professionals');

Route::get('addProfessional','ProfessionalController@addProfessional')->name('addProfessional');

Route::post('addProfessionalPost','ProfessionalController@addProfessionalPost')->name('addProfessionalPost');

Route::get('updateProfessional/{id}','ProfessionalController@updateProfessional')->name('updateProfessional');

Route::post('updateProfessionalPost/{id}','ProfessionalController@updateProfessionalPost')->name('updateProfessionalPost');

Route::get('deleteProfessional/{id}','ProfessionalController@deleteProfessional')->name('deleteProfessional');

Route::post('updateMaintainerQuestionPost/{maintainer_id}','QuestionController@updateMaintainerQuestionPost')->name('updateMaintainerQuestionPost');

Route::get('responses','ResponseController@responses')->name('responses');

Route::get('professionalResponses','ResponseController@professionalResponses')->name('professionalResponses');

Route::get('levels','LevelController@levels')->name('levels');

Route::post('addLevelPost','LevelController@addLevelPost')->name('addLevelPost');

Route::get('export_excel_tasks/{type}', 'ExcelController@export_excel_tasks')->name('export_excel_tasks');

Route::get('export_excel_responses/{type}', 'ExcelController@export_excel_responses')->name('export_excel_responses');

Route::get('scheduledTasks','ScheduledTaskController@scheduledTasks')->name('scheduledTasks');

Route::get('addScheduledTask','ScheduledTaskController@addScheduledTask')->name('addScheduledTask');

Route::post('addScheduledTaskPost','ScheduledTaskController@addScheduledTaskPost')->name('addScheduledTaskPost');

Route::get('points','PointController@points')->name('points');

Route::post('addPointPost','PointController@addPointPost')->name('addPointPost');

Route::get('updatePoint/{id}','PointController@updatePoint')->name('updatePoint');

Route::post('updatePointPost/{point_id}','PointController@updatePointPost')->name('updatePointPost');

Route::get('deletePoint/{id}','PointController@deletePoint')->name('deletePoint');

Route::get('deletePrincipalScheduledTask/{id}','ScheduledTaskController@deletePrincipalScheduledTask')->name('deletePrincipalScheduledTask');

Route::get('detailPrincipalScheduledTask/{id}','ScheduledTaskController@detailPrincipalScheduledTask')->name('detailPrincipalScheduledTask');

Route::post('updatePrincipalScheduledTaskPost/{id}','ScheduledTaskController@updatePrincipalScheduledTaskPost')->name('updatePrincipalScheduledTaskPost');

Route::get('principalScheduledTaskResponse/{id}','ScheduledTaskController@principalScheduledTaskResponse')->name('principalScheduledTaskResponse');

Route::get('addQuestionPredefined/{id}','QuestionController@addQuestionPredefined')->name('addQuestionPredefined');//add question by poll

Route::post('addQuestionPredefinedPost/{id}','QuestionController@addQuestionPredefinedPost')->name('addQuestionPredefinedPost');

// páginas de test de Max Rojas.
Route::get('/Test/test1','TestController@index');
Route::get('/report', "MyReportController@index");

//EXCEL
Route::get('excel_test', 'ExcelController@excel_test')->name('excel_test');




