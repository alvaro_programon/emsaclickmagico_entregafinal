<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $table = 'responses';

    public function container()//para obtener el container
    {
        return $this->belongsTo('App\Container');
    }

    public function supervisor()//para obtener la poll
    {
    	return $this->belongsTo('App\Supervisor');
    }

    public function question()//para obtener la poll
    {
    	return $this->belongsTo('App\Question');
    }

    public function option()//para obtener la poll
    {
    	return $this->belongsTo('App\Option');
    }
}
