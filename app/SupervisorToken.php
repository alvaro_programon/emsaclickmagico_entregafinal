<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupervisorToken extends Model
{
    protected $table = 'supervisor_tokens';

    public function supervisor()//para obtener la poll
    {
    	return $this->belongsTo('App\Supervisor');
    }
}
