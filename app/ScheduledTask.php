<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduledTask extends Model
{
    protected $table = 'scheduled_tasks';

    public function professional()
    {
    	return $this->belongsTo('App\Professional');
    }

    public function supervisor()
    {
    	return $this->belongsTo('App\Supervisor');
    }

    public function principalScheduledTask()
    {
        return $this->belongsTo('App\PrincipalScheduledTask');
    }
}
