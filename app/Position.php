<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Supervisor;

class Position extends Model
{
    protected $table = 'positions';

    public function supervisor()
    {
        return $this->hasMany('Supervisor');
    }
}
