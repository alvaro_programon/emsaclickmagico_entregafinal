<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/api/emliderazgo/login',
        '/api/emliderazgo/polls',
        '/api/emliderazgo/questions',
        '/api/emliderazgo/saveResponse',
        '/api/emliderazgo/reestableshingFcm',
        '/api/emliderazgo/scheduledTasks',
        '/api/emliderazgo/data',
        '/api/emliderazgo/saveScheduledTask',

        '/api/emprofesional/login',
        '/api/emprofesional/tasks',
        '/api/emprofesional/reestableshingFcm',
        '/api/emprofesional/levels',
        '/api/emprofesional/updateTask',
        '/api/emprofesional/saveScheduledTask',
        '/api/emprofesional/scheduledTasks',
        '/api/emprofesional/data',

        '/api/emgraficos/login',
        '/api/emgraficos/maintainerChart',
        '/api/emgraficos/maintainers',
        '/api/emgraficos/pointsChart',
        '/api/emgraficos/pollsTable',
        '/api/emgraficos/responses',
        '/api/emgraficos/scheduledTasksTable',
        '/api/emgraficos/chart1',
        '/api/emgraficos/chart2',
        '/api/emgraficos/chart3'

    ];
}
