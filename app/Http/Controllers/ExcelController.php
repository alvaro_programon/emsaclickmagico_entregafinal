<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Response;
use Excel;
use Carbon\Carbon;
use DB;

class ExcelController extends Controller
{
    public function export_excel_tasks($type)
    {
        $tasks = Task::get()->toArray();

        return Excel::create('tareas_profesionales_'.Carbon::now()->year.'_'.Carbon::now()->month.'_'.Carbon::now()->day.'_'.Carbon::now()->hour.'_'.Carbon::now()->minute, function($excel) use ($tasks)
        {
            $excel->sheet('sheet name', function($sheet) use ($tasks)
            {
                $sheet->fromArray($tasks);
            });
        })->download($type);
    }

    public function excel_test()
    {
        return $r = DB::select( 
            DB::raw("
                SELECT supervisors.name as 'Supervisor',
                questions.name as 'Pregunta',
                CASE responses.type WHEN 'professionals' THEN 'Profesionales'
                                    WHEN 'options' THEN 'Opciones'
                                    WHEN 'maintainers' THEN 'Desde Mantenedor'
                                    WHEN 'photo' THEN 'Foto'
                                    WHEN 'description' THEN 'Descripción'
                END AS 'Tipo',
                options.name as 'Opción',
                responses.content as 'Contenido',
                responses.limit_date as 'Fecha Límite',
                maintainers.name as 'Mantenedor',
                elements.name as 'Opción Mantenedor',
                responses.created_at as 'Fecha Respuesta'
                FROM responses 
                JOIN questions ON questions.id=responses.question_id
                JOIN supervisors ON supervisors.id=responses.supervisor_id
                LEFT JOIN options ON options.id=responses.option_id
                LEFT JOIN maintainers ON maintainers.id=responses.maintainer_id
                LEFT JOIN elements ON elements.id=responses.element_id
                ") 
        );
        
        $responses= json_decode( json_encode($r), true);
        
        return Excel::create('respuestas_supervisores_'.Carbon::now()->year.'_'.Carbon::now()->month.'_'.Carbon::now()->day.'_'.Carbon::now()->hour.'_'.Carbon::now()->minute, function($excel) use ($responses)
        {
            $excel->sheet('sheet name', function($sheet) use ($responses)
            {
                $sheet->fromArray($responses);
            });
        })->download('xls');
    }

    public function export_excel_responses($type)
    {
        $r = DB::select( 
            DB::raw("
                SELECT supervisors.name as 'Supervisor',
                questions.name as 'Pregunta',
                CASE responses.type WHEN 'professionals' THEN 'Profesionales'
                                    WHEN 'options' THEN 'Opciones'
                                    WHEN 'maintainers' THEN 'Desde Mantenedor'
                                    WHEN 'photo' THEN 'Foto'
                                    WHEN 'description' THEN 'Descripción'
                END AS 'Tipo',
                options.name as 'Opción',
                responses.content as 'Contenido',
                CASE responses.limit_date WHEN 'null' THEN ''
                                          ELSE responses.limit_date
                END AS 'Fecha Límite',
                CONCAT('http://localhost/ClickMagico/public/galery/',responses.photo) as 'Foto',
                maintainers.name as 'Mantenedor',
                elements.name as 'Opción Mantenedor',
                responses.created_at as 'Fecha Respuesta'
                FROM responses 
                JOIN questions ON questions.id=responses.question_id
                JOIN supervisors ON supervisors.id=responses.supervisor_id
                LEFT JOIN options ON options.id=responses.option_id
                LEFT JOIN maintainers ON maintainers.id=responses.maintainer_id
                LEFT JOIN elements ON elements.id=responses.element_id
                ") 
        );
        
        $responses= json_decode( json_encode($r), true);
        
        return Excel::create('respuestas_supervisores_'.Carbon::now()->year.'_'.Carbon::now()->month.'_'.Carbon::now()->day.'_'.Carbon::now()->hour.'_'.Carbon::now()->minute, function($excel) use ($responses)
        {
            $excel->sheet('sheet name', function($sheet) use ($responses)
            {
                $sheet->fromArray($responses);
            });
        })->download($type);
    }   

    public function importExcel(Request $request)
    {
        /*if($request->hasFile('import_file')){
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {
                    $data['title'] = $row['title'];
                    $data['description'] = $row['description'];

                    if(!empty($data)) {
                        DB::table('post')->insert($data);
                    }
                }
            });
        }*/
    }  
}
