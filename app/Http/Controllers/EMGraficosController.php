<?php

namespace App\Http\Controllers;

use DB;
use App\Admin;
use App\Element;
use App\Response;
use App\Maintainer;
use App\Supervisor;
use Illuminate\Http\Request;

class EMGraficosController extends Controller
{
    public function login(Request $request)
    {
        $username=$request->input('username');
        $password=$request->input('password');

        $result=Admin::where('username',$username)
                            ->where('password',$password)
                            ->first();
        if (!is_null($result))
        {
            return response()->json(['state'=>"OK",
                                    'message'=>"Login Correcto!",
                                    'token'=>$result->token
                                    ]);
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Credenciales incorrectas..."]);
        }
    }

    public function maintainerChart($maintainer_id)
    {
        $elements = DB::select( 
            DB::raw("
                SELECT elements.name as 'name',
                count(responses.element_id) as 'uses'        
                from elements
                join responses
                on (elements.id = responses.element_id)
                WHERE responses.maintainer_id = '$maintainer_id'
                group by elements.name
                ")
        );
        $maintainer=Maintainer::where('id',$maintainer_id)
                            ->first();
        //return response()->json(['state'=>"OK",'maintainers'=>$maintainers]);
        return view('maintainerChart',['elements'=>json_encode($elements)
                                    ,'maintainer_name'=>$maintainer->name]);
    }

    public function maintainers(Request $request)
    {
        $maintainers=Maintainer::all();
        return response()->json(['state'=>"OK",'maintainers'=>$maintainers]);
    }

    public function pointsChart()
    {
        $elements = DB::select( 
            DB::raw("
                SELECT points.name as 'name',
                count(scheduled_tasks.principal_scheduled_task_id) as 'uses'        
                from principal_scheduled_tasks
                join scheduled_tasks
                on (principal_scheduled_tasks.id = scheduled_tasks.principal_scheduled_task_id)
                join points
                on (principal_scheduled_tasks.point_id = points.id)
                group by points.name
                ")
        );
        //$maintainer=Maintainer::where('id',$maintainer_id)->first();

        //return response()->json(['state'=>"OK",'elements'=>$elements]);
        return view('pointsChart',['elements'=>json_encode($elements)]);
    }

    public function chart1()
    {
        $contrato=Maintainer::where('name','Contrato')->first();
        $contratos=Element::where('maintainer_id',$contrato->id)->get();
        return view('chart1View',compact('contratos'));
    }
    public function chart2()
    {
        return view('chart2View');
    }
    public function chart3()
    {
        return view('chart3View');
    }
    public function chart4()
    {
        $tipo_reporte=[
            'EFC1' => 'EFC1',
            'EFC2' => 'EFC2',
            'EFC3' => 'EFC3',
            'EFC4' => 'EFC4',
            'EFC5' => 'EFC5',
            'EFC6' => 'EFC6',
            'IPER' => 'IPER',
            'Reportes de seguridad' => 'Reportes de seguridad'
        ];
        return view('chart4View',compact('tipo_reporte'));
    }
    public function resultchart1(Request $request)
    {
	$elements = DB::select( 
        DB::raw("
            SELECT polls.name as 'poll',
            sum(CASE responses.type WHEN 'professionals' THEN 1 ELSE 0 END) as 'asignadas',
            count(DISTINCT containers.id) as 'realizadas'
            from containers
            join responses
            on (responses.container_id = containers.id)
            join questions
            on (questions.id = responses.question_id)
            join polls
            on (polls.id = questions.poll_id)
            WHERE responses.element_id = ".$request->contrato_id."
            AND responses.created_at > DATE('".$request->fecha_inicio."')
            AND responses.created_at < DATE('".$request->fecha_fin."')
            group by polls.name
            ")
        );
        return response()->json(['state'=>"OK",'elements'=>$elements]);
    }
    public function resultchart2(Request $request)
    {
        $elements = DB::select( 
            DB::raw("
                SELECT principal_scheduled_tasks.task_date as 'date',
                points.name as 'point',
                count(scheduled_tasks.principal_scheduled_task_id) as 'total',
                sum(CASE scheduled_tasks.level WHEN 'Bajo' THEN 1 ELSE 0 END) as 'bajos',
                sum(CASE scheduled_tasks.level WHEN 'Medio' THEN 1 ELSE 0 END) as 'medios',
                sum(CASE scheduled_tasks.level WHEN 'Alto' THEN 1 ELSE 0 END) as 'altos',
                sum(CASE scheduled_tasks.level WHEN 'Completado' THEN 1 ELSE 0 END) as 'completados',
                sum(CASE WHEN scheduled_tasks.updated_at > principal_scheduled_tasks.task_date THEN 1 ELSE 0 END) as 'fuera_plazo'
                from principal_scheduled_tasks
                join points
                on (principal_scheduled_tasks.point_id = points.id)
                join scheduled_tasks
                on (scheduled_tasks.principal_scheduled_task_id = principal_scheduled_tasks.id)
                WHERE scheduled_tasks.type = 'professionals'
                AND principal_scheduled_tasks.created_at > DATE('".$request->fecha_inicio."')
                AND principal_scheduled_tasks.created_at < DATE('".$request->fecha_fin."')
                group by principal_scheduled_tasks.task_date,points.name
            ")
        );
        return response()->json(['state'=>"OK",'elements'=>$elements]);
    }
    public function resultchart3(Request $request)
    {
        $elements = DB::select( 
            DB::raw("
                SELECT
                points.name as 'point',
                -- count(scheduled_tasks.principal_scheduled_task_id) as 'total',
                sum( if (scheduled_tasks.level is null, 1 , 0 ) )as 'sinProgreso',
                sum(CASE scheduled_tasks.level WHEN 'Bajo' THEN 1 ELSE 0 END) as 'bajos',
                sum(CASE scheduled_tasks.level WHEN 'Medio' THEN 1 ELSE 0 END) as 'medios',
                sum(CASE scheduled_tasks.level WHEN 'Alto' THEN 1 ELSE 0 END) as 'altos',
                sum(CASE scheduled_tasks.level WHEN 'Completado' THEN 1 ELSE 0 END) as 'completados'                
                from principal_scheduled_tasks
                join points
                on (principal_scheduled_tasks.point_id = points.id)
                join scheduled_tasks
                on (scheduled_tasks.principal_scheduled_task_id = principal_scheduled_tasks.id)
                WHERE scheduled_tasks.type = 'supervisors'
                AND principal_scheduled_tasks.created_at >= DATE('".$request->fecha_inicio."')
                AND principal_scheduled_tasks.created_at <= DATE('".$request->fecha_fin."')
                group by points.name
                order by 1 asc")
        );
        return response()->json(['state'=>"OK",'elements'=>$elements]);
    }

    public function resultchart4(Request $request)
    {
        $elements = DB::select(DB::raw("CALL reporte1(0,'".$request->fecha_inicio."','".$request->fecha_fin."','')"));
        
        return response()->json(['state'=>"OK",'elements'=>$elements]);
    }
    /*public function pollsTable()
    {
        $elements = DB::select( 
            DB::raw("
                SELECT polls.name as 'name',
                count(responses.question_id) as 'uses'     
                from polls
                join questions
                on (polls.id = questions.poll_id)
                join responses
                on (responses.question_id = questions.id)
                group by polls.name
                ")
        );
        //$maintainer=Maintainer::where('id',$maintainer_id)->first();

        //return response()->json(['state'=>"OK",'elements'=>$elements]);
        return view('pollsTable',['elements'=>json_encode($elements)]);
    }*/

    public function responses()
    {
        $responses=Response::orderBy('created_at','desc')->get();
        return view('responsesTable',['responses'=>$responses]);
    }

    public function scheduledTasksTable()
    {
        $responses=Response::orderBy('created_at','desc')->get();
        return view('scheduledTasksTable',['responses'=>$responses]);
    }

    public function landingcharts()
    {
        return view('landingchart');
    }
}
