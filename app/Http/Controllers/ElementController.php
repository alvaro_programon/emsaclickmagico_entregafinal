<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Element;
use Illuminate\Support\Facades\Session;

class ElementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addElementPost(Request $request,$maintainer_id)
    {
    	$element=new Element;
        $element->name=$request->input('name');
        $element->maintainer_id=$maintainer_id;
        $element->save();

        Session::flash('message1','La Opción se agregó correctamente!');
        return redirect()->route('detailMaintainer',['id'=>$maintainer_id]);
    }

    public function updateElement($id)
    {
        $element=Element::FindOrFail($id);
        if (is_null($element))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('maintainers');
        } 
        else
        {
            return view('updateElement',['element'=>$element]);
        }
    }

    public function updateElementPost(Request $request,$id,$maintainer_id)
    {
        $element=Element::where('id',$id)
            	->update(['name'=>$request->input('name')]);
        Session::flash('message1','La Opción se actualizó correctamente!');
        return redirect()->route('detailMaintainer',['id'=>$maintainer_id]);
    }

    public function deleteElement($id)
    {
        $element=Element::FindOrFail($id);
        $result=$element->delete();
        if ($result)
        {
            Session::flash('message1','La Opción se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar la pregunta.');
        }
        return redirect()->route('detailMaintainer',['id'=>$element->maintainer_id]);
    }
}
