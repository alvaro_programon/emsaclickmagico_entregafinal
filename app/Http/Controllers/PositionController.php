<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Position;
use Illuminate\Support\Facades\Session;

class PositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function positions()
    {
    	$positions=Position::all();
        return view('positions',['positions'=>$positions]);
    }

    public function addPositionPost(Request $request)
    {
    	$position=new Position;
        
        $position->name=$request->input('name');

        $position->save();

        return redirect()->route('positions');
    }
    
    public function updatePosition($id)
    {
        $position=Position::FindOrFail($id);
        if (is_null($position))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('/');
        }
        else
        {
            return view('updatePosition',['position'=>$position]);
        }
    }

    public function updatePositionPost(Request $request,$id)
    {
        $position=Position::where('id',$id)
            ->update(['name'=>$request->input('name')]);
        Session::flash('message1','El Cargo se actualizó correctamente!');
        return redirect()->route('positions');
    }

    public function deletePosition($id)
    {
        $position=Position::FindOrFail($id);
        $result=$position->delete();
        if ($result)
        {
            Session::flash('message1','El Cargo se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar el Cargo.');
        }
        return redirect()->route('positions');
    }
}
