<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Poll;
use App\Option;
use App\Maintainer;
use Illuminate\Support\Facades\Session;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addQuestionPredefined($id)
    {
        $poll=Poll::FindOrFail($id);
        if (is_null($poll))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('polls');
        }
        else
        {
            return view('addQuestionPredefined',['poll'=>$poll]);
        }
    }

    public function addQuestionPredefinedPost(Request $request,$id)
    {
        $question=new Question;
        
        $question->name=$request->input('name');
        $question->type='options';
        $question->poll_id=$id;
        $question->save();

        $si_option=new Option;
        $si_option->name='Sí';
        $si_option->question_id=$question->id;
        $si_option->save();

        $no_option=new Option;
        $no_option->name='No';
        $no_option->question_id=$question->id;
        $no_option->save();

        $responsable=new Question;
        $responsable->name='Responsable';
        $responsable->type='professionals';
        $responsable->limit_date='true';
        $responsable->poll_id=$id;
        $responsable->activated_by_option_id=$no_option->id;
        $responsable->save();

        $activable=Option::where('id',$no_option->id)
            ->update(['turn_on_question_id'=>$responsable->id]);

        Session::flash('message1','La Pregunta se creó correctamente!');
        return redirect()->route('addQuestionPredefined',['id'=>$id]);
    }
    
    public function addQuestion($id)
    {
        $poll=Poll::FindOrFail($id);
        $maintainers=Maintainer::all();
        if (is_null($poll))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('polls');
        }
        else
        {
            return view('addQuestion',['poll'=>$poll,'maintainers'=>$maintainers    ]);
        }
    }

    public function addQuestionPost(Request $request,$id)
    {
    	$question=new Question;
        
        $question->name=$request->input('name');
        $question->type=$request->input('type');
        if ($request->input('type')=='professionals')
        {
            if ($request->input('checkbox_limit_date')=='true')
            {
                $question->limit_date='true';
            }
        }
        $question->poll_id=$id;
        if ($request->input('type')=='maintainers')
        {
            $question->maintainer_id=$request->input('maintainer_id');
        }

        $question->save();

        if ($request->input('type')=='options')
        {
            $options=$request->input('options');
            if (sizeof($options)>0)
            {
                foreach($options as $option)
                {
                    $new_option=new Option;
                    $new_option->name=$option;
                    $new_option->question_id=$question->id;
                    $new_option->save();
                }
            }
        }

        Session::flash('message1','La Pregunta se creó correctamente!');

        return redirect()->route('detailPoll',['id'=>$id]);
    }

    public function detailQuestion($id)
    {
        $question=Question::FindOrFail($id);
        if (is_null($question))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('polls');
        }
        else
        {
            $maintainers=Maintainer::all();
            $options=Option::where('question_id',$id)
                                ->get();
            return view('detailQuestion',['question'=>$question,'options'=>$options,'maintainers'=>$maintainers]);
        }
    }

    public function deleteQuestion($id_poll,$id)
    {
        $question=Question::FindOrFail($id);
        $result=$question->delete();
        if ($result)
        {
            Session::flash('message1','La Pregunta se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar la pregunta.');
        }
        return redirect()->route('detailPoll',['id'=>$id_poll]);
    }

    public function updateQuestion($id)
    {
        $question=Question::FindOrFail($id);
        if (is_null($question))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('polls');
        }
        else
        {
            return view('updateQuestion',['question'=>$question]);
        }
    }

    public function updateQuestionPost(Request $request,$id)
    {
        $question=Question::where('id',$id)
            ->update(['name'=>$request->input('name')]);
        Session::flash('message1','La Pregunta se actualizó correctamente!');
        return redirect()->route('detailQuestion',['id'=>$id]);
    }

    public function updateMaintainerQuestionPost(Request $request,$id)
    {
        $question=Question::where('id',$id)
            ->update(['maintainer_id'=>$request->input('maintainer_id')]);
        Session::flash('message1','La Pregunta se actualizó correctamente!');
        return redirect()->route('detailQuestion',['id'=>$id]);
    }
    
    public function addActionFromQuestion($id)//question id
    {
        $maintainers=Maintainer::all();
        $question=Question::FindOrFail($id);
        return view('addActionFromQuestion',['question'=>$question,'maintainers'=>$maintainers]);
    }

    public function addActionFromQuestionPost(Request $request,$poll_id,$question_id)
    {//from question
        $question=new Question;
        
        $question->name=$request->input('name');
        $question->type=$request->input('type');
        if ($request->input('type')=='professionals')
        {
            if ($request->input('checkbox_limit_date')=='true')
            {
                $question->limit_date='true';
            }
        }
        $question->poll_id=$poll_id;

        if ($request->input('type')=='maintainers')
        {
            $question->maintainer_id=$request->input('maintainer_id');
        }
        //$question->turn_on_question_id=$question_id;
        $question->save();

        $question_father=Question::where('id',$question_id)
            ->update(['turn_on_question_id'=>$question->id]);

        if ($request->input('type')=='options')
        {
            $options=$request->input('options');
            if (sizeof($options)>0)
            {
                foreach($options as $option)
                {
                    $new_option=new Option;
                    $new_option->name=$option;
                    $new_option->question_id=$question->id;
                    $new_option->save();
                }
            }
        }

        Session::flash('message1','La Acción se creó correctamente!');

        return redirect()->route('detailQuestion',['id'=>$question->id]);
    }

    public function addAction($id)//option id
    {
        $maintainers=Maintainer::all();
        $option=Option::FindOrFail($id);
        return view('addAction',['option'=>$option,'maintainers'=>$maintainers]);
    }

    public function addActionPost(Request $request,$poll_id,$option_id)
    {//from option
        $question=new Question;
        
        $question->name=$request->input('name');
        $question->type=$request->input('type');
        if ($request->input('type')=='professionals')
        {
            if ($request->input('checkbox_limit_date')=='true')
            {
                $question->limit_date='true';
            }
        }
        $question->poll_id=$poll_id;
        $question->activated_by_option_id=$option_id;
        if ($request->input('type')=='maintainers')
        {
            $question->maintainer_id=$request->input('maintainer_id');
        }
        $question->save();

        $option=Option::where('id',$option_id)
            ->update(['turn_on_question_id'=>$question->id]);

        if ($request->input('type')=='options')
        {
            $options=$request->input('options');
            if (sizeof($options)>0)
            {
                foreach($options as $option)
                {
                    $new_option=new Option;
                    $new_option->name=$option;
                    $new_option->question_id=$question->id;
                    $new_option->save();
                }
            }
        }

        Session::flash('message1','La Acción se creó correctamente!');

        return redirect()->route('detailQuestion',['id'=>$question->id]);
    }
}
