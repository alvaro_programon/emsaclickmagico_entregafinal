<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poll;
use App\Question;
use App\Department;
use App\TokenFcmSupervisor;
use Illuminate\Support\Facades\Session;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class PollController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$polls=Poll::all();
        return view('polls',['polls'=>$polls]);
    }

    public function addPoll()
    {
        $departments=Department::all();
        return view('addPoll',['departments'=>$departments]);
    }

    public function addPollPost(Request $request)
    {
    	$poll=new Poll;
        
        $poll->name=$request->input('name');
        $poll->department=$request->input('department');

        $poll->save();

        Session::flash('message1','La Encuesta se agregó correctamente!');

        $this->sendNotificationAllSupervisor();

        return redirect()->route('polls');
    }

    public function sendNotificationAllSupervisor()
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('EMSA');
        $notificationBuilder->setBody('Se ha creado una nueva tarea...')
                                    ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $tokens=TokenFcmSupervisor::select('token')->get();
        $deviceTokens = array();
        foreach ($tokens as $token)
        {
            array_push($deviceTokens,$token->token);
        }

        $downstreamResponse = FCM::sendTo($deviceTokens, $option, $notification, $data);
    }

    public function deletePoll($id)
    {
        $poll=Poll::FindOrFail($id);
        $result=$poll->delete();
        
        if ($result)
        {
            Session::flash('message1','La Encuesta se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar la encuesta.');
        }
        return redirect()->route('polls');
    }

    public function detailPoll($id)
    {
        $poll=Poll::FindOrFail($id);
        if (is_null($poll))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('polls');
        }
        else
        {
            $questions=Question::where('poll_id',$id)
                                ->whereNull('activated_by_option_id')//activated_by_option_id si es null es porque es en primera profundidad
                                ->orderBy('created_at','ASC')
                                ->get();
            return view('detailPoll',['poll'=>$poll,'questions'=>$questions]);
        }
    }

    public function updatePoll($id)
    {
        $poll=Poll::FindOrFail($id);
        if (is_null($poll))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('polls');
        }
        else
        {
            return view('updatePoll',['poll'=>$poll]);
        }
    }

    public function updatePollPost(Request $request,$id)
    {
        $poll=Poll::where('id',$id)
            ->update(['name'=>$request->input('name')]);
        Session::flash('message1','La encuesta se actualizó correctamente!');
        return redirect()->route('polls');
    }
}
