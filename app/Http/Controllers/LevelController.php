<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Level;

class LevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function levels()
    {
    	$levels=Level::all();
        return view('levels',['levels'=>$levels]);
    }
}
