<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Professional;
use App\ProfessionalToken;
use App\Response;
use App\Question;
use App\TokenFcmProfessional;
use App\Task;
use App\Level;
use App\Point;
use App\ScheduledTask;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class EMProfesionalController extends Controller
{
    public function login(Request $request)
    {
    	$username=$request->input('username');
        $password=$request->input('password');
    	$token_fcm=$request->input('token_fcm');
        $imei=$request->input('imei');

    	$result=Professional::where('username',$username)
    						->where('password',$password)
                			->first();
        if (!is_null($result))
        {
            $token_fcm_professional=TokenFcmProfessional::
                            where('professional_id',$result->id)
                            ->where('imei',$imei)
                            ->first();
            if (!is_null($token_fcm_professional))
            {
                $token_fcm_professional=TokenFcmProfessional::
                                    where('professional_id',$result->id)
                                    ->where('imei',$imei)
                                    ->update(['token'=>$token_fcm]);
            }
            else
            {
                $token_fcm_professional=new TokenFcmProfessional;
                $token_fcm_professional->professional_id=$result->id;
                $token_fcm_professional->token=$token_fcm;
                $token_fcm_professional->imei=$imei;
                $token_fcm_professional->save();
            }

            return response()->json(['state'=>"OK",
                                    'message'=>"Login Correcto!",
                                    'token'=>$result->token->token,
                                    'professional_id'=>$result->id.'',
                                    'name'=>$result->name.'']);
        }
    	else
    	{
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Credenciales incorrectas..."]);
    	}
    }

    public function reestableshingFcm(Request $request)
    {
        $professional_id=$request->input('professional_id');
        $imei=$request->input('imei');
        $result=TokenFcmProfessional::
                    where('professional_id',$professional_id)
                    ->where('imei',$imei)
                    ->delete();

        if (!is_null($result))
        {
            return response()->json(['state'=>"OK"]);
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"No hay registros asociados."]);
        }
    }

    public function tasks(Request $request)
    {
        $token=$request->input('token');
    	$professional_id=$request->input('professional_id');
        $result=ProfessionalToken::where('token',$token)->first();

        if (!is_null($result))
        {
        	$tasks=Task::where('professional_id',$professional_id)
			->where('state','!=','Completado')
                        ->with('response')
			->orderBy('created_at','desc')
                        ->get();
            return response()->json(['state'=>"OK",
                                    'tasks'=>$tasks,
                                    ]);
        }
    	else
    	{
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Login vencido..."]);
    	}
    }

    public function levels(Request $request)
    {
        $token=$request->input('token');
        $result=ProfessionalToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $levels=Level::all();
        return response()->json(['state'=>"OK",
                                    'levels'=>$levels,
                                    ]);
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Login vencido..."]);
        }
    }

    public function scheduledTasks(Request $request)
    {
        $token=$request->input('token');
        $result=ProfessionalToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $professional_id=$request->input('professional_id');
            $scheduledTasks=ScheduledTask::
                            where('professional_id',$professional_id)
			    ->where('level','!=','Completado')
                            ->with('PrincipalScheduledTask')
                            ->get();
            return response()->json(['state'=>"OK",'scheduledTasks'=>$scheduledTasks]);
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Sesión caducada..."]);
        }
    }

    public function data(Request $request)
    {
        $token=$request->input('token');
        $result=ProfessionalToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $levels=Level::all();
            $points=Point::all();

            return response()->json(['state'=>"OK",'points'=>$points,'levels'=>$levels]);
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Sesión caducada..."]);
        }
    }

    public function saveScheduledTask(Request $request)
    {
        $token=$request->input('token');
        $result=ProfessionalToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $rules = array(
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8096',
            );
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) 
            {
                return response()->json(['state'=>"ERROR",
                                    'message'=>"Foto con formato inválido o Tamaño muy grande."]);
            }
            else
            {
                $photoName = Carbon::now()->year.Carbon::now()->month.Carbon::now()->day.Carbon::now()->hour.Carbon::now()->minute.Carbon::now()->second;
                $photo_name='null';
                if ($request->photo!="")
                {
                    $photo_name= $photoName.'photo.'.$request->photo->getClientOriginalExtension();
                    $request->photo->move(public_path('galery'),$photo_name);
                }

                $id=(int)$request->input('id');
                $scheduledTask=ScheduledTask::
                                where('id',$id)
                                ->update(['level'=>$request->input('level'),
                                        'comentary'=>$request->input('comentary'),
                                        'photo'=>$photo_name]);
                return response()->json(['state'=>"OK",
                                    'message'=>'Respuesta enviada']);
            }
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Sesión caducada..."]);
        }
    }

    public function updateTask(Request $request)
    {
        $token=$request->input('token');
        $result=ProfessionalToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $id=$request->input('id');
            $state=$request->input('state');
            $comentary=$request->input('comentary');
            
            $rules = array(
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8096',
            );
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) 
            {
                    return response()->json(['state'=>"ERROR",
                                    'message'=>"Foto con formato inválido o Tamaño muy grande."]);
            }
            else
            {
                $photoName = Carbon::now()->year.Carbon::now()->month.Carbon::now()->day.Carbon::now()->hour.Carbon::now()->minute.Carbon::now()->second;
                if ($request->photo!="")
                {
                    $photo_name= $photoName.$id.'photo.'.$request->photo->getClientOriginalExtension();
                    $request->photo->move(public_path('galery'),$photo_name);
                }
                $task=Task::where('id',$id)
                            ->update(['state'=>$state
                                    ,'comentary'=>$comentary
                                    ,'photo'=>$photo_name]);
                return response()->json(['state'=>"OK",
                                    'message'=>'Tarea actualizada!',
                                    ]);
            }
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Login vencido..."]);
        }
    }
}
