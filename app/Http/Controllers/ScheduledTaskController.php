<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScheduledTask;
use App\Point;
use App\Supervisor;
use App\Professional;
use App\PrincipalScheduledTask;
use Illuminate\Support\Facades\Session;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\TokenFcmSupervisor;
use App\TokenFcmProfessional;

class ScheduledTaskController extends Controller 
//y tambien es PrincipalScheduledTasksController
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function scheduledTasks()
    {
    	$principalScheduledTasks=PrincipalScheduledTask::orderBy('created_at','desc')->get();
        return view('scheduledTasks',['principalScheduledTasks'=>$principalScheduledTasks]);
    }

    public function addScheduledTask()
    {
    	$points=Point::all();
    	$professionals=Professional::all();
    	$supervisors=Supervisor::all();
    	return view('addScheduledTask',['points'=>$points,'professionals'=>$professionals,'supervisors'=>$supervisors]);
    }

    public function addScheduledTaskPost(Request $request)
    {
    	$principal_scheduled_task=new PrincipalScheduledTask;
    	$principal_scheduled_task->point_id=$request->input('point_id');
    	$principal_scheduled_task->task_date=$request->input('task_date');
    	$principal_scheduled_task->save();

    	$checkbox_supervisors=$request->input('checkbox_supervisors');
        if (!is_null($checkbox_supervisors))
        {
            if (sizeof($checkbox_supervisors)>0)
            {
                foreach($checkbox_supervisors as $checkbox_supervisor)
                {
                    $scheduled_task=new ScheduledTask;
                    $scheduled_task->supervisor_id=(int)$checkbox_supervisor;
                    $scheduled_task->principal_scheduled_task_id=$principal_scheduled_task->id;
                    $scheduled_task->type='supervisors';
		    $scheduled_task->level='';
                    $scheduled_task->save();

                    $this->sendNotification('supervisors',(int)$checkbox_supervisor);
                }
            }
        }
        
        $checkbox_professionals=$request->input('checkbox_professionals');
        if (!is_null($checkbox_professionals))
        {
            if (sizeof($checkbox_professionals)>0)
            {
                foreach($checkbox_professionals as $checkbox_professional)
                {
                    $scheduled_task=new ScheduledTask;
                    $scheduled_task->professional_id=(int)$checkbox_professional;
                    $scheduled_task->principal_scheduled_task_id=$principal_scheduled_task->id;
                    $scheduled_task->type='professionals';
		    $scheduled_task->level='';
                    $scheduled_task->save();

                    $this->sendNotification('professionals',(int)$checkbox_professional);
                }
            }
        }

        return redirect()->route('scheduledTasks');
    }

    public function updatePrincipalScheduledTaskPost(Request $request,$id)
    {
    	$principal_scheduled_task=PrincipalScheduledTask::where('id',$id)
            ->update(['point_id'=>$request->input('point_id'),
        			  'task_date'=>$request->input('task_date')]);

        $supervisors_not_selected = array();
        $professionals_not_selected = array();
        
    	$checkbox_supervisors=$request->input('checkbox_supervisors');
	
        if (!is_null($checkbox_supervisors) && sizeof($checkbox_supervisors)>0)
        {
            foreach($checkbox_supervisors as $checkbox_supervisor)
            {	
            	$scheduled_task=ScheduledTask::
            					where('supervisor_id',$checkbox_supervisor)
    							->where('principal_scheduled_task_id',$id)
    							->first();
    			if (is_null($scheduled_task))
        		{
        			$scheduled_task=new ScheduledTask;
	                $scheduled_task->supervisor_id=(int)$checkbox_supervisor;
	                $scheduled_task->principal_scheduled_task_id=$id;
	                $scheduled_task->type='supervisors';
			$scheduled_task->level='';
	                $scheduled_task->save();

	                $this->sendNotification('supervisors',(int)$checkbox_supervisor);
        		}
        		array_push($supervisors_not_selected,$checkbox_supervisor);
            }
            $deletedScheduleSupervisors=ScheduledTask::
        					where('principal_scheduled_task_id',$id)
        					->where('type','supervisors')
        					->whereNotIn('supervisor_id',$supervisors_not_selected)
        					->delete();
        }
        else
        {
        	$deletedScheduleSupervisors=ScheduledTask::
        					where('principal_scheduled_task_id',$id)
        					->where('type','supervisors')
        					->delete();
        }
        $checkbox_professionals=$request->input('checkbox_professionals');
        if (sizeof($checkbox_professionals)>0)
        {
            foreach($checkbox_professionals as $checkbox_professional)
            {
				$scheduled_task=ScheduledTask::
            					where('professional_id',$checkbox_professional)
    							->where('principal_scheduled_task_id',$id)
    							->first();
    			if (is_null($scheduled_task))
        		{
        			$scheduled_task=new ScheduledTask;
	                $scheduled_task->professional_id=(int)$checkbox_professional;
	                $scheduled_task->principal_scheduled_task_id=$id;
	                $scheduled_task->type='professionals';
			$scheduled_task->level='';
	                $scheduled_task->save();

	                $this->sendNotification('professionals',(int)$checkbox_professional);
        		}
        		array_push($professionals_not_selected,$checkbox_professional);
            }
            $deletedScheduleProfessionals=ScheduledTask::
        					where('principal_scheduled_task_id',$id)
        					->where('type','professionals')
        					->whereNotIn('professional_id',$professionals_not_selected)
        					->delete();
        }
        else
        {
        	$deletedScheduleProfessionals=ScheduledTask::
        					where('principal_scheduled_task_id',$id)
        					->where('type','professionals')
        					->delete();
        }

        return redirect()->route('scheduledTasks');
    }
    
    public function detailPrincipalScheduledTask($id)
    {
        $principal_scheduled_task=PrincipalScheduledTask::FindOrFail($id);
        if (is_null($principal_scheduled_task))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('scheduledTasks');
        }
        else
        {
        	$points=Point::all();
	    	$professionals=Professional::all();
	    	$supervisors=Supervisor::all();
    		return view('detailPrincipalScheduledTask',['principal_scheduled_task'=>$principal_scheduled_task,'points'=>$points,'professionals'=>$professionals,'supervisors'=>$supervisors]);
        }
    }

    public function principalScheduledTaskResponse($id)
    {
        $principal_scheduled_task=PrincipalScheduledTask::FindOrFail($id);
        
        if (is_null($principal_scheduled_task))
        {
            return redirect()->route('scheduledTasks');
        }
        else
        {
            return view('principalScheduledTaskResponse',['principal_scheduled_task'=>$principal_scheduled_task]);
        }
    }

    public function deletePrincipalScheduledTask($id)
    {
        $principal_scheduled_task=PrincipalScheduledTask::FindOrFail($id);
        $result=$principal_scheduled_task->delete();
        if ($result)
        {
            Session::flash('message1','La Tarea Programada se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar la Tarea Programada.');
        }
        return redirect()->route('scheduledTasks');
    }

    public function sendNotification($type,$user_id)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('EMSA Liderazgo');
        $notificationBuilder->setBody('Tiene una nueva tarea programada...')
                                    ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        if ($type=='supervisors')
        {
        	$tokens=TokenFcmSupervisor::select('token')
                                ->where('supervisor_id',$user_id)
                                ->get();
        }
        else//professionals
        {
			$tokens=TokenFcmProfessional::select('token')
                                ->where('professional_id',$user_id)
                                ->get();
        }

        $deviceTokens = array();
        foreach ($tokens as $token)
        {
            array_push($deviceTokens,$token->token);
        }
        if (sizeof($deviceTokens)>0) {
            $downstreamResponse = FCM::sendTo($deviceTokens, $option, $notification, $data);
        }
    }
}
