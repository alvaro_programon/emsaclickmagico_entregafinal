<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Response;
use App\Task;

class ResponseController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function responses()
    {
    	$responses=Response::orderBy('created_at','desc')->get();
        return view('responses',['responses'=>$responses]);
    }
    
    public function professionalResponses()
    {
        $tasks=Task::orderBy('created_at','desc')->get();
        return view('tasks',['tasks'=>$tasks]);
    }

    public function getExcel()
    {
    	
    }
}
