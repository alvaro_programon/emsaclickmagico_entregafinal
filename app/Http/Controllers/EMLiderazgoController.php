<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supervisor;
use App\SupervisorToken;
use App\Poll;
use App\Question;
use App\Maintainer;
use App\Professional;
use App\Point;
use App\Response;
use App\TokenFcmSupervisor;
use App\TokenFcmProfessional;
use App\ScheduledTask;
use App\Task;
use App\Level;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Container;

class EMLiderazgoController extends Controller//EMLiderazgo -> Supervisores
{
    public function login(Request $request)
    {
    	$username=$request->input('username');
    	$password=$request->input('password');
        $token_fcm=$request->input('token_fcm');
        $imei=$request->input('imei');

    	$result=Supervisor::where('username',$username)
    						->where('password',$password)
                			->first();
        if (!is_null($result))
        {
            $token_fcm_supervisor=TokenFcmSupervisor::
                            where('supervisor_id',$result->id)
                            ->where('imei',$imei)
                            ->first();
            if (!is_null($token_fcm_supervisor))
            {
                $token_fcm_supervisor=TokenFcmSupervisor::
                                    where('supervisor_id',$result->id)
                                    ->where('imei',$imei)
                                    ->update(['token'=>$token_fcm]);
            }
            else
            {
                $token_fcm_supervisor=new TokenFcmSupervisor;
                $token_fcm_supervisor->supervisor_id=$result->id;
                $token_fcm_supervisor->token=$token_fcm;
                $token_fcm_supervisor->imei=$imei;
                $token_fcm_supervisor->save();
            }

            return response()->json(['state'=>"OK",
                                    'message'=>"Login Correcto!",
                                    'token'=>$result->token->token,
                                    'supervisor_id'=>$result->id.'',
                                    'name'=>$result->name.'']);
        }
    	else
    	{
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Credenciales incorrectas..."]);
    	}
    }

    public function polls(Request $request)
    {
    	$token=$request->input('token');
    	$result=SupervisorToken::where('token',$token)->first();

        if (!is_null($result))
        {
        	$polls=Poll::with('questions')->get();
            return response()->json(['state'=>"OK",'polls'=>$polls]);
        }
    	else
    	{
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Sesión caducada..."]);
    	}
    }

    public function questions(Request $request)
    {
    	$token=$request->input('token');
    	$result=SupervisorToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $poll_id=$request->input('poll_id');
        	$questions=Question::where('poll_id',$poll_id)->with('options')->get();
            return response()->json(['state'=>"OK",'questions'=>$questions]);
        }
    	else
    	{
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Sesión caducada..."]);
    	}
    }

    public function data(Request $request)
    {
        $token=$request->input('token');
        $result=SupervisorToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $levels=Level::all();
            $maintainers=Maintainer::with('elements')->get();
            $professionals=Professional::select('id','username','name','last_name','email')->get();
            $points=Point::all();

            return response()->json(['state'=>"OK",'maintainers'=>$maintainers,'professionals'=>$professionals,'points'=>$points,'levels'=>$levels]);
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Sesión caducada..."]);
        }
    }

    public function saveResponse(Request $request)
    {
        $token=$request->input('token');
        $result=SupervisorToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $container_id=0;
            if ($request->input('container_id')=='')//crear container
            {
                $container=new Container;
                $container->supervisor_id=(int)$request->input('supervisor_id');
                $container->save();
                $container_id=$container->id;
            }
            else//usar container existente
            {
                $container_id=(int)$request->input('container_id');
            }

            $response=new Response;

            if ($request->input('type')=='maintainers')
            {
                if (!is_null($request->input('maintainer_id')) || $request->input('maintainer_id')!='')
                {
                    $response->maintainer_id=(int)$request->input('maintainer_id');
                }
                if (!is_null($request->input('option_id')) || $request->input('option_id')!='')
                {
                    $response->element_id=(int)$request->input('option_id');
                }
            }
            if ($request->input('type')=='options')
            {
                if (!is_null($request->input('option_id')) || $request->input('option_id')!='')
                {
                    $response->option_id=(int)$request->input('option_id');
                }
            }
            if ($request->input('type')=='photo')
            {
                $rules = array(
                    'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8096',
                );
                $validator = Validator::make($request->all(), $rules);

                if ($validator->fails()) 
                {
                    return response()->json(['state'=>"ERROR",
                                    'message'=>"Foto con formato inválido o Tamaño muy grande."]);
                }
                else
                {
                    $photoName = Carbon::now()->year.Carbon::now()->month.Carbon::now()->day.Carbon::now()->hour.Carbon::now()->minute.Carbon::now()->second;
                    if ($request->photo!="")
                    {
                        $photo_name= $photoName.'photo.'.$request->photo->getClientOriginalExtension();
                        $request->photo->move(public_path('galery'),$photo_name);

                        $response->photo=$photo_name;
                    }
                }
            }
            if ($request->input('type')=='professionals')
            {
                $response->limit_date=$request->input('limit_date');//true
                $response->professional_id=(int)$request->input('option_id');

                $this->sendNotification((int)$request->input('option_id'));
            }

            if (!is_null($request->input('question_id')) || $request->input('question_id')!='') {
                $response->question_id=(int)$request->input('question_id');
            }
            
            $response->type=$request->input('type');
            $response->content=$request->input('content');
            $response->supervisor_id=(int)$request->input('supervisor_id');
            $response->father_question=$request->input('father_question');
            $response->container_id=$container_id;

            $response->save();

	    if ($request->input('type')=='professionals')//create task
            {
                $task=new Task;
                $task->response_id=$response->id;
                $task->professional_id=(int)$request->input('option_id');
                $task->state='Sin estado aún.';

                $question=Question::where('id',$response->question_id)->first();
                if ($question!=null)
                {
                    $task->poll=$question->poll->name;
                }
                else
                {
                    $task->poll='';
                }

                $task->save();
            }

            return response()->json(['state'=>"OK",
                                    'message'=>'Respuesta enviada',
                                    'container_id'=>$container_id]);
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Sesión caducada..."]);
        }
    }
    
    public function saveScheduledTask(Request $request)
    {
        $token=$request->input('token');
        $result=SupervisorToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $rules = array(
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8096',
            );
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) 
            {
                return response()->json(['state'=>"ERROR",
                                    'message'=>"Foto con formato inválido o Tamaño muy grande."]);
            }
            else
            {
                $photoName = Carbon::now()->year.Carbon::now()->month.Carbon::now()->day.Carbon::now()->hour.Carbon::now()->minute.Carbon::now()->second;
                $photo_name='null';
                if ($request->photo!="")
                {
                    $photo_name= $photoName.'photo.'.$request->photo->getClientOriginalExtension();
                    $request->photo->move(public_path('galery'),$photo_name);
                }

                $id=(int)$request->input('id');
                $scheduledTask=ScheduledTask::
                                where('id',$id)
                                ->update(['level'=>$request->input('level'),
                                        'comentary'=>$request->input('comentary'),
                                        'photo'=>$photo_name]);
                return response()->json(['state'=>"OK",
                                    'message'=>'Respuesta enviada']);
            }
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Sesión caducada..."]);
        }
    }

    public function scheduledTasks(Request $request)
    {
        $token=$request->input('token');
        $result=SupervisorToken::where('token',$token)->first();

        if (!is_null($result))
        {
            $supervisor_id=$request->input('supervisor_id');
            $scheduledTasks=ScheduledTask::
                            where('supervisor_id',$supervisor_id)
                            ->where('level','!=','Completado')
                            ->with('PrincipalScheduledTask')
                            ->get();
            return response()->json(['state'=>"OK",'scheduledTasks'=>$scheduledTasks]);
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"Sesión caducada..."]);
        }
    }

    public function sendNotification($professional_id)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('EMSA');
        $notificationBuilder->setBody('Tiene una nueva asignación...')
                                    ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $tokens=TokenFcmProfessional::select('token')
                                ->where('professional_id',$professional_id)
                                ->get();
        $deviceTokens = array();
        foreach ($tokens as $token)
        {
            array_push($deviceTokens,$token->token);
        }
        if (sizeof($deviceTokens)>0) {
            $downstreamResponse = FCM::sendTo($deviceTokens, $option, $notification, $data);
        }

        /*$downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();
        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();
        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        // return Array (key:token, value:errror) - in production you should remove from your database the tokens*/
    }

    public function reestableshingFcm(Request $request)
    {
        $supervisor_id=$request->input('supervisor_id');
        $imei=$request->input('imei');
        $result=TokenFcmSupervisor::
                    where('supervisor_id',$supervisor_id)
                    ->where('imei',$imei)
                    ->delete();

        if (!is_null($result))
        {
            return response()->json(['state'=>"OK"]);
        }
        else
        {
            return response()->json(['state'=>"ERROR",
                                    'message'=>"No hay registros asociados."]);
        }
    }
}
