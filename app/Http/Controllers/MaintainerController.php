<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maintainer;
use App\Element;
use Illuminate\Support\Facades\Session;

class MaintainerController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function maintainers()
    {
    	$maintainers=Maintainer::all();
        return view('maintainers',['maintainers'=>$maintainers]);
    }

    public function addMaintainer()
    {
        return view('addMaintainer');
    }

    public function addMaintainerPost(Request $request)
    {
    	$maintainer=new Maintainer;
        $maintainer->name=$request->input('name');
        $maintainer->save();
        $elements=$request->input('options');
        if (!is_null($elements))
        {
        	if (sizeof($elements)>0)
	        {
	            foreach($elements as $element)
	            {
	                $new_element=new Element;
	                $new_element->name=$element;
	                $new_element->maintainer_id=$maintainer->id;
	                $new_element->save();
	            }
	        }
        }
        Session::flash('message1','El Mantenedor se creó correctamente!');
        return redirect()->route('maintainers');
    }

    public function detailMaintainer($id)
    {
        $maintainer=Maintainer::FindOrFail($id);
        if (is_null($maintainer))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('maintainers');
        }
        else
        {
            return view('detailMaintainer',['maintainer'=>$maintainer]);
        }
    }

    public function updateMaintainer($id)
    {
        $maintainer=Maintainer::FindOrFail($id);
        if (is_null($maintainer))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('maintainers');
        }
        else
        {
            return view('updateMaintainer',['maintainer'=>$maintainer]);
        }
    }

    public function updateMaintainerPost(Request $request,$id)
    {
        $maintainer=Maintainer::where('id',$id)
            		->update(['name'=>$request->input('name')]);
        Session::flash('message1','El Mantenedor se actualizó correctamente!');
        return redirect()->route('detailMaintainer',['id'=>$id]);
    }

    public function deleteMaintainer($id)
    {
        $maintainer=Maintainer::FindOrFail($id);
        $result=$maintainer->delete();
        if ($result)
        {
            Session::flash('message1','El Mantenedor se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar el mantenedor.');
        }
        return redirect()->route('maintainers');
    }
}
