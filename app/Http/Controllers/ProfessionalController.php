<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Professional;
use App\ProfessionalToken;
use Illuminate\Support\Facades\Session;

class ProfessionalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function professionals()
    {
    	$professionals=Professional::all();
        return view('professionals',['professionals'=>$professionals]);
    }

    public function addProfessional()
    {
    	return view('addProfessional');
    }

    public function addProfessionalPost(Request $request)
    {
    	$token=new ProfessionalToken;
        $token->token=str_random(90);
        $token->save();

    	$professional=new Professional;
    	$professional->username=$request->input('username');
        $professional->password=$request->input('password');
        $professional->name=$request->input('name');
        $professional->last_name=$request->input('last_name');
        $professional->email=$request->input('email');
        $professional->token_id=$token->id;
        $professional->save();

        Session::flash('message1','El Profesional se agregó correctamente!');

        return redirect()->route('professionals');
    }

    public function updateProfessional($id)
    {
        $professional=Professional::FindOrFail($id);
        if (is_null($professional))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('professionals');
        }
        else
        {
            return view('updateProfessional',['professional'=>$professional]);
        }
    }

    public function updateProfessionalPost(Request $request,$id)
    {
        $professional=Professional::where('id',$id)
            ->update([
            		'username'=>$request->input('username'),
                    'password'=>$request->input('password'),
            		'name'=>$request->input('name'),
                    'last_name'=>$request->input('last_name'),
                    'email'=>$request->input('email')
                    ]);
        Session::flash('message1','El Profesional se actualizó correctamente!');
        return redirect()->route('professionals');
    }

    public function deleteProfessional($id)
    {
    	$professional=Professional::FindOrFail($id);
        $result=$professional->delete();
        if ($result)
        {
            Session::flash('message1','El Profesional se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar el profesional.');
        }
        return redirect()->route('professionals');
    }
}
