<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Option;
use Illuminate\Support\Facades\Session;

class OptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function updateOption($id)
    {
        $option=Option::FindOrFail($id);
        if (is_null($option))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('polls');
        }
        else
        {
            return view('updateOption',['option'=>$option]);
        }
    }

    public function updateOptionPost(Request $request,$id,$question_id)
    {
        $option=Option::where('id',$id)
            ->update(['name'=>$request->input('name')]);
        Session::flash('message1','La Opción se actualizó correctamente!');
        return redirect()->route('detailQuestion',['id'=>$question_id]);
    }

    public function deleteOption($id)
    {
        $option=Option::FindOrFail($id);
        $result=$option->delete();
        if ($result)
        {
            Session::flash('message1','La Opción se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar la pregunta.');
        }
        return redirect()->route('detailQuestion',['id'=>$option->question_id]);
    }

    public function addOptionPost(Request $request,$question_id)
    {
    	$option=new Option;
        $option->name=$request->input('name');
        $option->question_id=$question_id;
        $option->save();

        Session::flash('message1','La Opción se agregó correctamente!');
        return redirect()->route('detailQuestion',['id'=>$question_id]);
    }
}
