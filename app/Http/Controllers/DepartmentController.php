<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use Illuminate\Support\Facades\Session;

class DepartmentController extends Controller
{
    public function departments()
    {
    	$departments=Department::all();
        return view('departments',['departments'=>$departments]);
    }

    public function addDepartmentPost(Request $request)
    {
    	$department=new Department;
        
        $department->name=$request->input('name');

        $department->save();

        return redirect()->route('departments');
    }

    public function updateDepartment($id)
    {
        $department=Department::FindOrFail($id);
        if (is_null($department))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('departments');
        }
        else
        {
            return view('updateDepartment',['department'=>$department]);
        }
    }

    public function updateDepartmentPost(Request $request,$id)
    {
        $department=Department::where('id',$id)
            ->update(['name'=>$request->input('name')]);
        Session::flash('message1','El Departamento se actualizó correctamente!');
        return redirect()->route('departments');
    }

    public function deleteDepartment($id)
    {
        $department=Department::FindOrFail($id);
        $result=$department->delete();
        if ($result)
        {
            Session::flash('message1','El Departamento se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar el Departamento.');
        }
        return redirect()->route('departments');
    }
}
