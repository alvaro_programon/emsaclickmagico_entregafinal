$responses_options = Response::
            join('questions', 'responses.question_id', '=', 'questions.id')
            ->join('supervisors', 'responses.supervisor_id', '=', 'supervisors.id')
            ->join('options', 'responses.option_id', '=', 'options.id')
            //->join('maintainers', 'responses.maintainer_id', '=', 'maintainers.id')
            //->join('elements', 'responses.element_id', '=', 'elements.id')
            ->select('supervisors.name as Supervisor','questions.name as Tarea','options.name as Respuesta'/*,'maintainers.name as Mantenedor','elements.name as Elemento de Mantenedor'*/,'responses.type as Tipo Pregunta','responses.content as Contenido','responses.limit_date as Fecha Límite','responses.created_at as Fecha Respuesta')
            ->get()
            ->toArray();

        $responses = Response::
            join('questions', 'responses.question_id', '=', 'questions.id')
            ->join('supervisors', 'responses.supervisor_id', '=', 'supervisors.id')
            //->join('options', 'responses.option_id', '=', 'options.id')
            ->join('maintainers', 'responses.maintainer_id', '=', 'maintainers.id')
            ->join('elements', 'responses.element_id', '=', 'elements.id')
            ->select('supervisors.name as Supervisor','questions.name as Tarea','maintainers.name as Mantenedor','elements.name as Elemento de Mantenedor','responses.content as Contenido','responses.limit_date as Fecha Límite','responses.created_at as Fecha Respuesta')
            ->get()
            ->toArray();