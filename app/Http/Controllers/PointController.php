<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Point;
use Illuminate\Support\Facades\Session;

class PointController extends Controller
{
    public function points()
    {
    	$points=Point::all();
        return view('points',['points'=>$points]);
    }

    public function addPointPost(Request $request)
    {
    	$point=new Point;
        
        $point->name=$request->input('name');

        $point->save();

        return redirect()->route('points');
    }

    public function updatePoint($id)
    {
        $point=Point::FindOrFail($id);
        if (is_null($point))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('points');
        }
        else
        {
            return view('updatePoint',['point'=>$point]);
        }
    }

    public function updatePointPost(Request $request,$id)
    {
        $point=Point::where('id',$id)
            ->update(['name'=>$request->input('name')]);
        Session::flash('message1','El Punto se actualizó correctamente!');
        return redirect()->route('points');
    }

    public function deletePoint($id)
    {
        $point=Point::FindOrFail($id);
        $result=$point->delete();
        if ($result)
        {
            Session::flash('message1','El Punto se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar el Punto.');
        }
        return redirect()->route('points');
    }
}
