<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supervisor;
use App\Position;
use App\SupervisorToken;
use Illuminate\Support\Facades\Session;

class SupervisorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$supervisors=Supervisor::all();
        return view('supervisors',['supervisors'=>$supervisors]);
    }

    public function addSupervisor()
    {
    	$positions=Position::all();
        return view('addSupervisor',['positions'=>$positions]);
    }

    public function addSupervisorPost(Request $request)
    {
        $token=new SupervisorToken;
        $token->token=str_random(90);
        $token->save();

    	$supervisor=new Supervisor;
        $supervisor->name=$request->input('name');
        $supervisor->last_name=$request->input('last_name');
        $supervisor->email=$request->input('email');
        $supervisor->username=$request->input('username');
        $supervisor->password=$request->input('password');
        $supervisor->token_id=$token->id;
        if (is_null($request->input('active')))
        {
            $supervisor->enable='false';
        }
        else
        {
            $supervisor->enable=$request->input('active');
        }
        $supervisor->position_id=$request->input('position_id');
        $supervisor->save();

        Session::flash('message1','El Supervisor se agregó correctamente!');

        return redirect()->route('/');
    }

    public function deleteSupervisor($id)
    {
        $supervisor=Supervisor::FindOrFail($id);
        $result=$supervisor->delete();
        if ($result)
        {
            Session::flash('message1','El Supervisor se eliminó correctamente!');
        }
        else
        {
            Session::flash('message3','Ocurrió un error al eliminar el supervisor.');
        }
        return redirect()->route('/');
    }

    public function updateSupervisor($id)
    {
        $supervisor=Supervisor::FindOrFail($id);
        if (is_null($supervisor))
        {
            Session::flash('message3','Ocurrió un problema.');
            return redirect()->route('/');
        }
        else
        {
            $positions=Position::all();
            return view('updateSupervisor',['supervisor'=>$supervisor,'positions'=>$positions]);
        }
    }

    public function updateSupervisorPost(Request $request,$id)
    {
        if (is_null($request->input('active')))
        {
            $active='false';
        }
        else
        {
            $active='true';
        }
        $supervisor=Supervisor::where('id',$id)
            ->update(['name'=>$request->input('name'),
                    'last_name'=>$request->input('last_name'),
                    'email'=>$request->input('email'),
                    'username'=>$request->input('username'),
                    'password'=>$request->input('password'),
                    'enable'=>$active,
                    'position_id'=>$request->input('position_id')
                    ]);
        Session::flash('message1','El Supervisor se actualizó correctamente!');
        return redirect()->route('/');
    }
}
