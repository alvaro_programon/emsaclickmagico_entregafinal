<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TokenFcmProfessional extends Model
{
    protected $table = 'tokens_fcm_professional';
}
