<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ScheduledTask;

class PrincipalScheduledTask extends Model
{
    protected $table = 'principal_scheduled_tasks';

    public function point()
    {
        return $this->belongsTo('App\Point');
    }

    public function scheduledTasks()
    {
        return $this->hasMany('App\ScheduledTask');
    }

    public function existsSupervisor($principal_scheduled_task_id,$supervisor_id)
    {
    	$scheduled_task=ScheduledTask::where('supervisor_id',$supervisor_id)
    								->where('principal_scheduled_task_id',$principal_scheduled_task_id)
    								->first();
        if (!is_null($scheduled_task))
        {
        	return true;
        }
        else
        {
			return false;
        }
    }

    public function existsProfessional($principal_scheduled_task_id,$professional_id)
    {
    	$scheduled_task=ScheduledTask::where('professional_id',$professional_id)
    								->where('principal_scheduled_task_id',$principal_scheduled_task_id)
    								->first();
        if (!is_null($scheduled_task))
        {
        	return true;
        }
        else
        {
			return false;
        }
    }
}
