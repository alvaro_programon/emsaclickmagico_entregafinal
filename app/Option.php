<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;

class Option extends Model
{
    protected $table = 'options';

    public function question()//para obtener la poll
    {
    	return $this->belongsTo('App\Question');
    }
    
    public function has_question($option_id)
    {
    	$question=Question::where('activated_by_option_id',$option_id)->first();
        return $question;
    }
}
