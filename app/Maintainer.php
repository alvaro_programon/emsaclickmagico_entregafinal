<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maintainer extends Model
{
    protected $table = 'maintainers';

    public function elements()//para tener los elementos
    {
        return $this->hasMany('App\Element');
    }
}
