<?php

namespace App;

use App\Position;
use App\SupervisorToken;
use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{
    protected $table = 'supervisors';

    public function position()
    {
    	return $this->belongsTo(Position::class,'position_id');
    }

    public function token()
    {
    	return $this->belongsTo(SupervisorToken::class,'token_id');
    }
}
