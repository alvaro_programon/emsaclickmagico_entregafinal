<?php
//MyReport.php
namespace App\Reports;

require_once dirname(__FILE__)."/../../vendor/koolphp/koolreport/koolreport/autoload.php"; // No need, if you install KoolReport through composer

class MyReport extends \koolreport\KoolReport
{
    //We leave this blank to demo only
    
    function settings()
    {
        return array(
            "dataSources"=>array(
                "mydata"=>array(
                    'connectionString' => 'mysql:host=127.0.0.1;dbname=click_magico_laravel_db',
                    'username' => 'root',
                    'password' => 'clickmagico12',
                    'charset' => 'utf8',
                )
            )
        );
    }
    function setup()
    {
        $this->src("mydata")
        ->query("select id,name,last_name from `professionals`")
        ->pipe($this->dataStore("employees"));
    }
}

