<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    protected $table = 'polls';

    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
