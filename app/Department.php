<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Supervisor;

class Department extends Model
{
    protected $table = 'departments';
}
