<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Option;
use App\Maintainer;

class Question extends Model
{
    protected $table = 'questions';

    public function maintainer()
    {
        return $this->belongsTo(Maintainer::class,'maintainer_id');
    }

    public function options()//para tener las options
    {
        return $this->hasMany('App\Option');
    }

    public function responses()//para tener las options
    {
        return $this->hasMany('App\Response');
    }

    public function poll()//para obtener la poll
    {
    	return $this->belongsTo('App\Poll');
    }

    public function get_id_parent_question($option_id)
    {
    	$option=Option::FindOrFail($option_id);
        if (is_null($option))
        {
            return redirect()->route('polls');
        }
        else
        {
        	return $option->question->id;
        }
    }
}
