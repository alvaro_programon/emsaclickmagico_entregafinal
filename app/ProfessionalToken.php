<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessionalToken extends Model
{
    protected $table = 'professional_tokens';

    public function professional()//para obtener la poll
    {
    	return $this->belongsTo('App\Professional');
    }
}
