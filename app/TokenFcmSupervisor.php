<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TokenFcmSupervisor extends Model
{
    protected $table = 'tokens_fcm_supervisor';
}
