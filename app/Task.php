<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    public function response()
    {
    	return $this->belongsTo('App\Response');
    }

    public function professional()//para obtener la poll
    {
    	return $this->belongsTo('App\Professional');
    }
}
