<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $table = 'elements';

    public function maintainer()//para obtener la Maintainer
    {
    	return $this->belongsTo('App\Maintainer');
    }
}
