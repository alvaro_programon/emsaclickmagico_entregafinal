<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProfessionalToken;
use App\TokenFcmProfessional;

class Professional extends Model
{
    protected $table = 'professionals';

    public function token()
    {
    	return $this->belongsTo(ProfessionalToken::class,'token_id');
    }

    public function token_fcm()
    {
    	return $this->hasOne(TokenFcmProfessional::class);
    }
}
