<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    protected $table = 'containers';

    public function responses()//para tener las responses
    {
        return $this->hasMany('App\Response');
    }
}
